<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>internet hotspot > login</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />

<script src="script/jquery-1.5.2.min.js"></script>
<script src="script/modernizr.js"></script>

</head>

<body>

<?php

$width = urldecode($_POST['width']);

if (!isset($width) || $width == NULL || $width == 0) {
	$width = 1000;
}

$mobile = ($width <= 960);

include "php/functions.php";
$user_name = urldecode($_POST['username']);
$mac_address = substr($user_name, 2);
$location = urldecode($_POST['location']);

if (!isset($location) || $location == NULL || $location == '') {
	$location = 'QeenSt150';
}

$hsname = urldecode($_POST['hostname']);
$user_id = check_user($mac_address);
$page = get_location_page($location)."/online";
$tariff = select_tariff($location, $mac_address);

$advs = select_advs($location, $mac_address, $tariff, $mobile);
////////echo "tariff = " . $tariff . "<br>";
////////echo "mac_address = " . $mac_address . "<br>";

//*****************************************************************************
//Added by Marat. GA tracking script
//*****************************************************************************
?>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	userID = '<?php echo $user_id?>';
	pageAddr = '<?php echo $page?>';
	ga('create', 'UA-70023606-1', {
		'clientId': userID,
		'userId': userID
	});
	ga('set', 'location', pageAddr);
	ga('send', 'pageview');
</script>
<?php
//*****************************************************************************

include "login" . $tariff . ".php";

?>
</body>
</html>

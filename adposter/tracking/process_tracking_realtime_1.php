<?php
include ("calc_coords_1.php");

// // // !!!! can be useful later
// // // function string2Hex($string){
    // // // $hex = 0;
	// // // $len = strlen($string);
    // // // for ($i = 0; $i < $len; $i++){
        // // // $digit = strtoupper(substr($string, $i, 1));
		// // // switch ($digit) {
			// // // case "A";
				// // // $digit = 10;
			// // // break;
			// // // case "B";
				// // // $digit = 11;
			// // // break;
			// // // case "C";
				// // // $digit = 12;
			// // // break;
			// // // case "D";
				// // // $digit = 13;
			// // // break;
			// // // case "E";
				// // // $digit = 14;
			// // // break;
			// // // case "F";
				// // // $digit = 15;
			// // // break;
		// // // }
		// // // $hex = $hex + $digit * pow(16, $len - $i - 1);
    // // // }
    // // // return $hex;
// // // }

// // // function hexToStr($hex){
    // // // $string='';
    // // // for ($i=0; $i < strlen($hex)-1; $i+=2){
        // // // $string .= chr(hexdec($hex[$i].$hex[$i+1]));
    // // // }
    // // // return $string;
// // // }

//find max signal for each ip (wifi sniffer) in the conseq
function getIpArray($recArr, $startRow, $stopRow){
	$ipArr = array();
	$ipaCnt = 0;
	for ($k = $startRow; $k <= $stopRow; $k++) {
		//check for ip in the array
		$ip = $recArr[$k][2];
		$signalLvl = $recArr[$k][3];
		$signalChnl = $recArr[$k][4];
		$hasIp = false;
		for($l=0; $l < $ipaCnt; $l++){
			if ($ipArr[$l]['ip'] == $ip) {
				$hasIp = true;
				break;
			}
		}
		//if has ip, compare signal lvls (and replace)
		if ($hasIp) {
			if ($ipArr[$l]['signal'] < $signalLvl) {
				$ipArr[$l]['signal'] = $signalLvl;
				$ipArr[$l]['chnl'] = $signalChnl;
			}
		} 
		//or create new rec with the curent signal lvl
		else {
			$ipArr[$ipaCnt] = array();
			$ipArr[$ipaCnt]['ip'] = $ip;
			$ipArr[$l]['signal'] = $signalLvl;
			$ipArr[$l]['chnl'] = $signalChnl;
			$ipaCnt++;
		}
	}
	return $ipArr;
}

echo "\n\n\n";
echo "****** start\n";
//
$curDir = getcwd();
echo "****** current dir = ".$curDir."\n";
//read settings
@$settings = parse_ini_file("process_tracking.ini");
if (!$settings) exit ("!!! FATAL ERROR !!! CAN'T FIND/READ INI FILE. EXIT");
echo "****** setings:\n";
echo "****** location_id = {$settings['location_id']}\n";
echo "****** server = {$settings['server']}\n";
echo "****** user = {$settings['user']}\n";
echo "****** password = {$settings['password']}\n";
echo "****** database_name = {$settings['database_name']}\n";
echo "\n";
date_default_timezone_set($settings['timezone']);
//

//infinite loop to check current dir for dump files and process them ()
while (true){
	echo "*****************************************************************************\n";
	echo "**** checking dir\n";
	$time = date("Y-m-d H:i:s");
	echo "**** time = {$time}\n";
	
	//go through all files (and dirs) in the folder
	$allFiles = scandir($curDir);
	//
	for ($fileNum = 0; $fileNum < count($allFiles); $fileNum++){
		//get file name
		$filename = $allFiles[$fileNum];
		echo "** filename = ".$filename."\n";
		//current timestamp (if can get it from filename), ignore links to current dir and updir
		//!!!!!!!!!!!!change this condition
		if (($filename != ".") && ($filename != "..")){
			$datetimeStr = str_replace("_", " ", $filename);
			$dateStr = substr($datetimeStr, 0, 10);
			$timeStr = substr($datetimeStr, count($datetimeStr)-9, 8);
			$timeStr = str_replace("-", ":", $timeStr);
			$timeStamp = $dateStr." ".$timeStr;
		} 
		else {
			$timeStamp = "";
		}
		//if it is a dump file to process (if not - ingore): 
		$stampToTime = strtotime($timeStamp);
		if ($filename == "111"){
			echo "** TO BE PROCESSED:\n";
			echo "** timestamp = {$timeStamp}\n";
			$attempt = 0;
			$procSuccess = false;
			//initial stage of execution. if there is an exception, the execution will continue after a while (30 sec) from the stage it was interrupted
			$execStage = "connect_to_db";
			//
			while (!$procSuccess) {
				$attempt++;
				echo "\n\n\nSTART PROCESSING: ATTEMPT {$attempt}\n";
				$time = date("Y-m-d H:i:s");
				echo "time = {$time}\n";
				echo "\n";
				//
				try {
					//go through all stages from the last one it was interrupted
					switch ($execStage){
						case "connect_to_db":
							//connect to DB
							echo "connect to DB\n";
							@$link = mysql_connect($settings['server'], $settings['user'], $settings['password']);
							echo mysql_error();
							if (!$link) throw new Exception('ERROR! Cannot connect to DB server');
							@$selRes = mysql_select_db($settings['database_name']);
							if (!$selRes) throw new Exception('ERROR! Cannot find DB');
							echo "\n";
							//
							$execStage = "get_sniffers_list";
						case "get_sniffers_list":
							//get list of sniffers(sniffers)
							echo "get list of sniffers\n";
							$query  = "SELECT ip, coord_x, coord_y FROM tbl_sniffers WHERE location_id = '{$settings['location_id']}'";
							$result = mysql_query($query);
							if (!$result) throw new Exception('ERROR! Cannot get sniffers list from DB');
							$sniffers_list = array();
							for ($i=0; $i<mysql_num_rows($result); $i++){
								$myrow = mysql_fetch_array($result);
								$sniffers_list[$i] = $myrow;
							}
							echo "\n";
							//
							$execStage = "read_file";	
						case "read_file":
							//read the initial file
							echo "read file, sort data, convert into an array\n";
							
							$dumpArray = file($filename);
							if (!$dumpArray) throw new Exception('ERROR! Cannot read file');
							$macSortArray = array();
							$msaCnt = 0;
							
							//sort records by MACs
							for ($i=0; $i < count($dumpArray); $i++){
								if ($dumpArray[$i] != "") {
									$strArray = explode("\t", $dumpArray[$i]);
									
									$signalLvl = 0 + $strArray[2];
									$signalChnl = 0 + $strArray[3];
									//records with weak signal are discarded not to waste time on them
									if ($signalLvl >= $minSignalLvl) {
										//convert strings to numbers to improve performance in future processing  
										////  !!! $mac = string2Hex(str_replace(":", "", $strArray[1]));
										$mac = $strArray[1];
										/////echo $mac."\n";
										$time = strtotime(substr($strArray[0], 0, 21));
										$absTime = $time * 1000 + substr($strArray[0], 22, 3);
										////  !!! $ip = 0 + str_replace(".", "", $strArray[4]);
										$ip = str_replace("\n", "", $strArray[4]);
										
										//check for MAC addr already in the array
										$hasMac = false;
										for($k=0; $k < $msaCnt; $k++){
											//if(gmp_cmp($macSortArray[$k][0], $mac) == 0){
											if ($macSortArray[$k][0] == $mac) {
												$hasMac = true;
												break;
											}
										}
										//add to existing
										if ($hasMac) {
											$recNum = count($macSortArray[$k][1]);
											$macSortArray[$k][1][$recNum] = array();
											$macSortArray[$k][1][$recNum][0] = $time;
											$macSortArray[$k][1][$recNum][1] = $absTime;
											$macSortArray[$k][1][$recNum][2] = $ip;
											$macSortArray[$k][1][$recNum][3] = $signalLvl;
											$macSortArray[$k][1][$recNum][4] = $signalChnl;
										} 
										//or create new
										else {
											$macSortArray[$msaCnt] = array();
											$macSortArray[$msaCnt][0] = $mac;
											$macSortArray[$msaCnt][1] = array();
											$recNum = 0;
											$macSortArray[$msaCnt][1][$recNum] = array();
											$macSortArray[$msaCnt][1][$recNum][0] = $time;
											$macSortArray[$msaCnt][1][$recNum][1] = $absTime;
											$macSortArray[$msaCnt][1][$recNum][2] = $ip;
											$macSortArray[$msaCnt][1][$recNum][3] = $signalLvl;
											$macSortArray[$msaCnt][1][$recNum][4] = $signalChnl;
											$msaCnt++;
										}
									}
								}
							}
							
							/* 
							for ($i=0; $i < count($macSortArray); $i++){
								echo("\t".$macSortArray[$i][0]."\n");
								for ($j=0; $j < count($macSortArray[$i][1]); $j++){
									echo("\t\t".$macSortArray[$i][1][$j][0]."\t".$macSortArray[$i][1][$j][2]."\t".$macSortArray[$i][1][$j][3]."\t".$macSortArray[$i][1][$j][4]."\t"."\n");
								}
							} */
							
							//find pack consequences for each MAC (device) in the array
							//each consequence will give us a tracking point recorded in DB
							$consequenceArray = array();
							for ($i=0; $i < count($macSortArray); $i++){
								//create record for the MAC
								$consequenceArray[$i] = array();
								$consequenceArray[$i]['mac'] = $macSortArray[$i][0];
								$consequenceArray[$i]['id'] = 0;
								$consequenceArray[$i]['cons'] = array();
								$cCnt = 0;
								//init consequence params
								$startRow = 0;
								$startTime = $macSortArray[$i][1][$startRow][1];
								//go through all recs for the MAC
								for ($j=1; $j < count($macSortArray[$i][1]); $j++){
									//if more that 1 second between packs consider as a boundary of the conseq
									if (($macSortArray[$i][1][$j][1] - $macSortArray[$i][1][$j-1][1]) > 1000) {
										$stopRow = $j - 1;
										//find max signal for each ip (wifi sniffer) in the conseq
										$ipArr = getIpArray($macSortArray[$i][1], $startRow, $stopRow);
										//record conseq data
										// // // $consequenceArray[$i]['cons'][$cCnt] = array();
										// // // $consequenceArray[$i]['cons'][$cCnt]['time'] = $macSortArray[$i][1][$startRow][0];
										// // // $consequenceArray[$i]['cons'][$cCnt]['ips'] = $ipArr;
										// // // $cCnt++;
										//proceed for the next conseq
										$startRow = $j;
										$startTime = $macSortArray[$i][1][$startRow][1];
									} 
									//if more than 2 sec between start time and current pack time, create conseq from the first half (approx) of the processed records, new conseq will start from the beginning of the second half. it is done in order to create consequences of no more than 2 seconds length 
									elseif (($macSortArray[$i][1][$j][1] - $startTime) > 2000) {
										for ($k = $startRow; $k < $j; $k++){
											if (($macSortArray[$i][1][$k][1] - $startTime) > 1000) {
												$stopRow = $k;
												//find max signal for each ip (wifi sniffer) in the conseq
												$ipArr = getIpArray($macSortArray[$i][1], $startRow, $stopRow);
												//record conseq data
												// // // $consequenceArray[$i]['cons'][$cCnt] = array();
												// // // $consequenceArray[$i]['cons'][$cCnt]['time'] = $macSortArray[$i][1][$startRow][0];
												// // // $consequenceArray[$i]['cons'][$cCnt]['ips'] = $ipArr;
												// // // $cCnt++;
												//proceed for the next conseq
												$startRow = $k + 1;
												$startTime = $macSortArray[$i][1][$startRow][1];
											}
										}
									}
									//last record (and conseq)
									if ($j == (count($macSortArray[$i][1]) - 1)) {
										$stopRow = $j;
										//find max signal for each ip (wifi sniffer) in the conseq
										$ipArr = getIpArray($macSortArray[$i][1], $startRow, $stopRow);
										//record conseq data
										$consequenceArray[$i]['cons'][$cCnt] = array();
										$consequenceArray[$i]['cons'][$cCnt]['time'] = $macSortArray[$i][1][$startRow][0];
										$consequenceArray[$i]['cons'][$cCnt]['ips'] = $ipArr;
									}
								}
							}
							//convert MACs into strings back to work with DB
							for ($i=0; $i < count($consequenceArray); $i++){
								$consequenceArray[$i]['mac'] = strtoupper($consequenceArray[$i]['mac']);
								// // // $consequenceArray[$i]['mac'] = strtoupper(hexToStr($consequenceArray[$i]['mac']));
								// // // $len = strlen($consequenceArray[$i]['mac']);
								// // // for ($l=0; $l < (12 - $len); $l++){
									// // // $consequenceArray[$i]['mac'] = "0".$consequenceArray[$i]['mac'];
								// // // }
								// // // $str = "";
								// // // for ($l=0; $l < 6; $l++){
									// // // $str = $str.substr($consequenceArray[$i]['mac'], $l*2, 2).".";
								// // // }
								// // // $consequenceArray[$i]['mac'] = substr($str, 0, 17);
							}
							/* echo ("\n\n\n");
							for ($i=0; $i < count($consequenceArray); $i++){
								echo("\t".$consequenceArray[$i]['mac']."\n");
								for ($j=0; $j < count($consequenceArray[$i]['cons']); $j++){
									echo("\t\t".$consequenceArray[$i]['cons'][$j]['time']."\n");
									for ($k=0; $k < count($consequenceArray[$i]['cons'][$j]['ips']); $k++){
										echo("\t\t\t".$consequenceArray[$i]['cons'][$j]['ips'][$k]['ip']."\t".$consequenceArray[$i]['cons'][$j]['ips'][$k]['signal']."\t".$consequenceArray[$i]['cons'][$j]['ips'][$k]['chnl']."\n");
									}
								}
							} */
							echo "\n";
							//
							$execStage = "check_MACs";
						case "check_MACs":
							//check for new/returned devices (clients)
							//get IDs for returned ones from the base
							echo "check which MACs are already in DB:\n";
							$query  = "SELECT id, mac_address, stuff FROM tbl_stat_users WHERE mac_address = ''";
							for ($i=0; $i < count($consequenceArray); $i++){
								$query = $query . " OR mac_address = '{$consequenceArray[$i]['mac']}'";
							}
							////////////////echo "{$query}\n";
							$result = mysql_query($query);
							if (!$result) throw new Exception('ERROR! Cannot get MAC list from DB');
							for ($i=0; $i<mysql_num_rows($result); $i++){
								$myrow = mysql_fetch_array($result);
								for ($j=0; $j < count($consequenceArray); $j++){
									if (strtoupper ($myrow['mac_address']) == strtoupper ($consequenceArray[$j]['mac'])){
										if ($myrow['stuff'] == 0){
											$consequenceArray[$j]['id'] = $myrow['id'];
											echo "{$consequenceArray[$j]['mac']} {$consequenceArray[$j]['id']}\n";
										} else {
											$consequenceArray[$j]['id'] = -1;
											echo "{$consequenceArray[$j]['mac']} staff_device\n";
										}
									}
								}
							}
							mysql_free_result($result);
							echo "\n";
							//
							$execStage = "add_MACs";
						case "add_MACs":
							//add new devices MACs to the base
							echo "add new MACs to DB:\n";
							$hasNewMACs = false;
							$query  = "SELECT id, mac_address FROM tbl_stat_users WHERE mac_address = ''";
							$queryIns = "INSERT INTO tbl_stat_users (mac_address) VALUES";
							for ($i=0; $i < count($consequenceArray); $i++){
								if ($consequenceArray[$i]['id'] == 0){
									$query = $query . " OR mac_address = '{$consequenceArray[$i]['mac']}'";
									$queryIns = $queryIns . " ('{$consequenceArray[$i]['mac']}'), ";
									$hasNewMACs = true;
									echo "{$consequenceArray[$i]['mac']}\n";
								}
							}
							$queryIns = substr($queryIns, 0, count($queryIns)-3);
							////////////echo "{$queryIns}\n";
							if ($hasNewMACs) $result = mysql_query($queryIns);
							if (!$result) throw new Exception('ERROR! Cannot insert new MACs to DB');
							echo "\n";
							//
							$execStage = "find_added_MACs";
						case "find_added_MACs":
							//get IDs for new devices from the base
							echo "find added MACs in DB:\n";
							//////////////echo "{$query}\n";
							$result = mysql_query($query);
							if (!$result) throw new Exception('ERROR! Cannot get new MAC list from DB');
							for ($i=0; $i<mysql_num_rows($result); $i++){
								$myrow = mysql_fetch_array($result);
								for ($j=0; $j < count($consequenceArray); $j++){
									if ($myrow['mac_address'] == $consequenceArray[$j]['mac']){
										$consequenceArray[$j]['id'] = $myrow['id'];
										echo "{$consequenceArray[$j]['mac']} {$consequenceArray[$j]['id']}\n";
										break;
									}
								}
							}
							mysql_free_result($result);
							echo "\n";
							//
							$execStage = "calc_coords";
						case "calc_coords":
							//calculate devices coordinates
							echo "calculate coordinates:\n";
							$coordinates = array();
							for ($i=0; $i < count($consequenceArray); $i++){
								$coordinates[$i] = array();
								$coordinates[$i]['mac'] = $consequenceArray[$i]['mac'];
								$coordinates[$i]['id'] = $consequenceArray[$i]['id'];
								$coordinates[$i]['hits'] = array();
								$hitsCount = 0;
								for ($j=0; $j < count($consequenceArray[$i]['cons']); $j++){
									//create an array spots (sniffers) with coordinates and distances to calc coords and create a "hit"
									$sniffers = array();
									$sniffersCount = 0;
									for ($k=0; $k < count($consequenceArray[$i]['cons'][$j]['ips']); $k++){
										for ($l=0; $l < count($sniffers_list); $l++) {
											if ($consequenceArray[$i]['cons'][$j]['ips'][$k]['ip'] == $sniffers_list[$l]['ip']) {
												$point = new Point;
												$point->x = 0 + $sniffers_list[$l]['coord_x'];
												$point->y = 0 + $sniffers_list[$l]['coord_y'];
												$sniffers[$sniffersCount]['point'] = $point;
												$sniffers[$sniffersCount]['dist'] = getDistance($consequenceArray[$i]['cons'][$j]['ips'][$k]['signal'], $consequenceArray[$i]['cons'][$j]['ips'][$k]['chnl']);
												$sniffersCount++;
												break;
											}
										}
									}
									//if can get a solution record it
									$point = calcCoords($sniffers);
									if ($point) {
										$coordinates[$i]['hits'][$hitsCount] = array();
										$coordinates[$i]['hits'][$hitsCount]['time'] = date("Y-m-d H:i:s", $consequenceArray[$i]['cons'][$j]['time']);
										$coordinates[$i]['hits'][$hitsCount]['point'] = $point;
										$hitsCount++;
									}
								}
							}
							echo "\n";
							//
							$execStage = "record_coords";
						case "record_coords":
							//record coordinates to the DB
							echo "found coordinates:\n";
							$queryDel = "DELETE FROM tbl_tracking";
							mysql_query($queryDel);
							
							$queryIns = "INSERT INTO tbl_tracking (location_id, user_id, time, coord_x, coord_y) VALUES";
							$hasHits = false;
							for ($i=0; $i < count($coordinates); $i++){
								echo("\t".$coordinates[$i]['mac']."\n");
								for ($j=0; $j < count($coordinates[$i]['hits']); $j++) {
									echo("\t\t".$coordinates[$i]['hits'][$j]['time']."\t(".$coordinates[$i]['hits'][$j]['point']->x."; ".$coordinates[$i]['hits'][$j]['point']->y.")\n");
									$queryIns = $queryIns . " ('{$settings['location_id']}', '{$coordinates[$i]['id']}', '{$coordinates[$i]['hits'][$j]['time']}', '{$coordinates[$i]['hits'][$j]['point']->x}', '{$coordinates[$i]['hits'][$j]['point']->y}'), ";
									$hasHits = true;
								}
							}
							echo "\n";
							echo "record coordinates:\n";
							$queryIns = substr($queryIns, 0, strlen($queryIns)-2);
							if ($hasHits) $result = mysql_query($queryIns);
							if (!$result) throw new Exception('ERROR! Cannot add records to DB');
							echo "\n";
							//
							$execStage = "finish";
						case "finish":
							//delete dump file
							echo "delete dump file\n";
							// // // // // $delRes = unlink($filename);
							// // // // // if (!$delRes) throw new Exception('ERROR! Cannot delete dump file');	
							//	
							$procSuccess = true;
							mysql_close($link);
							echo "SUCCESS\n";
							$time = date("Y-m-d H:i:s");
							echo "time = {$time}\n";
							echo "\n\n\n";
						default:
					}
				}
				catch (Exception $e){
					//if there was an exception try to finish the procees in a while (from the last stage)
					$procSuccess = false;
					echo $e->getMessage(), "\n";
					echo mysql_error()."\n";
					sleep(30);
				}
			}
		}
	}
	echo "**** sleep\n";
	sleep(30);
}
?>
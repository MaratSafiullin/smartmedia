<?php
$maxDeviation = 0.15;
$minDistance = 5;
$minSignalLvl = -75;
$maxIncreaseRadiusAttempts = 15;
$max3SpotsSolutionDeviation = $minDistance / 2;

class Point {
	public $x = 0;
	public $y = 0;
}

$solvedSqEquations = 0;

//get distance to a point from signal strengh
function getDistance($signal, $chnl){
	$dist = pow(10, (27.55 - log10(2407 + $chnl * 5) * 20 - $signal) / 20);
	return $dist;
}
//solve square equation
function solveSqEq($sqEqA, $sqEqB, $sqEqC){
	$sqEqDisc = $sqEqB*$sqEqB - 4 * $sqEqA * $sqEqC;

	//if there is a solution
	if ($sqEqDisc >= 0){
		$sqEqDiscSR = sqrt($sqEqDisc);
		$solution = array();
		$solution[0] = (-$sqEqB + $sqEqDiscSR) / (2 * $sqEqA);
		$solution[1]  = (-$sqEqB - $sqEqDiscSR) / (2 * $sqEqA);
		return $solution;
	} else {
		return false;
	}
}

//chose on of the poins using the distance to the 3-rd point
function getRelevantPoint($point1, $point2, $pointChk, $radiusChk){
	global $maxDeviation;
	
	$deviceX1 = $point1->x;
	$deviceX2 = $point2->x;
	$deviceY1 = $point1->y;
	$deviceY2 = $point2->y;
	$xChk = $pointChk->x;
	$yChk = $pointChk->y;
	
	//find minimal deviation
	$deviation1 = abs(sqrt(($deviceX1 - $xChk)*($deviceX1 - $xChk) + ($deviceY1 - $yChk)*($deviceY1 - $yChk)) - $radiusChk);
	$deviation2 = abs(sqrt(($deviceX2 - $xChk)*($deviceX2 - $xChk) + ($deviceY2 - $yChk)*($deviceY2 - $yChk)) - $radiusChk);
	if ($deviation1 < $deviation2) {
		$deviation = $deviation1;
		$point = $point1;
	} else {
		$deviation = $deviation2;
		$point = $point2;
	}
	
	//the solution is only possible if the deviation is not too big
	//otherwise have to discard it (probably there was an error getting initial data from sniffers or WiFi signal obstacles)
	if (($deviation / $radiusChk) < $maxDeviation) {
		return $point ;
	} else {
		return false;
	} 
}

//calculate coordinates based on possible circles intersection points
function calcCoordsBasic($spot1, $dist1, $spot2, $dist2, $spotChk, $distChk){
	global $solvedSqEquations;
	
	$x1 = $spot1->x;
	$y1 = $spot1->y;
	$radius1 = $dist1;
	$x2 = $spot2->x;
	$y2 = $spot2->y;
	$radius2 = $dist2;
	$pointChk = new Point;
	$pointChk->x = $spotChk->x;
	$pointChk->y = $spotChk->y;
	$radiusChk = $distChk;
	
	//try to find 2 possible points of intersection, then chose one of them as relevant 
	
	//have equation system, have to solve
	//(x-x1)^2 + (y-y1)^2 = radius1^2;
	//(x-x2)^2 + (y-y2)^2 = radius2^2;
	$kX = -2*($x1 - $x2);
	$kY = -2*($y1 - $y2);
	$constXY = $x1*$x1 + $y1*$y1 - $radius1*$radius1 - $x2*$x2 - $y2*$y2 + $radius2*$radius2;
	
	//try to find 
	//y = kXY * x + constXY;
	//if possible
	if (($kX !=0) && ($kY !=0)){
		$kXY = -$kX / $kY;
		$constXY = -$constXY / $kY;

		//using 
		//y = kXY * x + constXY;
		//convert one of the initial equations into a square equation 
		$sqEqA = 1 + $kXY*$kXY;
		$sqEqB = 2 * $kXY * $constXY - 2 * $x2 - 2 * $kXY * $y2;
		$sqEqC = $constXY*$constXY - 2 * $constXY * $y2 - $radius2*$radius2 + $x2*$x2 + $y2*$y2;
		
		//try to solve the square equation 
		$sqEqSolution = solveSqEq($sqEqA, $sqEqB, $sqEqC);

		//if there is a solution
		if ($sqEqSolution) {
			$solvedSqEquations++;
			//get 2 possible points
			$point1 = new Point;
			$point2 = new Point;
			$point1->x = $sqEqSolution[0];
			$point2->x = $sqEqSolution[1];
			$point1->y = $point1->x * $kXY + $constXY;
			$point2->y = $point2->x * $kXY + $constXY;

			//chose on of the poins using the distance to the 3-rd spot
			$point = getRelevantPoint($point1, $point2, $pointChk, $radiusChk);
			
			if ($point) {
				// echo($point->x."\n");
				// echo($point->y."\n");
				// echo("\n");
				return $point;
			} else {
				// echo("Deviation >> norm\n");
				return false;
			}
		}  else {
			// echo("No Sq Eq Res\n");
			return false;
		}
	}
	//if can't find
	//y = kXY * x + constXY;
	elseif (($kX == 0) && ($kY != 0)) {
		//find y
		$deviceY = -$constXY / $kY;
		
		//convert one of the initial equations into a square equation 
		$sqEqA = 1;
		$sqEqB = -2 * $x1;
		$sqEqC = $x1*$x1 + ($deviceY - $y1)*($deviceY - $y1) - $radius1*$radius1;
		$sqEqDisc = $sqEqB*$sqEqB - 4 * $sqEqA * $sqEqC;
		
		//try to solve the square equation 
		$sqEqSolution = solveSqEq($sqEqA, $sqEqB, $sqEqC);

		//if there is a solution
		if ($sqEqSolution) {
			$solvedSqEquations++;
			//get 2 possible points
			$point1 = new Point;
			$point2 = new Point;
			$point1->x = $sqEqSolution[0];
			$point2->x = $sqEqSolution[1];
			$point1->y = $deviceY;
			$point2->y = $deviceY;

			//chose on of the poins using the distance to the 3-rd spot
			$point = getRelevantPoint($point1, $point2, $pointChk, $radiusChk);
			
			if ($point) {
				// echo($point->x."\n");
				// echo($point->y."\n");
				// echo("\n");
				return $point;
			} else {
				// echo("Deviation >> norm\n");
				return false;
			}
		}  else {
			// echo("No Sq Eq Res\n");
			return false;
		}
	}
	elseif (($kY == 0) && ($kX != 0)) {
		//find x
		$deviceX = -$constXY / $kX;
		
		//convert one of the initial equations into a square equation 
		$sqEqA = 1;
		$sqEqB = -2 * $y1;
		$sqEqC = $y1*$y1 + ($deviceX - $x1)*($deviceX - $x1) - $radius1*$radius1;
		$sqEqDisc = $sqEqB*$sqEqB - 4 * $sqEqA * $sqEqC;
		
		//try to solve the square equation 
		$sqEqSolution = solveSqEq($sqEqA, $sqEqB, $sqEqC);

		//if there is a solution
		if ($sqEqSolution) {
			$solvedSqEquations++;
			//get 2 possible points
			$point1 = new Point;
			$point2 = new Point;
			$point1->x = $deviceX;
			$point2->x = $deviceX;
			$point1->y = $sqEqSolution[0];
			$point2->y = $sqEqSolution[1];
			
			// echo $point1->x."\n";
			// echo $point1->y."\n";
			// echo "\n";
			// echo $point2->x."\n";
			// echo $point2->y."\n";
			// echo "\n";
			
			//chose on of the poins using the distance to the 3-rd spot
			$point = getRelevantPoint($point1, $point2, $pointChk, $radiusChk);
			
			if ($point) {
				// echo($point->x."\n");
				// echo($point->y."\n");
				// echo("\n");
				return $point;
			} else {
				// echo("Deviation >> norm\n");
				return false;
			}
		}  else {
			// echo("No Sq Eq Res\n");
			return false;
		}
	}
}

//calculate coordinates based on distances from 3 sniffers, use averaging to improve precision
function calcCoords3Spots($spot1, $dist1, $spot2, $dist2, $spot3, $dist3){
	global $solvedSqEquations;
	global $maxIncreaseRadiusAttempts;
	$attempts = 0;
	$solutionCount = 0;
	$solvedSqEquations = 0;
	
	//try to find a solution for 3 spots increasing all the distances (circles radiuses) gradually step by step until we get at 3 cases of circles intersection (square equation solution) among them. can be useful when data from sniffers is incorrect and shows too small distances	
	while (($solvedSqEquations < 3) and ($attempts < $maxIncreaseRadiusAttempts)) {
		$attempts++;
		$solvedSqEquations = 0;
		$solutionCount = 0;
		$averageX = 0;
		$averageY = 0;
		
		// echo $dist1."\n";
		// echo $dist2."\n";
		// echo $dist3."\n";
		// echo "\n";
		$solution1 = calcCoordsBasic($spot1, $dist1, $spot2, $dist2, $spot3, $dist3);
		if ($solution1) {
			$solutionCount++;
			$averageX = $averageX + $solution1->x;
			$averageY = $averageY + $solution1->y;
			// echo("sol1\n");
			// echo($solution1->x."\n");
			// echo($solution1->y."\n");
			// echo("\n");
		}
		$solution2 = calcCoordsBasic($spot1, $dist1, $spot3, $dist3, $spot2, $dist2);
		if ($solution2) {
			$solutionCount++;
			$averageX = $averageX + $solution2->x;
			$averageY = $averageY + $solution2->y;
			// echo("sol2\n");
			// echo($solution2->x."\n");
			// echo($solution2->y."\n");
			// echo("\n");
		}
		$solution3 = calcCoordsBasic($spot2, $dist2, $spot3, $dist3, $spot1, $dist1);
		if ($solution3) {
			$solutionCount++;
			$averageX = $averageX + $solution3->x;
			$averageY = $averageY + $solution3->y;
			// echo("sol3\n");
			// echo($solution3->x."\n");
			// echo($solution3->y."\n");
			// echo("\n");
		}
		$dist1 = $dist1 * 1.1;
		$dist2 = $dist2 * 1.1;
		$dist3 = $dist3 * 1.1;
	}
	// echo $solution1->x."\n";
	// echo $solution1->y."\n";
	// echo $solution2->x."\n";
	// echo $solution2->y."\n";
	// echo $solution3->x."\n";
	// echo $solution3->y."\n";
	// echo "\n";
	
	if ($solutionCount > 0){
		$averageX = $averageX / $solutionCount;
		$averageY = $averageY / $solutionCount;
		// echo($averageX."\n");
		// echo($averageY."\n");
		// echo("\n");
		$point = new Point;
		$point->x = $averageX;
		$point->y = $averageY;
		return $point;
	} else {
		// echo("No average solution\n");
		// echo("\n");
		return false;
	}
}

//the main function
//calculate coordinates based on an array of sniffers data, try to use a combination of any 3 of them to get a solution. this is because some data can be distorted by physical reasons
function calcCoords($sniffers){
	global $minDistance;
	global $max3SpotsSolutionDeviation;
	$solutionCount = 0;
	$averageX = 0;
	$averageY = 0;
	$foundPoints = array();
	$relevantPointsCnt = 0;
	$finalX = 0;
	$finalY = 0;
	
	//exception: if a distance to any sniffer is less than minDistance (meters), take it's position as a device position
	for ($k=0; $k < count($sniffers); $k++){
		if ($sniffers[$k]['dist'] < $minDistance) {
			return $sniffers[$k]['point'];
		}
	}
	//calculation
	if (count($sniffers) >= 3) {
		for ($k1=0; $k1 < count($sniffers); $k1++){
			for ($k2=$k1+1; $k2 < count($sniffers); $k2++){
				for ($k3=$k2+1; $k3 < count($sniffers); $k3++){
					$point = calcCoords3Spots($sniffers[$k1]['point'], $sniffers[$k1]['dist'], $sniffers[$k2]['point'], $sniffers[$k2]['dist'], $sniffers[$k3]['point'], $sniffers[$k3]['dist']);
					if ($point) {
						$averageX = $averageX + $point->x;
						$averageY = $averageY + $point->y;
						$foundPoints[$solutionCount] = $point;
						$solutionCount++;
						echo "-" . $point->x . "\n";
						echo "-" . $point->y . "\n";
					}
				}
			}
		}
	}
	if ($solutionCount > 0){
		$averageX = $averageX / $solutionCount;
		$averageY = $averageY / $solutionCount;
			//discard irrelevant points and get a final solution
			for ($i=0; $i < $solutionCount; $i++){
				$deviationX = $foundPoints[$i]->x - $averageX;
				$deviationY = $foundPoints[$i]->y - $averageY;
				$deviation = sqrt($deviationX*$deviationX + $deviationY*$deviationY);
				if ($deviation < $max3SpotsSolutionDeviation){
					$finalX = $finalX + $foundPoints[$i]->x;
					$finalY = $finalY + $foundPoints[$i]->y;
					$relevantPointsCnt++;
				} else {
					echo "---" . $deviation . "\n";
				}
			}
			if ($relevantPointsCnt > 0){
				$finalX = $finalX / $relevantPointsCnt;
				$finalY = $finalY / $relevantPointsCnt;
				$point = new Point;
				$point->x = $finalX;
				$point->y = $finalY;
				return $point;
			} else {
				return false;
			}
	} else {
		return false;
	}
}

// $spot1 = new Point;
// $spot1->x = 103;
// $spot1->y = 137;

// $spot2 = new Point;
// $spot2->x = 137;
// $spot2->y = 137;

// $spot3 = new Point;
// $spot3->x = 103;
// $spot3->y = 103;

// $spot4 = new Point;
// $spot4->x = 137;
// $spot4->y = 103;

// // $dist1 = getDistance(-69, 1);
// // $dist2 = getDistance(-66, 1);
// // $dist3 = getDistance(-70, 1);
// // $dist4 = getDistance(-68, 1);
// $dist1 = 25 * 0.5;
// $dist2 = 22 * 0.5;
// $dist3 = 27 * 0.5;
// $dist4 = 25 * 0.5;
// echo $dist1."\n";
// echo $dist2."\n";
// echo $dist3."\n";
// echo $dist4."\n";
// echo "\n";

// //$point = calcCoordsBasic($spot1, $dist1, $spot2, $dist2, $spot3, $dist3);

// $point = calcCoords3Spots($spot1, $dist1, $spot2, $dist2, $spot3, $dist3);
// if ($point){
	// echo $point->x."\n";
	// echo $point->y."\n";
// } else {
	// echo "!\n";
// }

// $point = calcCoords3Spots($spot1, $dist1, $spot2, $dist2, $spot4, $dist4);
// if ($point){
	// echo $point->x."\n";
	// echo $point->y."\n";
// } else {
	// echo "!\n";
// }

// $point = calcCoords3Spots($spot1, $dist1, $spot3, $dist3, $spot4, $dist4);
// if ($point){
	// echo $point->x."\n";
	// echo $point->y."\n";
// } else {
	// echo "!\n";
// }

// $point = calcCoords3Spots($spot2, $dist2, $spot3, $dist3, $spot4, $dist4);
// if ($point){
	// echo $point->x."\n";
	// echo $point->y."\n";
// } else {
	// echo "!\n";
// }
?>
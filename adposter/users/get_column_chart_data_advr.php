<?php
include ("check_session.php");
include ("db_open.php");

//get first and last dates for a week
function getDatesByWeek($_week_number, $_year = null) {
	$year = $_year ? $_year : date('Y');
	$week_number = sprintf('%02d', $_week_number);
	$date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
	$date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
	return array($date_base, $date_limit);
}

//returns array of counts of shows or clicks for every location grouped by time as defined in arguments 
function getCountArray($DBTable, $userID, $startDate, $stopDate, $locationsCount, $locationsNum, $allLocations, $locationsArr, $periodType, $advsArr){
	//set date period format
	switch($periodType){
		case "days":
			$dateFormat = "DATE_FORMAT({$DBTable}.time_stamp,'%Y-%m-%d') AS date";
		break;
		case "weeks":
			$dateFormat = "DATE_FORMAT({$DBTable}.time_stamp,'%x-%v') AS date";
		break;
		case "months":
			$dateFormat = "DATE_FORMAT({$DBTable}.time_stamp,'%x-%m') AS date";
		break;
		case "hours":
			$dateFormat = "DATE_FORMAT({$DBTable}.time_stamp,'%H') AS date";
		break;
	}
	//get data about clicks, grouped by given periodType
	$query = "SELECT COUNT({$DBTable}.id) AS count, {$dateFormat}, tbl_location.location 
	FROM {$DBTable} 
	INNER JOIN tbl_location ON {$DBTable}.location_id = tbl_location.id
	INNER JOIN tbl_advs ON {$DBTable}.adv_id = tbl_advs.id
	INNER JOIN tbl_customers ON tbl_advs.customer_id = tbl_customers.id
	INNER JOIN tbl_users_customers ON tbl_users_customers.customer_id = tbl_customers.id
	INNER JOIN tbl_users ON tbl_users.id = tbl_users_customers.user_id
	WHERE tbl_users.id = '{$userID}' AND DATE_FORMAT({$DBTable}.time_stamp,'%Y-%m-%d') >= '{$startDate}' AND DATE_FORMAT({$DBTable}.time_stamp,'%Y-%m-%d') <= '{$stopDate}' ";
	//if there are specific locations to show in parameters select only them
	if (count($locationsArr) > 1){
		$locationsConstr = " AND (tbl_location.id='" . $locationsArr[0] . "'";
		for ($i=1; $i<count($locationsArr)-1; $i++){
			$locationsConstr = $locationsConstr . " OR tbl_location.id='" . $locationsArr[$i] . "'";
		}
		$locationsConstr = $locationsConstr . ")";
		$query = $query . $locationsConstr;
	}
	//if there are specific advs to show in parameters select only them
	if (count($advsArr) > 1){
		$advsConstr = " AND (tbl_advs.id='" . $advsArr[0] . "'";
		for ($i=1; $i<count($advsArr)-1; $i++){
			$advsConstr = $advsConstr . " OR tbl_advs.id='" . $advsArr[$i] . "'";
		}
		$advsConstr = $advsConstr . ")";
		$query = $query . $advsConstr;
	}  
//
	//check if need to group data by location or just get totals
	if ($allLocations) {
		$query = $query."GROUP BY date
		ORDER BY date";
	} else {
		$query = $query."GROUP BY date, tbl_location.location
		ORDER BY date, tbl_location.location";
	}
	
	$result1 = mysql_query($query) or die('Incorrect query: ' . mysql_error());
	
	//result array
	$resultArr = array();
	//if there is no locations grouping
	if ($allLocations){
		for ($i=0; $i<mysql_num_rows($result1); $i++){
			$date = mysql_result($result1,$i,'date');
			$count = mysql_result($result1,$i,'count');
			$resultArr[$i][0] = $date;
			$resultArr[$i][1] = $count;
		}
	} 
	//else each date is a row of array where date and counts are stored
	elseif (mysql_num_rows($result1) > 0){
		$resultCurrentRow = 0;
		//array of clicks for each day
		$countResults = array();
		for ($j=0; $j<$locationsCount; $j++){
			$countResults[$j] = 0;
		}
		//first day to process, store current day being processed
		$initDate = mysql_result($result1,0,'date');
		//go through all records (grouped by days and locations)
		for ($i=0; $i<mysql_num_rows($result1); $i++){
			$date = mysql_result($result1,$i,'date');
			//if reached next day (compared to current stored)
			if ($initDate != $date){
				//return data for that day (should be in one row for the receiving script)
				$resultArr[$resultCurrentRow][0] = $initDate;
				for ($j=0; $j<$locationsCount; $j++){
					$resultArr[$resultCurrentRow][$j+1] = $countResults[$j];
					$countResults[$j] = 0;
				}
				$resultCurrentRow++;
				//new current day
				$initDate = $date;
			}
			//write data about this day/location for future returning
			$count = mysql_result($result1,$i,'count');
			$location = mysql_result($result1,$i,'tbl_location.location');
			$countResults[$locationsNum[$location]] = $count;
		}
		//return data for the last day 
		$resultArr[$resultCurrentRow][0] = $initDate;
		for ($j=0; $j<$locationsCount; $j++){
			$resultArr[$resultCurrentRow][$j+1] = $countResults[$j];;
			$countResults[$j] = 0;
		}
	}
	
	return $resultArr;
}

//read paramters
if (isset($_POST['start_date'])){
	$startDate = $_POST['start_date'];
}
//$startDate = "2015-09-15";
if (isset($_POST['stop_date'])){
	$stopDate = $_POST['stop_date'];
}
//$stopDate = "2015-10-28";
if (isset($_POST['data_type'])){
	$dataType = $_POST['data_type'];
}
//$dataType = "shows";
// $dataType = "clicks";
//$dataType = "CTR";
if (isset($_POST['period_type'])){
	$periodType = $_POST['period_type'];
}
//$periodType = "days";
//$periodType = "weeks";
//$periodType = "months";
//$periodType = "hours";
if ($periodType == "hours"){
	$stopDate = $startDate;
}
if (isset($_POST['locations'])){
	$locations = $_POST['locations'];
}
if (isset($_POST['advs'])){
	$advs = $_POST['advs'];
}
if (isset($_POST['grouping'])){
	$grouping = $_POST['grouping'];
}

//chech for SQL injections
if(get_magic_quotes_gpc()==1){
	$startDate=stripslashes(trim($startDate));
	$stopDate=stripslashes(trim($stopDate));
	$locations=stripslashes(trim($locations));
	$advs=stripslashes(trim($advs));
} else {
	$startDate=trim($startDate);
	$stopDate=trim($stopDate);
	$locations=trim($locations);
	$advs=trim($advs);
}
$startDate=mysql_real_escape_string($startDate);
$stopDate=mysql_real_escape_string($stopDate);
$locations=mysql_real_escape_string($locations);
$advs=mysql_real_escape_string($advs);
$startDate = strip_tags($startDate);
$stopDate = strip_tags($stopDate);
$locations = strip_tags($locations);
$advs = strip_tags($advs);

$startDate = date("Y-m-d", strtotime($startDate));
$stopDate = date("Y-m-d", strtotime($stopDate));
$locationsArr = explode("+", $locations);
$advsArr = explode("+", $advs);

//table to select data from will depend on data type we need clicks or shows or ctr
if ($dataType == "clicks"){
	$DBTable = "tbl_stat_adv_clicked";
} elseif (($dataType == "shows")) {
	$DBTable = "tbl_stat_adv_shown";
}

//get list of locations for chart columns
$query = "SELECT tbl_location.location FROM tbl_location 
INNER JOIN tbl_advs_locations ON tbl_advs_locations.location_id = tbl_location.id
INNER JOIN tbl_advs ON tbl_advs_locations.adv_id = tbl_advs.id
INNER JOIN tbl_customers ON tbl_advs.customer_id = tbl_customers.id
INNER JOIN tbl_users_customers ON tbl_users_customers.customer_id = tbl_customers.id
INNER JOIN tbl_users ON tbl_users.id = tbl_users_customers.user_id
WHERE tbl_users.id = '{$userID}' ";
//if there are specific locations to show in parameters select only them
if (count($locationsArr) > 1){
	$locationsConstr = " AND (tbl_location.id='" . $locationsArr[0] . "'";
	for ($i=1; $i<count($locationsArr)-1; $i++){
		$locationsConstr = $locationsConstr = $locationsConstr . " OR tbl_location.id='" . $locationsArr[$i] . "'";
	}
	$locationsConstr = $locationsConstr . ")";
	$query = $query . $locationsConstr;
}
//
$query = $query . "GROUP BY tbl_location.location
ORDER BY tbl_location.location" ;



$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
//check there is any data about click for the period 
if (mysql_num_rows($result) != 0) {
	$locationsNum = array();
	$locationsCount = mysql_num_rows($result);
	//if getting CTR and there are many locations, they all are counted together
	if (($dataType == "CTR") && (count($locationsArr) > 2) && ($grouping != "locations") || ($grouping == "all_locations")){
		$allLocations = true;
		echo "All locations"."+"."\n";
	} 
	//else there are separate
	else {
		$allLocations = false;
		for ($i=0; $i<mysql_num_rows($result); $i++){
			$location = mysql_result($result,$i,'tbl_location.location');
			$locationsNum[$location] = $i;
			//return
			echo $location."+";
		}
		echo "\n";
	}
	
	function getChartWeekTitle($yearWeek){
		$year = substr($yearWeek, 0, 4);
		$week = substr($yearWeek, 5, 2);
		$resArr = getDatesByWeek($week, $year);
		return date('m-d', $resArr[0]) . "..." . date('m-d', $resArr[1]);
	}
	
	//for shows or clicks just get data from table
	if (($dataType == "clicks") || ($dataType == "shows")){
		$chartData = getCountArray($DBTable, $userID, $startDate, $stopDate, $locationsCount, $locationsNum, $allLocations, $locationsArr, $periodType, $advsArr);
		for ($i=0; $i<count($chartData); $i++){
			//if it's weeks don't return them as "year-weekNum" but format to "firstDate->lastDate"
			if ($periodType == "weeks") {
				echo getChartWeekTitle($chartData[$i][0]) . "+";
			}
			//otherwise just return as is
			else {
				echo $chartData[$i][0]."+";
			}
			for ($j=1; $j<count($chartData[$i]); $j++){
				echo $chartData[$i][$j]."+";
			}
			echo "\n";
		}
	} 
	//for CTR have to do calculations
	elseif ($dataType == "CTR") {
		$showsData = getCountArray("tbl_stat_adv_shown", $userID, $startDate, $stopDate, $locationsCount, $locationsNum, $allLocations, $locationsArr, $periodType, $advsArr);
		$clicksData = getCountArray("tbl_stat_adv_clicked", $userID, $startDate, $stopDate, $locationsCount, $locationsNum, $allLocations, $locationsArr, $periodType, $advsArr);
		$clicksDataCurrentRow = 0;
		//for each date from shows calculate
		for ($i=0; $i<count($showsData); $i++){
			//if it's weeks don't return them as "year-weekNum" but format to "firstDate->lastDate"
			if ($periodType == "weeks") {
				echo getChartWeekTitle($showsData[$i][0]) . "+";
			}
			//otherwise just return as is
			else {
				echo $showsData[$i][0]."+";
			}
			//if there are clicks, do dividing
			if ($clicksData[$clicksDataCurrentRow][0] == $showsData[$i][0]){
				for ($j=1; $j<count($showsData[$i]); $j++){
					if ($showsData[$i][$j] != "0") {
						echo number_format($clicksData[$clicksDataCurrentRow][$j] / $showsData[$i][$j], 4)."+";
					} 
					//or not
					else {
						echo "0+";
					}
				}
				$clicksDataCurrentRow++;
			} 
			//if no clicks then there are just zeros
			else {
				for ($j=1; $j<count($showsData[$i]); $j++){
					echo "0+";
				}
			}
			echo "\n";
		}
	}
} else {
	//echo "NO RESULTS";
}
mysql_free_result($result);

include ("db_close.php");
?>
<?php
header("Content-Type: text/html; charset=utf-8");
include ("check_session.php");
include ("db_open.php");

//returns array of counts of shows or clicks for every location grouped by time as defined in arguments 
function getCountArray($DBTable, $userID, $startDate, $stopDate, $locationsArr, $customersArr){
	//get data about clicks, grouped by customers
	$query = "SELECT COUNT({$DBTable}.id) AS count, tbl_customers.name AS customer_name
	FROM {$DBTable} 
	INNER JOIN tbl_advs ON {$DBTable}.adv_id = tbl_advs.id
	INNER JOIN tbl_customers ON tbl_advs.customer_id = tbl_customers.id
	INNER JOIN tbl_location ON {$DBTable}.location_id = tbl_location.id
	INNER JOIN tbl_users_locations ON tbl_users_locations.location_id = tbl_location.id
	INNER JOIN tbl_users ON tbl_users.id = tbl_users_locations.user_id
	WHERE tbl_users.id = '{$userID}' AND DATE_FORMAT({$DBTable}.time_stamp,'%Y-%m-%d') >= '{$startDate}' AND DATE_FORMAT({$DBTable}.time_stamp,'%Y-%m-%d') <= '{$stopDate}' ";
	//if there are specific locations to show in parameters select only them
	if (count($locationsArr) > 1){
		$locationsConstr = " AND (tbl_location.id='" . $locationsArr[0] . "'";
		for ($i=1; $i<count($locationsArr)-1; $i++){
			$locationsConstr = $locationsConstr . " OR tbl_location.id='" . $locationsArr[$i] . "'";
		}
		$locationsConstr = $locationsConstr . ")";
		$query = $query . $locationsConstr;
	}
	//if there are specific customers to show in parameters select only them
	if (count($customersArr) > 1){
		$customersConstr = " AND (tbl_customers.id='" . $customersArr[0] . "'";
		for ($i=1; $i<count($customersArr)-1; $i++){
			$customersConstr = $customersConstr . " OR tbl_customers.id='" . $customersArr[$i] . "'";
		}
		$customersConstr = $customersConstr . ")";
		$query = $query . $customersConstr;
	}
	//
	$query = $query."GROUP BY customer_name
	ORDER BY customer_name";
	$result1 = mysql_query($query) or die('Incorrect query: ' . mysql_error());
	//result array
	$resultArr = array();
	//add all selected customers and their results
	for ($i=0; $i<mysql_num_rows($result1); $i++){
		$customerName = mysql_result($result1, $i, 'customer_name');
		$count = mysql_result($result1,$i,'count');
		$resultArr[$i][0] = $customerName;
		$resultArr[$i][1] = $count;
		///////////echo $customerName . "+" . $count . "\n";
	}
	//
	return $resultArr;
}

//read paramters
if (isset($_POST['start_date'])){
	$startDate = $_POST['start_date'];
}
//$startDate = "2015-09-15";
if (isset($_POST['stop_date'])){
	$stopDate = $_POST['stop_date'];
}
//$stopDate = "2015-10-28";
if (isset($_POST['data_type'])){
	$dataType = $_POST['data_type'];
}
//$dataType = "shows";
// $dataType = "clicks";
//$dataType = "CTR";
if (isset($_POST['locations'])){
	$locations = $_POST['locations'];
}
if (isset($_POST['customers'])){
	$customers = $_POST['customers'];
}

//chech for SQL injections
if(get_magic_quotes_gpc()==1){
	$startDate=stripslashes(trim($startDate));
	$stopDate=stripslashes(trim($stopDate));
	$locations=stripslashes(trim($locations));
	$customers=stripslashes(trim($customers));
} else {
	$startDate=trim($startDate);
	$stopDate=trim($stopDate);
	$locations=trim($locations);
	$customers=trim($customers);
}
$startDate=mysql_real_escape_string($startDate);
$stopDate=mysql_real_escape_string($stopDate);
$locations=mysql_real_escape_string($locations);
$customers=mysql_real_escape_string($customers);
$startDate = strip_tags($startDate);
$stopDate = strip_tags($stopDate);
$locations = strip_tags($locations);
$customers = strip_tags($customers);

$startDate = date("Y-m-d", strtotime($startDate));
$stopDate = date("Y-m-d", strtotime($stopDate));
$locationsArr = explode("+", $locations);
$customersArr = explode("+", $customers);

//table to select data from will depend on data type we need clicks or shows or ctr
if ($dataType == "clicks"){
	$DBTable = "tbl_stat_adv_clicked";
} elseif (($dataType == "shows")) {
	$DBTable = "tbl_stat_adv_shown";
}

//for shows or clicks just get data from table
if (($dataType == "clicks") || ($dataType == "shows")){
	$chartData = getCountArray($DBTable, $userID, $startDate, $stopDate, $locationsArr, $customersArr);
	for ($i=0; $i<count($chartData); $i++){
		for ($j=0; $j<count($chartData[$i]); $j++){
			echo $chartData[$i][$j]."+";
		}
		echo "\n";
	}

} 
//for CTR have to do calculations
elseif ($dataType == "CTR") {
	$showsData = getCountArray("tbl_stat_adv_shown", $userID, $startDate, $stopDate, $locationsArr, $customersArr);
	$clicksData = getCountArray("tbl_stat_adv_clicked", $userID, $startDate, $stopDate, $locationsArr, $customersArr);
	$clicksDataCurrentRow = 0;
	//for each row from shows calculate
	for ($i=0; $i<count($showsData); $i++){
		echo $showsData[$i][0]."+";
		//if there are clicks, do dividing
		if ($clicksData[$clicksDataCurrentRow][0] == $showsData[$i][0]){
			for ($j=1; $j<count($showsData[$i]); $j++){
				if ($showsData[$i][$j] != "0") {
					echo number_format($clicksData[$clicksDataCurrentRow][$j] / $showsData[$i][$j], 4)."+";
				} 
				//or not
				else {
					echo "0+";
				}
			}
			$clicksDataCurrentRow++;
		} 
		//if no clicks then there are just zeros
		else {
			for ($j=1; $j<count($showsData[$i]); $j++){
				echo "0+";
			}
		}
		echo "\n";
	}
}
include ("db_close.php");
?>
<?php
include ("check_session.php");
include ("db_open.php");

//read paramters
if (isset($_POST['tariffs'])){
	$tariffs = $_POST['tariffs'];
}

//chech for SQL injections
if(get_magic_quotes_gpc()==1){
	$tariffs=stripslashes(trim($tariffs));
} else {
	$tariffs=trim($tariffs);
}
$tariffs=mysql_real_escape_string($tariffs);
$tariffs = strip_tags($tariffs);
$tariffArr = explode("+", $tariffs);

//get list of advs for selection input
$query = "SELECT tbl_advs.id, tbl_advs.description FROM tbl_advs 
INNER JOIN tbl_customers ON tbl_advs.customer_id = tbl_customers.id
INNER JOIN tbl_users_customers ON tbl_users_customers.customer_id = tbl_customers.id
INNER JOIN tbl_users ON tbl_users.id = tbl_users_customers.user_id
WHERE tbl_users.id = '{$userID}'";

//if there are specific advs to show in parameters select only them
if (count($tariffArr) > 1){
	$tariffConstr = " AND (tbl_advs.tariff='" . $tariffArr[0] . "'";
	for ($i=1; $i<count($tariffArr)-1; $i++){
		$tariffConstr = $tariffConstr . " OR tbl_advs.tariff='" . $tariffArr[$i] . "'";
	}
	$tariffConstr = $tariffConstr . ")";
	$query = $query . $tariffConstr;
}

$query = $query . "GROUP BY tbl_advs.description
ORDER BY tbl_advs.description";

$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
//check there is any data
if (mysql_num_rows($result) != 0) {
	for ($i=0; $i<mysql_num_rows($result); $i++){
		$ad = mysql_result($result,$i,'tbl_advs.description');
		$ID = mysql_result($result,$i,'tbl_advs.id');
		//return
		echo "<tr data-adv-id='{$ID}'>\n";
		echo "<td><input type='checkbox' checked value='{$ID}' class='chart-control'></input></td>\n";
		echo "<td>{$ad}</td>\n";
		echo "</tr>\n";
	}
} else {
	//echo "NO RESULTS";
}
mysql_free_result($result);

include ("db_close.php");
?>
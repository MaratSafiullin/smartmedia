<?php
header("Content-Type: text/html; charset=utf-8");

include "get_user_credentials.php";
include "get_lang_array.php";
include ("check_session.php");
/* 
//load google API client library
set_include_path('vendor');
require_once "autoload.php";

//prepare credentials (to get token)
$client_email = $email;
$private_key = file_get_contents('keyfiles/'.$keyfile_name);
$scopes =  array(Google_Service_Analytics::ANALYTICS_READONLY);
$credentials = new Google_Auth_AssertionCredentials(
    $client_email,
    $scopes,
    $private_key
);

//getting token
$client = new Google_Client();
$client->setAssertionCredentials($credentials);
if ($client->getAuth()->isAccessTokenExpired()) {
  $client->getAuth()->refreshTokenWithAssertion();
}

//extract token string to use it with Embed API components
$access_token = $client->getAccessToken();
$token_obj = json_decode($access_token);
$token_string = $token_obj->{'access_token'};
 */
?>
<!DOCTYPE html>
<html xml:lang="ru-ru" lang="ru-ru">
<head>
	<script type="text/javascript">
		var pageType = "dashboard_customers_statistics";
		var accessToken = "<?php echo $token_string;?>";
		var viewID = "<?php echo $view_id;?>";
		<?php printLangArrayJS();?>
	</script>
	
	<meta http-equiv="content-language" content="ru" /> 
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel='shortcut icon' type='image/x-icon' href='../favicon.ico' />

    <title><?php echo getSign("page_title", $user_language);?></title>

    <link href="../css/metro.css" rel="stylesheet">
    <link href="../css/metro-icons.css" rel="stylesheet">
    <link href="../css/metro-responsive.css" rel="stylesheet">
	<link href="../css/chart.css" rel="stylesheet">

    <script src="../script/jquery-2.1.3.min.js"></script>
    <script src="../script/jquery.dataTables.min.js"></script>
    <script src="../script/metro.js"></script>
	
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['table']}]}"></script>
	<script type="text/javascript" src="../script/dump.js"></script>
	<script type="text/javascript" src="../script/chart.js"  charset="utf-8"></script>
  <style>
        html, body {
            height: 100%;
        }
        body {
        }
        .page-content {
            padding-top: 3.125rem;
            min-height: 100%;
            height: 100%;
        }
        .table .input-control.checkbox {
            line-height: 1;
            min-height: 0;
            height: auto;
        }

        @media screen and (max-width: 800px){
            #cell-sidebar {
                flex-basis: 52px;
            }
            #cell-content {
                flex-basis: calc(100% - 52px);
            }
        }
    </style>
	
	
	
</head>
<body style="background: #71B1D1;">
	<section id="auth-button"></section>
   <div class="app-bar darcula" data-role="appbar">
		<span class="app-bar-element branding size-x200"><?php echo "{$first_name} {$last_name}";?></span>				      
		<div class="place-right">		            
			<div class="app-bar-element place-right">
				<span><a style="color: #FFFFFF;" href="login.php?logout=1"><?php echo getSign("logout", $user_language);?></a></span>
			</div>				
			
			<div class="app-bar-nonelement">								
				<span><?php echo getSign("range", $user_language);?></span>
				<div class="input-control select">
					<select id="select-period" class="chart-control">
						<option value="hours"><?php echo getSign("hour", $user_language);?></option>
						<option value="days" selected><?php echo getSign("day", $user_language);?></option>
						<option value="weeks"><?php echo getSign("week", $user_language);?></option>
						<option value="months"><?php echo getSign("month", $user_language);?></option>
					</select>
				</div>
			</div>				
			
			<?php
			if ($user_language == "rus"){
				$datePickerLocale = "ru";
			} else {
				$datePickerLocale = "en";
			}
			?>
			
			<div class="app-bar-nonelement">   			
				<span><?php echo getSign("from", $user_language);?></span>
				<div class="input-control text" id="select-date-from" data-role="datepicker" data-other-days="true" data-week-start="1" data-locale="<?php echo $datePickerLocale;?>" data-preset="<?php echo date('Y-m-d', time() - 6 * 24 * 60 * 60);?>" data-format="dd.mm.yyyy">
					<input class="date-text chart-control" type="text" value="<?php echo date('d.m.Y', time() - 6 * 24 * 60 * 60);?>">
					<button class="button"><span class="mif-calendar"></span></button>
				</div>   
		   </div>
			   
			<div class="app-bar-nonelement">
				<span><?php echo getSign("to", $user_language);?></span>
				<div class="input-control text" id="select-date-to" data-role="datepicker" data-other-days="true" data-week-start="1" data-locale="<?php echo $datePickerLocale;?>" data-preset="<?php echo date('Y-m-d');?>" data-format="dd.mm.yyyy">
					<input class="date-text chart-control" type="text" value="<?php echo date('d.m.Y');?>">
					<button class="button"><span class="mif-calendar"></span></button>
				</div>
			</div>      
			<span class="app-bar-divider"></span>
		</div>
   </div> 
	<div class="flex-grid" style="background: white;">
		<div class="row" style="height: 100vh;">
			<div class="cell size-x200" id="cell-sidebar" style="background-color: #71b1d1;">
				<ul class="sidebar">
					<li class="active" data-panel_id="chart-panel-overview"><a href="#">
						<span class="mif-widgets icon"></span>
						<span class="title"><?php echo getSign("gen_info", $user_language);?></span>
						<span class="counter"></span>
					</a></li>
					<li data-panel_id="chart-panel-visits"><a href="#">
						<span class="mif-organization icon"></span>
						<span class="title"><?php echo getSign("visits", $user_language);?></span>
						<span class="counter"></span>
					</a></li>
					<li data-panel_id="chart-panel-demography"><a href="#">
						<span class="mif-mars icon"></span>
						<span class="title"><?php echo getSign("demography", $user_language);?></span>
						<span class="counter"></span>
					</a></li>
					<li data-panel_id="chart-panel-interests"><a href="#">
						<span class="mif-favorite icon"></span>
						<span class="title"><?php echo getSign("interests", $user_language);?></span>
						<span class="counter"></span>
					</a></li>
					<li data-panel_id="chart-panel-duration"><a href="#">
						<span class="mif-history icon"></span>
						<span class="title"><?php echo getSign("duration", $user_language);?></span>
						<span class="counter"></span>
					</a></li>
					<li data-panel_id="chart-panel-"><a href="#">
						<span class="mif-location icon"></span>
						<span class="title"><?php echo getSign("tracking", $user_language);?></span>
					</a></li>
					<li data-panel_id="chart-panel-export"><a href="#">
						<span class="mif-file-download icon"></span>
						<span class="title"><?php echo getSign("export", $user_language);?></span>
					</a></li>
				</ul>
			</div>
			
			
			<div id="panels-box" class="cell auto-size">
			<div id="load-indicator"><div id="load-indicator-spinner"><span class="mif-spinner3 mif-ani-spin" ></span></div></div>
			
				<div class="panel chart-panel" style="height: 100%; width: 100%;" id="chart-panel-overview">
					<div class="heading">
						<span class="title"><?php echo getSign("general_information", $user_language);?></span>
					</div>
					<div class="content">
						<div class="ga-total-vidgets-container">
							<div class="panel success ga-totalnum-vidget" id="sessions-total">
								<div class="heading">
									<span class="title"><?php echo getSign("visitors", $user_language);?></span>
								</div>
								<div class="content">
								</div>
							</div>
							<div class="panel success ga-totalnum-vidget" id="new-sessions-total">
								<div class="heading">
									<span class="title"><?php echo getSign("new", $user_language);?></span>
								</div>
								<div class="content">
								</div>
							</div>
							<div class="panel success ga-totalnum-vidget" id="old-sessions-total">
								<div class="heading">
									<span class="title"><?php echo getSign("returning", $user_language);?></span>
								</div>
								<div class="content">
								</div>
							</div>
							<div class="panel success ga-totalnum-vidget"  id="users-total">
								<div class="heading">
									<span class="title"><?php echo getSign("unique", $user_language);?></span>
								</div>
								<div class="content">
								</div>
							</div>
							<div class="panel success ga-totalnum-vidget" id="session-avgtime-total">
								<div class="heading">
									<span class="title"><?php echo getSign("avg_duration", $user_language);?></span>
								</div>
								<div class="content">
								</div>
							</div>
						</div>
						<div class="chart-container-pie-totals" id="chart-gender-totals-div">
							<h2><?php echo getSign("visitors_gender_unique", $user_language);?></h2>
							<section id="chart-gender-totals-container"></section>
						</div>
						<div class="chart-container-pie-totals" id="chart-usertypes-totals-div">
							<h2><?php echo getSign("new_returning_unique", $user_language);?></h2>
							<section id="chart-usertypes-totals-container"></section>
						</div>
					</div>
				</div>
				<div class="panel chart-panel" style="height: 100%; width: 100%;" id="chart-panel-visits">
					<div class="heading">
						<span class="title"><?php echo getSign("visits", $user_language);?></span>
					</div>
					<div class="content">
						<div class="chart-container-timeline" id="visits-timeline-div">
							<h2><?php echo getSign("visitors_visits", $user_language);?></h2>
							<section id="visits-timeline-container"></section>
						</div>
						<div class="chart-container-timeline" id="customers-timeline-div">
							<h2><?php echo getSign("visitors_unique", $user_language);?></h2>
							<section id="customers-timeline-container"></section>
						</div>
						<div class="chart-container-column-totals-100" id="chart-dayvisitors-totals-div">
							<h2><?php echo getSign("visitors_timeofday", $user_language);?></h2>
							<section id="chart-dayvisitors-totals-container"></section>
						</div>
						<div class="chart-container-table-100" id="chart-combined-visits-table-div">
							<h2><?php echo getSign("visitors_combined_data", $user_language);?></h2>
							<section id="chart-combined-visits-table-container"></section>
						</div>
					</div>
				</div>
				<div class="panel chart-panel" style="height: 100%; width: 100%;" id="chart-panel-demography">
					<div class="heading">
						<span class="title"><?php echo getSign("demography", $user_language);?></span>
					</div>
					<div class="content">
						<div class="chart-container-pie-totals" id="chart-gender-totals-div">
							<h2><?php echo getSign("visitors_gender_unique", $user_language);?></h2>
						</div>
						<div class="chart-container-column-totals-50" id="chart-age-totals-div">
							<h2><?php echo getSign("visitors_age_unique", $user_language);?></h2>
							<section id="chart-age-totals-container"></section>
						</div>
						<div class="chart-container-table-100" id="chart-gender-table-div">
							<h2><?php echo getSign("visitors_gender_visits", $user_language);?></h2>
							<section id="chart-gender-table-container"></section>
						</div>
						<div class="chart-container-table-100" id="chart-age-table-div">
							<h2><?php echo getSign("visitors_age_visits", $user_language);?></h2>
							<section id="chart-age-table-container"></section>
						</div>
					</div>
				</div>
				<div class="panel chart-panel" style="height: 100%; width: 100%;" id="chart-panel-interests">
					<div class="heading">
						<span class="title"><?php echo getSign("interests", $user_language);?></span>
					</div>
					<div class="content">
						<div class="chart-container-bar-totals-100" id="chart-interests-commerc-div">
							<h2><?php echo getSign("interests_commerc", $user_language);?></h2>
							<section id="chart-interests-commerc-container"></section>
						</div>
						<div class="chart-container-bar-totals-100" id="chart-interests-general-div">
							<h2><?php echo getSign("interests_general", $user_language);?></h2>
							<section id="chart-interests-general-container"></section>
						</div>
					</div>
				</div>
				<div class="panel chart-panel" style="height: 100%; width: 100%;" id="chart-panel-duration">
					<div class="heading">
						<span class="title"><?php echo getSign("visits_duration", $user_language);?></span>
					</div>
					<div class="content">
						<div class="chart-container-timeline" id="visits-length-timeline-div">
							<h2><?php echo getSign("visit_avg_duration", $user_language);?></h2>
							<section id="visits-length-timeline-container"></section>
						</div>
						<div class="chart-container-table-100" id="visits-length-table-div">
							<h2><?php echo getSign("visit_avg_duration", $user_language);?></h2>
							<section id="visits-length-table-container"></section>
						</div>
					</div>
				</div>
				<div class="panel chart-panel" style="height: 100%; width: 100%;" id="chart-panel-export">
					<div class="heading">
						<span class="title"><?php echo getSign("export", $user_language);?></span>
					</div>
					<div class="content">
						<div class="app-bar-element place-right export-link">
							<span><a style="color: #FFFFFF;" href="#" id="export-chart-combined-visits-table"><?php echo getSign("export", $user_language);?></a></span>
						</div>
						<div class="chart-container-table-100" id="chart-combined-visits-table-div">
							<h2><?php echo getSign("visitors_combined_data", $user_language);?></h2>
						</div>
						<div class="app-bar-element place-right export-link">
							<span><a style="color: #FFFFFF;" href="#" id="export-chart-dayvisitors-table"><?php echo getSign("export", $user_language);?></a></span>
						</div>
						<div class="chart-container-table-100" id="chart-dayvisitors-table-div">
							<h2><?php echo getSign("visitors_timeofday", $user_language);?></h2>
							<section id="chart-dayvisitors-table-container"></section>
						</div>
						<div class="app-bar-element place-right export-link">
							<span><a style="color: #FFFFFF;" href="#" id="export-chart-gender-unique-table"><?php echo getSign("export", $user_language);?></a></span>
						</div>
						<div class="chart-container-table-100" id="chart-gender-unique-table-div">
							<h2><?php echo getSign("visitors_gender_unique", $user_language);?></h2>
							<section id="chart-gender-unique-table-container"></section>
						</div>
						<div class="app-bar-element place-right export-link">
							<span><a style="color: #FFFFFF;" href="#" id="export-chart-age-unique-table"><?php echo getSign("export", $user_language);?></a></span>
						</div>
						<div class="chart-container-table-100" id="chart-age-unique-table-div">
							<h2><?php echo getSign("visitors_age_unique", $user_language);?></h2>
							<section id="chart-age-unique-table-container"></section>
						</div>
						<div class="app-bar-element place-right export-link">
							<span><a style="color: #FFFFFF;" href="#" id="export-chart-gender-table"><?php echo getSign("export", $user_language);?></a></span>
						</div>
						<div class="chart-container-table-100" id="chart-gender-table-div">
							<h2><?php echo getSign("visitors_gender_visits", $user_language);?></h2>
						</div>
						<div class="app-bar-element place-right export-link">
							<span><a style="color: #FFFFFF;" href="#" id="export-chart-age-table"><?php echo getSign("export", $user_language);?></a></span>
						</div>
						<div class="chart-container-table-100" id="chart-age-table-div">
							<h2><?php echo getSign("visitors_age_visits", $user_language);?></h2>
						</div>
						<div class="app-bar-element place-right export-link">
							<span><a style="color: #FFFFFF;" href="#" id="export-chart-interests-commerc-table"><?php echo getSign("export", $user_language);?></a></span>
						</div>
						<div class="chart-container-table-100" id="chart-interests-commerc-table-div">
							<h2><?php echo getSign("interests_commerc", $user_language);?></h2>
							<section id="chart-interests-commerc-table-container"></section>
						</div>
						<div class="app-bar-element place-right export-link">
							<span><a style="color: #FFFFFF;" href="#" id="export-chart-interests-general-table"><?php echo getSign("export", $user_language);?></a></span>
						</div>
						<div class="chart-container-table-100" id="chart-interests-general-table-div">
							<h2><?php echo getSign("interests_general", $user_language);?></h2>
							<section id="chart-interests-general-table-container"></section>
						</div>
						<div class="app-bar-element place-right export-link">
							<span><a style="color: #FFFFFF;" href="#" id="export-visits-length-table"><?php echo getSign("export", $user_language);?></a></span>
						</div>
						<div class="chart-container-table-100" id="visits-length-table-div">
							<h2><?php echo getSign("visit_avg_duration", $user_language);?></h2>
						</div>
					</div>
				</div>
		</div>	
		</div>
	</div>
</body>
</html>

<?php
header("Content-Type: text/html; charset=utf-8");
/* 
include ("check_session.php");

//load google API client library
set_include_path('vendor');
require_once "autoload.php";

//prepare credentials (to get token)
$client_email = 'account-1@smartmedia-1131.iam.gserviceaccount.com';
$private_key = file_get_contents('Smartmedia-25c723b3783b.p12');
$scopes =  array(Google_Service_Analytics::ANALYTICS_READONLY);
$credentials = new Google_Auth_AssertionCredentials(
    $client_email,
    $scopes,
    $private_key
);

//getting token
$client = new Google_Client();
$client->setAssertionCredentials($credentials);
if ($client->getAuth()->isAccessTokenExpired()) {
  $client->getAuth()->refreshTokenWithAssertion();
}

//extract token string to use it with Embed API components
$access_token = $client->getAccessToken();
$token_obj = json_decode($access_token);
$token_string = $token_obj->{'access_token'};
 */
?>
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" type="text/css" media="all" href="../css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" media="all" href="../css/chart.css">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="../script/jquery-1.5.2.min.js"></script>
	<script type="text/javascript" src="../script/dump.js"></script>
	<script type="text/javascript" src="../script/chart.js"></script>
	<script type="text/javascript" src="../script/jquery-ui.js" charset="utf-8"></script>
	<script type="text/javascript" src="../script/jquery.ui.datepicker-ru.js" charset="utf-8"></script>
	<script type="text/javascript">
		var pageType = "a_charts";
		var accessToken = "<?php echo $token_string;?>";
	</script>
  </head>
  <body>
	<div id="header">
		<a href="login.php?logout=1">Выйти</a>
	</div>
	<div id="middle">
		<div id="control-panel">
			<div id="controls-wrapper">
				<div class="controls-block">
					<div id="period-selector-box">
						<label id="select-period-label">ОТЧЕТНЫЙ ПЕРИОД:</label>
						<br>
						<select id="select-period" class="chart-control">
							<option value="hours">Часы</option>
							<option value="days" selected>Дни</option>
							<option value="weeks">Недели</option>
							<option value="months">Месяцы</option>
						</select>
					</div>
					<br>
					<div id="date-from-box">
						<label id="select-date-from-label">ПОКАЗАТЬ С:</label>
						<br>
						<input readonly id="select-date-from" class="chart-control" type="text" value="<?php echo date('d.m.Y', time() - 6 * 24 * 60 * 60);?>"> 
					</div>
					<br>
					<div id="date-to-box">
						<label id="select-date-to-label">ПОКАЗАТЬ ПО:</label> 
						<br>
						<input readonly id="select-date-to" class="chart-control" type="text" value="<?php echo date('d.m.Y');?>">
					</div>
				</div>
			</div>
		</div>
		<div id="content-panel">
		
			<section id="auth-button"></section>
			
			<h1>Сводные данные по посетителям</h1>
			<div class="chart-container-pie-totals">
				<h2>Пол посетителей</h2>
				<section id="chart-gender-totals-container"></section>
			</div>
			<div class="chart-container-pie-totals">
				<h2>Возраст посетителей</h2>
				<section id="chart-age-totals-container"></section>
			</div>
			<div class="chart-container-pie-totals">
				<h2>Новые/Вернувшиеся посетители</h2>
				<section id="chart-usertypes-totals-container"></section>
			</div>
			<div class="chart-container-pie-totals">
				<h2>Суточное распределение посещений</h2>
				<section id="chart-dayvisitors-totals-container"></section>
			</div>
			
			<div class="content-panel-delimeter"></div>
			
			<h1>Графики посещений по времени</h1>
			<div class="chart-container-timeline">
				<h2>Уникальные посетители</h2>
				<section id="visits-timeline"></section>
			</div>
			<div class="chart-container-timeline">
				<h2>Посетители (количество посещений)</h2>
				<section id="customers-timeline"></section>
			</div>
			<div class="chart-container-timeline">
				<h2>Среднее время посещения</h2>
				<section id="visits-length-timeline"></section>
			</div>
			
			<div class="content-panel-delimeter"></div>
			
		</div>
	</div>
  </body>
</html>

<?php
header("Content-Type: text/html; charset=utf-8");
include ("check_session.php");
?>
<html>
  <head>
	<link rel="stylesheet" type="text/css" media="all" href="../css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" media="all" href="../css/chart.css">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="../script/jquery-1.5.2.min.js"></script>
	<script type="text/javascript" src="../script/chart.js"></script>
	<script type="text/javascript" src="../script/jquery-ui.js" charset="utf-8"></script>
	<script type="text/javascript" src="../script/jquery.ui.datepicker-ru.js" charset="utf-8"></script>
	<script type="text/javascript">var pageType = "location";</script>
  </head>
  <body>
	<div id="header">
		<a href="login.php?logout=1">LOGOUT</a>
	</div>
	<div id="middle">
		<div id="control-panel">
			<div id="controls-wrapper">
				<div class="controls-block">
					<div id="date-from-box">
						<label id="select-date-from-label">FROM:</label>
						<br>
						<input readonly id="select-date-from" class="chart-control" type="text" value="<?php echo date('d.m.Y', time() - 6 * 24 * 60 * 60);?>"> 
					</div>
					<br>
					<div id="date-to-box">
						<label id="select-date-to-label">TO:</label> 
						<br>
						<input readonly id="select-date-to" class="chart-control" type="text" value="<?php echo date('d.m.Y');?>">
					</div>
				</div>
				<div class="controls-block">
					<label id="select-locations-label">LOCATIONS:</label> 
					<br>
					<div id="select-locations">
						<table class="filter-table">
							<tbody>
							<?php include ("get_locations_list_partner.php");?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="controls-block">
					<label id="select-customers-label">CUSTOMERS:</label> 
					<br>
					<div id="select-customers">
						<table class="filter-table">
							<tbody id="customers-list">
							<?php include ("get_customers_list.php");?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div id="content-panel">
			<div id="php-text"></div>
				
			<div id="shows-chart" class="chart-container-bar"></div>
			
			<div id="clicks-chart" class="chart-container-bar"></div>

			<div id="CTR-chart" class="chart-container-bar"></div>
			
			<div class="content-panel-delimeter"></div>
			<!--<div id="container-shows-table" class="table-container"></div>
			<div id="container-clicks-table" class="table-container"></div>
			<div id="container-CTR-table" class="table-container"></div>-->
			<div id="container-combined-table" class="table-container"></div>
		</div>
	</div>
	
	
  </body>
</html>
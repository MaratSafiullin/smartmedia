<?php
include ("check_session.php");
include ("db_open.php");

//returns array of counts of shows or clicks for every location grouped by time as defined in arguments 
function getCountArray($DBTable, $userID, $startDate, $stopDate, $locationsCount, $locationsID, $allLocations, $periodType){
	//set date period format
	switch($periodType){
		case "days":
			$dateFormat = "DATE_FORMAT({$DBTable}.time_stamp,'%Y-%m-%d') AS date";
		break;
		case "weeks":
			$dateFormat = "DATE_FORMAT({$DBTable}.time_stamp,'%x-%v') AS date";
		break;
		case "months":
			$dateFormat = "DATE_FORMAT({$DBTable}.time_stamp,'%x-%m') AS date";
		break;
		case "hours":
			$dateFormat = "DATE_FORMAT({$DBTable}.time_stamp,'%H') AS date";
		break;
	}
	//get data about clicks, grouped by days
	$query = "SELECT COUNT({$DBTable}.id) AS count, {$dateFormat}, tbl_location.location 
	FROM {$DBTable} 
	INNER JOIN tbl_location ON {$DBTable}.location_id = tbl_location.id
	INNER JOIN tbl_advs ON {$DBTable}.adv_id = tbl_advs.id
	INNER JOIN tbl_customers ON tbl_advs.customer_id = tbl_customers.id
	INNER JOIN tbl_users ON tbl_users.customer_id = tbl_customers.id
	WHERE tbl_users.id = '{$userID}' AND DATE_FORMAT({$DBTable}.time_stamp,'%Y-%m-%d') >= '{$startDate}' AND DATE_FORMAT({$DBTable}.time_stamp,'%Y-%m-%d') <= '{$stopDate}' ";
	if ($allLocations) {
		$query = $query."GROUP BY date
		ORDER BY date";
	} else {
		$query = $query."GROUP BY date, tbl_location.location 
		ORDER BY date, tbl_location.location";
	}
	echo $query."<br><br><br>";
	$result1 = mysql_query($query) or die('Incorrect query: ' . mysql_error());
	
	//result array
	$resultArr = array();
	//if there is no locations grouping
	if ($allLocations){
		for ($i=0; $i<mysql_num_rows($result1); $i++){
			$date = mysql_result($result1,$i,'date');
			$count = mysql_result($result1,$i,'count');
			$resultArr[$i][0] = $date;
			$resultArr[$i][1] = $count;
		}
	} 
	//else each date is a row of array where date and counts are stored
	else {
		$resultCurrentRow = 0;
		//array of clicks for each day
		$countResults = array();
		for ($j=0; $j<$locationsCount; $j++){
			$countResults[$j] = 0;
		}
		//first day to process, store current day being processed
		$initDate = mysql_result($result1,0,'date');
		//go through all records (grouped by days and locations)
		for ($i=0; $i<mysql_num_rows($result1); $i++){
			$date = mysql_result($result1,$i,'date');
			//if reached next day (compared to current stored)
			if ($initDate != $date){
				//return data for that day (should be in one row for the receiving script)
				$resultArr[$resultCurrentRow][0] = $initDate;
				for ($j=0; $j<$locationsCount; $j++){
					$resultArr[$resultCurrentRow][$j+1] = $countResults[$j];
					$countResults[$j] = 0;
				}
				$resultCurrentRow++;
				//new current day
				$initDate = $date;
			}
			//write data about this day/location for future returning
			$count = mysql_result($result1,$i,'count');
			$location = mysql_result($result1,$i,'tbl_location.location');
			$countResults[$locationsID[$location]] = $count;
		}
		//return data for the last day 
		$resultArr[$resultCurrentRow][0] = $initDate;
		for ($j=0; $j<$locationsCount; $j++){
			$resultArr[$resultCurrentRow][$j+1] = $countResults[$j];;
			$countResults[$j] = 0;
		}
	}
	
	return $resultArr;
}

//read paramters
if (isset($_POST['start_date'])){
	$startDate = $_POST['start_date'];
}
$startDate = "2015-09-18";
if (isset($_POST['stop_date'])){
	$stopDate = $_POST['stop_date'];
}
$stopDate = "2016-01-31";
if (isset($_POST['data_type'])){
	$dataType = $_POST['data_type'];
}
//$dataType = "shows";
$dataType = "clicks";
//$dataType = "CTR";
if (isset($_POST['period_type'])){
	$periodType = $_POST['period_type'];
}
//$periodType = "days";
//$periodType = "weeks";
//$periodType = "months";
$periodType = "hours";
if ($periodType == "hours"){
	$stopDate = $startDate;
}
if (isset($_POST['locations'])){
	$locations = $_POST['locations'];
}

//chech for SQL injections
if(get_magic_quotes_gpc()==1){
	$startDate=stripslashes(trim($startDate));
	$stopDate=stripslashes(trim($stopDate));
	$locations=stripslashes(trim($locations));
} else {
	$startDate=trim($startDate);
	$stopDate=trim($stopDate);
	$locations=trim($locations);
}
$startDate=mysql_real_escape_string($startDate);
$stopDate=mysql_real_escape_string($stopDate);
$locations=mysql_real_escape_string($locations);
$startDate = strip_tags($startDate);
$stopDate = strip_tags($stopDate);
$locations = strip_tags($locations);

$locationsArr = explode("+", $locations);

//get list of locations for bar chart columns
//table to select will depend on data type we need clicks or shows or ctr
if ($dataType == "clicks"){
	$DBTable = "tbl_stat_adv_clicked";
} elseif (($dataType == "shows") || ($dataType == "CTR")) {
	$DBTable = "tbl_stat_adv_shown";
}
//get list of locations for bar chart columns
//table to select will depend on data type we need clicks or shows or ctr
$query = "SELECT tbl_location.location FROM tbl_location 
INNER JOIN {$DBTable} ON {$DBTable}.location_id = tbl_location.id
INNER JOIN tbl_advs ON {$DBTable}.adv_id = tbl_advs.id
INNER JOIN tbl_customers ON tbl_advs.customer_id = tbl_customers.id
INNER JOIN tbl_users ON tbl_users.customer_id = tbl_customers.id
WHERE tbl_users.id = '{$userID}' AND DATE_FORMAT({$DBTable}.time_stamp,'%Y-%m-%d') >= '{$startDate}' AND DATE_FORMAT({$DBTable}.time_stamp,'%Y-%m-%d') <= '{$stopDate}' ";
//if there are specific locations to show in parameters select only them
if (count($locationsArr) > 1){
	$locationsConstr = " AND (tbl_location.location='" . $locationsArr[0] . "'";
	for ($i=1; $i<count($locationsArr)-1; $i++){
		$locationsConstr = $locationsConstr = $locationsConstr . " OR tbl_location.location='" . $locationsArr[$i] . "'";
	}
	$locationsConstr = $locationsConstr . ")";
	$query = $query . $locationsConstr;
}
//
$query = $query . "GROUP BY tbl_location.location
ORDER BY tbl_location.location";

echo $query . "<br><br><br>";

$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
//check there is any data about click for the period 
if (mysql_num_rows($result) != 0) {
	$locationsID = array();
	$locationsCount = mysql_num_rows($result);
	for ($i=0; $i<mysql_num_rows($result); $i++){
		$location = mysql_result($result,$i,'tbl_location.location');
		$locationsID[$location] = $i;
		//return
		echo $location."+";
	}
	echo "<br><br><br>";
	
	if (($dataType == "clicks") || ($dataType == "shows")){
		$chartData = getCountArray($DBTable, $userID, $startDate, $stopDate, $locationsCount, $locationsID, false, $periodType);
		for ($i=0; $i<count($chartData); $i++){
			for ($j=0; $j<count($chartData[$i]); $j++){
				echo $chartData[$i][$j]."+";
			}
			echo "<br>";
		}
	} /* elseif ($dataType == "CTR") {
		$showsData = getCountArray("tbl_stat_adv_shown", $userID, $startDate, $stopDate, $locationsCount, $locationsID, true);
		$clicksData = getCountArray("tbl_stat_adv_clicked", $userID, $startDate, $stopDate, $locationsCount, $locationsID, true);
		$clicksDataCurrentRow = 0;
		echo "<br>";
		echo "<br>";
		echo "<br>";
		for ($i=0; $i<count($showsData); $i++){
			echo $showsData[$i][0]."+";
		}
		echo "<br>";
		echo "<br>";
		echo "<br>";
		for ($i=0; $i<count($clicksData); $i++){
			echo $clicksData[$i][0]."+";
		}
		echo "<br>";
		echo "<br>";
		for ($i=0; $i<count($showsData); $i++){
			echo $showsData[$i][0]."+".$showsData[$i][1]."+";
			if ($clicksData[$clicksDataCurrentRow][0] == $showsData[$i][0]){
				echo $clicksData[$clicksDataCurrentRow][0]."+".$clicksData[$clicksDataCurrentRow][1];
				$clicksDataCurrentRow++;
			}
			echo "<br>";
		} 
	} */
} else {
	//echo "NO RESULTS";
}
mysql_free_result($result);

include ("db_close.php");
?>
SELECT COUNT(tbl_stat_adv_shown.id) AS count, tbl_customers.name AS customer_name
FROM tbl_stat_adv_shown 
INNER JOIN tbl_advs ON tbl_stat_adv_shown.adv_id = tbl_advs.id
INNER JOIN tbl_customers ON tbl_advs.customer_id = tbl_customers.id
INNER JOIN tbl_location ON tbl_stat_adv_shown.location_id = tbl_location.id
INNER JOIN tbl_users_locations ON tbl_users_locations.location_id = tbl_location.id
INNER JOIN tbl_users ON tbl_users.id = tbl_users_locations.user_id
WHERE tbl_users.id = '13' AND DATE_FORMAT(tbl_stat_adv_shown.time_stamp,'%Y-%m-%d') >= '2015-09-15' AND DATE_FORMAT(tbl_stat_adv_shown.time_stamp,'%Y-%m-%d') <= '2016-01-31'
GROUP BY customer_name
ORDER BY customer_name




SELECT COUNT(tbl_stat_adv_shown.id) AS count, tbl_customers.name AS customer_name
FROM tbl_stat_adv_shown 
INNER JOIN tbl_advs ON tbl_stat_adv_shown.adv_id = tbl_advs.id
INNER JOIN tbl_customers ON tbl_advs.customer_id = tbl_customers.id
INNER JOIN tbl_location ON tbl_stat_adv_shown.location_id = tbl_location.id
INNER JOIN tbl_users_locations ON tbl_users_locations.location_id = tbl_location.id
INNER JOIN tbl_users ON tbl_users.id = tbl_users_locations.user_id
WHERE DATE_FORMAT(tbl_stat_adv_shown.time_stamp,'%Y-%m-%d') >= '2015-09-15' AND DATE_FORMAT(tbl_stat_adv_shown.time_stamp,'%Y-%m-%d') <= '2016-01-31'
GROUP BY customer_name
ORDER BY customer_name




SELECT COUNT(tbl_stat_adv_shown.id) AS count, tbl_customers.name AS customer_name, tbl_users.id
FROM tbl_stat_adv_shown 
INNER JOIN tbl_advs ON tbl_stat_adv_shown.adv_id = tbl_advs.id
INNER JOIN tbl_customers ON tbl_advs.customer_id = tbl_customers.id
INNER JOIN tbl_location ON tbl_stat_adv_shown.location_id = tbl_location.id
INNER JOIN tbl_users_locations ON tbl_users_locations.location_id = tbl_location.id
INNER JOIN tbl_users ON tbl_users.id = tbl_users_locations.user_id
WHERE tbl_users.id = '13'
GROUP BY customer_name, tbl_users.id
ORDER BY customer_name, tbl_users.id

<?php
header("Content-Type: text/html; charset=utf-8");
include ("check_session.php");
?>
<html>
  <head>
    <link rel="stylesheet" type="text/css" media="all" href="../css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" media="all" href="../css/chart.css">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="../script/jquery-1.5.2.min.js"></script>
	<script type="text/javascript" src="../script/chart.js"></script>
	<script type="text/javascript" src="../script/jquery-ui.js" charset="utf-8"></script>
	<script type="text/javascript" src="../script/jquery.ui.datepicker-ru.js" charset="utf-8"></script>
	<script type="text/javascript">var pageType = "advertiser";</script>
  </head>
  <body>
	<div id="header">
		<a href="login.php?logout=1">LOGOUT</a>
	</div>
	<div id="middle">
		<div id="control-panel">
			<div id="controls-wrapper">
				<div class="controls-block">
					<div id="period-selector-box">
						<label id="select-period-label">PERIOD:</label>
						<br>
						<select id="select-period" class="chart-control">
							<option value="hours">Hours</option>
							<option value="days" selected>Days</option>
							<option value="weeks">Weeks</option>
							<option value="months">Months</option>
						</select>
					</div>
					<br>
					<div id="date-from-box">
						<label id="select-date-from-label">FROM:</label>
						<br>
						<input readonly id="select-date-from" class="chart-control" type="text" value="<?php echo date('d.m.Y', time() - 6 * 24 * 60 * 60);?>"> 
					</div>
					<br>
					<div id="date-to-box">
						<label id="select-date-to-label">TO:</label> 
						<br>
						<input readonly id="select-date-to" class="chart-control" type="text" value="<?php echo date('d.m.Y');?>">
					</div>
				</div>
				<div class="controls-block">
					<label id="select-locations-label">LOCATIONS:</label> 
					<br>
					<div id="select-locations">
						<table class="filter-table">
							<tbody>
							<?php include ("get_locations_list.php");?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="controls-block">
					<label id="select-tariffs-label">PLANS:</label> 
					<br>
					<div id="select-tariffs">
						<table class="filter-table">
							<tbody>
								<tr data-tariff-id='0'>
									<td><input type='checkbox' checked value='0' class="checkbox-tariff"></input></td>
									<td>Easy start</td>
								</tr>
								<tr data-tariff-id='1'>
									<td><input type='checkbox' checked value='1' class="checkbox-tariff"></input></td>
									<td>Premium</td>
								</tr>
								<tr data-tariff-id='2'>
									<td><input type='checkbox' checked value='2' class="checkbox-tariff"></input></td>
									<td>VIP</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="controls-block">
					<label id="select-advs-label">ADVERTIZEMENTS:</label> 
					<br>
					<div id="select-advs">
						<table class="filter-table">
							<tbody id="advs-list">
							<?php include ("get_advs_list.php");?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div id="content-panel">
			<div id="php-text"></div>
			
			<div id="shows-chart" class="chart-container-column"></div>
			<div id="shows-piechart" class="chart-container-pie"></div>
			
			<div id="clicks-chart" class="chart-container-column"></div>
			<div id="clicks-piechart" class="chart-container-pie"></div>

			<div id="CTR-chart" class="chart-container-column"></div>
			<div id="CTR-piechart" class="chart-container-pie"></div>
			
			<div class="content-panel-delimeter"></div>
			<!--<div id="container-shows-table" class="table-container"></div>
			<div id="container-clicks-table" class="table-container"></div>
			<div id="container-CTR-table" class="table-container"></div>-->
			<div id="container-combined-table" class="table-container"></div>
		</div>
	</div>
  </body>
</html>
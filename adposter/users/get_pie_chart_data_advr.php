<?php
include ("check_session.php");
include ("db_open.php");

//returns array of counts of shows or clicks for every location as defined in arguments 
function getCountArray($DBTable, $userID, $startDate, $stopDate, $allLocations, $locationsArr, $periodType, $advsArr){
	//get data for each location for pie chart
	$query = "SELECT COUNT({$DBTable}.id) AS count, tbl_location.location FROM {$DBTable} 
	INNER JOIN tbl_location ON {$DBTable}.location_id = tbl_location.id
	INNER JOIN tbl_advs ON {$DBTable}.adv_id = tbl_advs.id
	INNER JOIN tbl_customers ON tbl_advs.customer_id = tbl_customers.id
	INNER JOIN tbl_users ON tbl_users.customer_id = tbl_customers.id
	WHERE tbl_users.id = '{$userID}' AND DATE_FORMAT({$DBTable}.time_stamp,'%Y-%m-%d') >= '{$startDate}' AND DATE_FORMAT({$DBTable}.time_stamp,'%Y-%m-%d') <= '{$stopDate}' ";
	//if there are specific locations to show in parameters select only them
	if (count($locationsArr) > 1){
		$locationsConstr = " AND (tbl_location.id='" . $locationsArr[0] . "'";
		for ($i=1; $i<count($locationsArr)-1; $i++){
			$locationsConstr = $locationsConstr . " OR tbl_location.id='" . $locationsArr[$i] . "'";
		}
		$locationsConstr = $locationsConstr . ")";
		$query = $query . $locationsConstr;
	}
	//if there are specific advs to show in parameters select only them
	if (count($advsArr) > 1){
		$advsConstr = " AND (tbl_advs.id='" . $advsArr[0] . "'";
		for ($i=1; $i<count($advsArr)-1; $i++){
			$advsConstr = $advsConstr . " OR tbl_advs.id='" . $advsArr[$i] . "'";
		}
		$advsConstr = $advsConstr . ")";
		$query = $query . $advsConstr;
	}
	//check if need to group data by location or just get totals
	if ($allLocations) {
		//add nothing
	} else {
		$query = $query . "GROUP BY tbl_location.location
		ORDER BY tbl_location.location";
	}
	
	$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
	//result array
	$resultArr = array();
	//check there is any data for the period 
	if (mysql_num_rows($result) != 0) {
		//return data for every location
		for ($i=0; $i<mysql_num_rows($result); $i++){
			$location = mysql_result($result,$i,'tbl_location.location');
			$count = mysql_result($result,$i,'count');
			if (($allLocations) && count($locationsArr) > 2) {
				$resultArr[$i][0] = "All locations";
			} else {
				$resultArr[$i][0] = $location;
			}
			$resultArr[$i][1] = $count;
		}
	} else {
		//echo "NO RESULTS";
	}
	mysql_free_result($result);
	
	return $resultArr;
}

//read paramters
if (isset($_POST['start_date'])){
	$startDate = $_POST['start_date'];
}
//$startDate = "2015-09-15";
if (isset($_POST['stop_date'])){
	$stopDate = $_POST['stop_date'];
}
//$stopDate = "2015-10-28";
if (isset($_POST['data_type'])){
	$dataType = $_POST['data_type'];
}
//$dataType = "shows";
// $dataType = "clicks";
//$dataType = "CTR";
if (isset($_POST['period_type'])){
	$periodType = $_POST['period_type'];
}
//$periodType = "days";
//$periodType = "weeks";
//$periodType = "months";
//$periodType = "hours";
if ($periodType == "hours"){
	$stopDate = $startDate;
}
if (isset($_POST['locations'])){
	$locations = $_POST['locations'];
}
if (isset($_POST['advs'])){
	$advs = $_POST['advs'];
}


//chech for SQL injections
if(get_magic_quotes_gpc()==1){
	$startDate=stripslashes(trim($startDate));
	$stopDate=stripslashes(trim($stopDate));
	$locations=stripslashes(trim($locations));
	$advs=stripslashes(trim($advs));
} else {
	$startDate=trim($startDate);
	$stopDate=trim($stopDate);
	$locations=trim($locations);
	$advs=trim($advs);
}
$startDate=mysql_real_escape_string($startDate);
$stopDate=mysql_real_escape_string($stopDate);
$locations=mysql_real_escape_string($locations);
$advs=mysql_real_escape_string($advs);
$startDate = strip_tags($startDate);
$stopDate = strip_tags($stopDate);
$locations = strip_tags($locations);
$advs = strip_tags($advs);

$startDate = date("Y-m-d", strtotime($startDate));
$stopDate = date("Y-m-d", strtotime($stopDate));
$locationsArr = explode("+", $locations);
$advsArr = explode("+", $advs);

//table to select data from will depend on data type we need clicks or shows or ctr
if ($dataType == "clicks"){
	$DBTable = "tbl_stat_adv_clicked";
} elseif (($dataType == "shows")) {
	$DBTable = "tbl_stat_adv_shown";
}

//for shows or clicks just get data from table
if (($dataType == "clicks") || ($dataType == "shows")){
	$chartData = getCountArray($DBTable, $userID, $startDate, $stopDate, false, $locationsArr, $periodType, $advsArr);
	for ($i=0; $i<count($chartData); $i++){
		echo $chartData[$i][0]."+";
		for ($j=1; $j<count($chartData[$i]); $j++){
			echo $chartData[$i][$j]."+";
		}
		echo "\n";
	}
}
//for CTR have to do calculations
elseif ($dataType == "CTR") {
	$showsData = getCountArray("tbl_stat_adv_shown", $userID, $startDate, $stopDate, true, $locationsArr, $periodType, $advsArr);
	$clicksData = getCountArray("tbl_stat_adv_clicked", $userID, $startDate, $stopDate, true, $locationsArr, $periodType, $advsArr);
	$clicksDataCurrentRow = 0;
	//for each row from shows calculate
	for ($i=0; $i<count($showsData); $i++){
		echo $showsData[$i][0]."+";
		//if there are clicks, do dividing
		if ($clicksData[$clicksDataCurrentRow][0] == $showsData[$i][0]){
			for ($j=1; $j<count($showsData[$i]); $j++){
				if ($showsData[$i][$j] != "0") {
					echo number_format($clicksData[$clicksDataCurrentRow][$j] / $showsData[$i][$j], 4)."+";
				} 
				//or not
				else {
					echo "0+";
				}
			}
			$clicksDataCurrentRow++;
		} 
		//if no clicks then there are just zeros
		else {
			for ($j=1; $j<count($showsData[$i]); $j++){
				echo "0+";
			}
		}
		echo "\n";
	}
}
include ("db_close.php");
?>
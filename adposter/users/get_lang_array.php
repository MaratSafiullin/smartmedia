<?php
include ("db_open.php");

//get user language column ($user_language from the containing file)
if (isset($user_language) && ($user_language != "")  && ($user_language != "eng")){
	$langCol = ", sign_{$user_language}";
} else {
	$langCol = "";
}
 
//query of sing matches for different languages from the db
$query = "SELECT sign_id, sign_eng {$langCol} FROM tbl_lang";

$langArray = array();
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
//generate an array
for ($i=0; $i < mysql_num_rows($result); $i++) {
	$row = mysql_fetch_array($result);
	$signID = $row['sign_id'];
	$engSign = $row['sign_eng'];
	$langArray[$i] = array();
	$langArray[$i]['id'] = $signID;
	$langArray[$i]['eng'] = $engSign;
	if ($langCol != ""){
		$customSign = $row['sign_'.$user_language];
		$langArray[$i][$user_language] = $customSign;
	}
}
mysql_free_result($result);
include ("db_close.php");

//get a sign by ID and language
function getSign($ID, $lang){
	global $langArray;
	for ($i=0; $i < count($langArray); $i++) {
		if ($langArray[$i]['id'] == $ID){
			if (isset($langArray[$i][$lang])){
				$result = $langArray[$i][$lang];
			} else {
				$result = "";
			}
			return $result;
		}
	}
}

//return lang array to JavaScript
function printLangArrayJS(){
	global $langArray;
	global $user_language;
	if (isset($user_language) && ($user_language != "")){
		echo "var userLanguage = '{$user_language}';\n";
	} else {
		echo "var userLanguage = 'eng';\n";
	}
	
	echo "var langArray = [];\n";
	for ($i=0; $i < count($langArray); $i++) {
		echo "langArray[{$i}] = [];\n";
		echo "langArray[{$i}]['id'] = \"{$langArray[$i]['id']}\";\n";
		echo "langArray[{$i}]['eng'] = \"{$langArray[$i]['eng']}\";\n";
		if (isset($user_language) && ($user_language != "")  && ($user_language != "eng")){
			echo "langArray[{$i}]['{$user_language}'] = \"{$langArray[$i][$user_language]}\";\n";
		}
	}
}
?>
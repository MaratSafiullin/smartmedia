<?php
header("Content-Type: text/html; charset=utf-8");

$user_language = 'rus';
include "get_lang_array.php";

/*
Login user using email and password
Or logout
*/
require_once 'access.class.php';
$user = new flexibleAccess();
//logout if there is a get parameter
if ( $_GET['logout'] == 1 ){
    $user->logout('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
}

//if not logged in
if (!$user->is_loaded()) {
	//Login stuff:
    //try to login only if there is email and pass 
    if ( isset($_POST['email']) && isset($_POST['pwd'])){
      if ( !$user->login($_POST['email'],$_POST['pwd'],$_POST['remember'] )){//Mention that we don't have to use addslashes as the class do the job
        echo getSign("wrong_login", $user_language);
      } else {
        header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
      }
    } 
?>
	
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Smart Media Net. Extended analytics ">
    <meta name="keywords" content="WiFi analytics">
    <meta name="author" content="user" >
    <link rel='shortcut icon' type='image/x-icon' href='../favicon.ico' />
    <title><?php echo getSign("login_title", $user_language);?></title>

    <link href="../css/metro.css" rel="stylesheet">
    <link href="../css/metro-icons.css" rel="stylesheet">
    <link href="../css/metro-responsive.css" rel="stylesheet">

    <script src="../script/jquery-2.1.3.min.js"></script>
    <script src="../script/metro.js"></script>
 
    <style>
        .login-form {
            width: 25rem;
            height: 18.75rem;
            position: fixed;
            top: 50%;
            margin-top: -9.375rem;
            left: 50%;
            margin-left: -12.5rem;
            background-color: #ffffff;
            opacity: 0;
            -webkit-transform: scale(.8);
            transform: scale(.8);
        }
    </style>

    <script>
        $(function(){
            var form = $(".login-form");

            form.css({
                opacity: 1,
                "-webkit-transform": "scale(1)",
                "transform": "scale(1)",
                "-webkit-transition": ".5s",
                "transition": ".5s"
            });
        });
    </script>
</head>
<body class="bg-darkTeal">
    <div class="login-form padding20 block-shadow">
        <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" />
            <h1 class="text-light"><?php echo getSign("log_in", $user_language);?></h1>
            <hr class="thin"/>
            <br />
            <div class="input-control text full-size" data-role="input">
                <label for="user_login"><?php echo getSign("email", $user_language);?></label>
                <input type="text" name="email" id="user_login">
                <button class="button helper-button clear"><span class="mif-cross"></span></button>
            </div>
            <br />
            <br />
            <div class="input-control password full-size" data-role="input">
                <label for="user_password"><?php echo getSign("password", $user_language);?></label>
                <input type="password" name="pwd" id="user_password">
                <button class="button helper-button reveal"><span class="mif-looks"></span></button>
            </div>
            <br />
            <br />
            <div class="form-actions">
                <button type="submit" value="login" class="button primary"><?php echo getSign("login_button", $user_language);?></button>
		<br />
                <br />
                <?php echo getSign("remember_me", $user_language);?> <input type="checkbox" name="remember" value="1" /><br /><br />
            </div>
            
        </form>
    </div>

    <!-- hit.ua -->
    <a href='http://hit.ua/?x=136046' target='_blank'>
        <script language="javascript" type="text/javascript"><!--
        Cd=document;Cr="&"+Math.random();Cp="&s=1";
        Cd.cookie="b=b";if(Cd.cookie)Cp+="&c=1";
        Cp+="&t="+(new Date()).getTimezoneOffset();
        if(self!=top)Cp+="&f=1";
        //--></script>
        <script language="javascript1.1" type="text/javascript"><!--
        if(navigator.javaEnabled())Cp+="&j=1";
        //--></script>
        <script language="javascript1.2" type="text/javascript"><!--
        if(typeof(screen)!='undefined')Cp+="&w="+screen.width+"&h="+
        screen.height+"&d="+(screen.colorDepth?screen.colorDepth:screen.pixelDepth);
        //--></script>
        <script language="javascript" type="text/javascript"><!--
        Cd.write("<img src='http://c.hit.ua/hit?i=136046&g=0&x=2"+Cp+Cr+
        "&r="+escape(Cd.referrer)+"&u="+escape(window.location.href)+
        "' border='0' wi"+"dth='1' he"+"ight='1'/>");
        //--></script></a>
    <!-- / hit.ua -->
</body>
</html>
<?php
} else {
  //User is loaded
  //if already logged in, redirect to chart page
  header("Location: dashboard_cust_stat.php");
}
?>
<?
/*
Adding a user to a table with email activation.
*/
require_once 'access.class.php';
$user = new flexibleAccess();

//if already logged in, redirect to chart page
if ($user->is_loaded()){
	header('Location: http://'.$_SERVER['HTTP_HOST']."/adposter/users/chart_partner.php");
}

if (!empty($_GET['activate'])){
	//This is the actual activation. User got the email and clicked on the special link we gave him/her
	$hash = $user->escape($_GET['activate']);
	$res = $user->query("SELECT `{$user->tbFields['active']}` FROM `{$user->dbTable}` WHERE `activation_hash` = '$hash' LIMIT 1",__LINE__);
	
	if ( $rec = mysql_fetch_array($res) ){
		if ( $rec[0] == 1 )
			echo 'Your account is already activated';
		else{
			//Activate the account:
			if ($user->query("UPDATE `{$user->dbTable}` SET `{$user->tbFields['active']}` = 1 WHERE `activation_hash` = '$hash' LIMIT 1", __LINE__))
				//echo 'Account activated. You may login now';
				header('Location: http://'.$_SERVER['HTTP_HOST']."/adposter/users/login.php");
			else
				echo 'Unexpected error. Please contact an administrator';
		}
	}else{
		echo 'User account does not exists';
	}
}

if (!empty($_POST['email'])){
  //Register user:
  
  //Get an activation hash and mail it to the user
  $hash = $user->randomPass(100);
  while( mysql_num_rows($user->query("SELECT * FROM `{$user->dbTable}` WHERE `activation_hash` = '$hash' LIMIT 1"))==1)//We need a unique hash
  	  $hash = $user->randomPass(100);
  //Adding the user. The logic is simple. We need to provide an associative array, where keys are the field names and values are the values :)
  $data = array(
  	'login_name' => '',
  	'email' => $_POST['email'],
  	'password' => $_POST['pwd'],
  	'activation_hash' => $hash,
  	'active' => 0
  );
  $userID = $user->insertUser($data);//The method returns the userID of the new user or 0 if the user is not added
  if ($userID==0)
  	echo 'User not registered';//user is allready registered or something like that
  else {
  	echo 'User registered with user id '.$userID. '. Activate your account using the instructions on your mail.';
  	//Here is a sample mail that user will get:
	$email = "Activate your user account by visiting : https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."?activate=".$hash."\n\n"."This is an auto-generated email. Please do not reply to this message.";
	
	$emailAddr = $_POST['email'];
	$head = "From: noreply@smartmedia.nz\n";
    $head = $head."To: {$emailAddr}\n";
    $head = $head."Subject: SmartMedia: Activate your account\n";
	mail($_POST['email'], 'Activate your account', $email, $head);  
  }
}
?>
<h1>Register</h1>
<p><form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" />
 email: <input type="text" name="email" /><br /><br />
 password: <input type="password" name="pwd" /><br /><br />
 <input type="submit" value="Register user" />
</form>
</p>

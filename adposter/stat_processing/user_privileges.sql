/*This file contains SQL requests to set user privileges for the MySQL user to be able to operate stat data from sniffer only. That user is used by the script running on the log server*/

GRANT SELECT (id, mac_address, stuff)
ON tbl_stat_users
TO smartmed_snfadd;

GRANT INSERT (mac_address)
ON tbl_stat_users
TO smartmed_snfadd;

GRANT SELECT (id, analytics_link)
ON tbl_location
TO smartmed_snfadd;

GRANT INSERT, SELECT, UPDATE
ON tbl_sniff_data
TO smartmed_snfadd;
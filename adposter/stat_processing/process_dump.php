<?php
//saves last step in file processing to recover
function saveLastStep($filename, $execStage){
	@$lsHandle = fopen("last_step", "w");
	@$lsStr = "filename = ".$filename."\n";
	@fwrite($lsHandle, $lsStr);
	@$lsStr = "execStage = ".$execStage."\n";
	@fwrite($lsHandle, $lsStr);
	@fclose($lsHandle);
}
//**************************************************************

echo "\n";
echo "\n";
echo "\n";
echo "****** start\n";
//get last step info (if exists)
@$lastStepInfo = parse_ini_file("last_step");
//
$curDir = getcwd();
echo "****** current dir = ".$curDir."\n";
//read settings
@$settings = parse_ini_file("process_dump.ini");
if (!$settings) exit ("!!! FATAL ERROR !!! CAN'T FIND/READ INI FILE. EXIT");
echo "****** setings:\n";
echo "****** prop_id = {$settings['prop_id']}\n";
echo "****** page = {$settings['page']}\n";
echo "****** location_ui = {$settings['location_ui']}\n";
echo "****** server = {$settings['server']}\n";
echo "****** user = {$settings['user']}\n";
echo "****** password = {$settings['password']}\n";
echo "****** database_name = {$settings['database_name']}\n";
echo "****** session_timeout = {$settings['session_timeout']}\n";
echo "****** min_hits = {$settings['min_hits']}\n";
echo "****** timezone = {$settings['timezone']}\n";
echo "\n";
date_default_timezone_set($settings['timezone']);
//
//infinite loop to check current dir for dump files and process them (every 60 seconds)
while (true){
	echo "*****************************************************************************\n";
	echo "**** checking dir\n";
	$time = date("Y-m-d H:i:s");
	echo "**** time = {$time}\n";
	
	//go through all files (and dirs) in the folder
	$allFiles = scandir($curDir);
	//if recover processing after interrupting the process before processing a dump file was successfully finished that file should be processed first (to finish and delete it)
	//normally it won't happen, the script should just run even if there was a problem until it succeeds
	if ($lastStepInfo){
		echo "** recovering with last step marker\n";
	}
	//
	for ($fileNum = 0; $fileNum < count($allFiles); $fileNum++){
		//get file name
		$filename = $allFiles[$fileNum];
		echo "** filename = ".$filename."\n";
		//current timestamp (if can get it from filename), ignore links to current dir and updir
		if (($filename != ".") && ($filename != "..")){
			$datetimeStr = str_replace("_", " ", $filename);
			$dateStr = substr($datetimeStr, 0, 10);
			$timeStr = substr($datetimeStr, count($datetimeStr)-9, 8);
			$timeStr = str_replace("-", ":", $timeStr);
			$timeStamp = $dateStr." ".$timeStr;
		} 
		else {
			$timeStamp = "";
		}
		//if it is a dump file to process (if not - ingore): 
		$stampToTime = strtotime($timeStamp);
		if ($stampToTime){
			echo "** TO BE PROCESSED:\n";
			echo "** timestamp = {$timeStamp}\n";
			$attempt = 0;
			$procSuccess = false;
			//initial stage of execution. if there is an exception, the execution will continue after a while (30 sec) from the stage it was interrupted
			$execStage = "read_file";
			//def UID for GA requests
			$lastUID = 0;
			//get last UID to process (if was saved)
			if ($lastStepInfo['lastUID'] != "") $lastUID = $lastStepInfo['lastUID'];
			while (!$procSuccess) {
				$attempt++;
				echo "\n\n\nSTART PROCESSING: ATTEMPT {$attempt}\n";
				$time = date("Y-m-d H:i:s");
				echo "time = {$time}\n";
				echo "\n";
				//
				try {
					//go through all stages from the last one it was interrupted
					switch ($execStage){
						case "read_file":
							$macArr = array();
							echo "open file\n";
							//read file with MACs
							@$handle = fopen($filename, "r");
							if ($handle) {
								$index = 0;
								//line by line
								echo "read MACs:\n";
								while (($buffer = fgets($handle)) !== false) {
									$buffer = trim($buffer);
									if ($buffer != ""){
										$macArr[$index]['mac'] = strtoupper($buffer);
										$macArr[$index]['id'] = 0;
										$index++;
										echo $buffer."\n";
									}
								}
								//if cant read the entire file
								if (!feof($handle)) {
									throw new Exception('ERROR! Reading dump file unsuccessful');
								}
								//close
								echo "close file\n";
								fclose($handle);
							} else throw new Exception('ERROR! Cannot open dump file');
							echo "\n";
							// list of MAC addresses
							echo "MAC list:\n";
							for ($i=0; $i < count($macArr); $i++){
								echo $macArr[$i]['mac']."\n";
							}
							echo "\n";
							//
							$execStage = "connect_to_db";
						case "connect_to_db":
							//connect to DB
							echo "connect to DB\n";
							@$link = mysql_connect($settings['server'], $settings['user'], $settings['password']);
							echo mysql_error();
							if (!$link) throw new Exception('ERROR! Cannot connect to DB server');
							@$selRes = mysql_select_db($settings['database_name']);
							if (!$selRes) throw new Exception('ERROR! Cannot find DB');
							//
							$execStage = "check_MACs";
						case "check_MACs":
							//check for new/returned devices (clients)
							//get IDs for returned ones from the base
							echo "check which MACs are already in DB:\n";
							$query  = "SELECT id, mac_address, stuff FROM tbl_stat_users WHERE mac_address = ''";
							for ($i=0; $i < count($macArr); $i++){
								$query = $query . " OR mac_address = '{$macArr[$i]['mac']}'";
							}
							////////////////echo "{$query}\n";
							$result = mysql_query($query);
							if (!$result) throw new Exception('ERROR! Cannot get data from DB');
							for ($i=0; $i<mysql_num_rows($result); $i++){
								$myrow = mysql_fetch_array($result);
								for ($j=0; $j < count($macArr); $j++){
									if (strtoupper ($myrow['mac_address']) == strtoupper ($macArr[$j]['mac'])){
										if ($myrow['stuff'] == 0){
											$macArr[$j]['id'] = $myrow['id'];
											echo "{$macArr[$j]['mac']} {$macArr[$j]['id']}\n";
										} else {
											$macArr[$j]['id'] = -1;
											echo "{$macArr[$j]['mac']} staff_device\n";
										}
									}
								}
							}
							mysql_free_result($result);
							echo "\n";
							//
							$execStage = "add_MACs";
						case "add_MACs":
							//add new devices MACs to the base
							echo "add new MACs to DB:\n";
							$hasNewMACs = false;
							$query  = "SELECT id, mac_address FROM tbl_stat_users WHERE mac_address = ''";
							$queryIns = "INSERT INTO tbl_stat_users (mac_address) VALUES";
							for ($i=0; $i < count($macArr); $i++){
								if ($macArr[$i]['id'] == 0){
									$query = $query . " OR mac_address = '{$macArr[$i]['mac']}'";
									$queryIns = $queryIns . " ('{$macArr[$i]['mac']}'), ";
									$hasNewMACs = true;
									echo "{$macArr[$i]['mac']}\n";
								}
							}
							$queryIns = substr($queryIns, 0, count($queryIns)-3);
							////////////echo "{$queryIns}\n";
							if ($hasNewMACs) $result = mysql_query($queryIns);
							if (!$result) throw new Exception('ERROR! Cannot insert data to DB');
							echo "\n";
							//
							$execStage = "find_added_MACs";
						case "find_added_MACs":
							//get IDs for new devices from the base
							echo "find added MACs in base:\n";
							//////////////echo "{$query}\n";
							$result = mysql_query($query);
							if (!$result) throw new Exception('ERROR! Cannot get data from DB');
							for ($i=0; $i<mysql_num_rows($result); $i++){
								$myrow = mysql_fetch_array($result);
								for ($j=0; $j < count($macArr); $j++){
									if ($myrow['mac_address'] == $macArr[$j]['mac']){
										$macArr[$j]['id'] = $myrow['id'];
										echo "{$macArr[$j]['mac']} {$macArr[$j]['id']}\n";
										break;
									}
								}
							}
							mysql_free_result($result);
							echo "\n";
							//
							$execStage = "get_location_id";
						case "get_location_id":
							//get location ID in the base
							//if cant find, proceed anyway
							$location_id = 'NULL';
							$query  = "SELECT id FROM tbl_location WHERE analytics_link = '{$settings['page']}'";
							///////////echo "{$query}\n";
							$result = mysql_query($query);
							if (mysql_num_rows($result) != 0){
								$myrow = mysql_fetch_array($result);
								$location_id = $myrow['id'];
							}
							echo "location_id = {$location_id}\n";
							echo "\n";
							//
							$execStage = "record_stat_data";
						case "record_stat_data":
							//check for the last stage saved (if any). possibly skip this stage
							if (($lastStepInfo['execStage'] == "record_stat_data") || !$lastStepInfo){
								//add data about devices to the base
								//if couldnt find location ID, there will be NULL, but location UI still present
								echo "add sniff data to DB:\n";
								$hasIDtoRec = false;
								$queryIns = "INSERT INTO tbl_sniff_data (user_id, time_stamp, location_id, location_ui) VALUES";
								for ($i=0; $i < count($macArr); $i++){
									//if not staff MAC address
									if ($macArr[$i]['id'] != -1){
										$hasIDtoRec = true;
										$queryIns = $queryIns . " ('{$macArr[$i]['id']}', '{$timeStamp}', '{$location_id}', '{$settings['location_ui']}'), ";
										echo "user_id = {$macArr[$i]['id']}\n";
									}
								}
								if ($hasIDtoRec){
									$queryIns = substr($queryIns, 0, count($queryIns)-3);
									////////////////echo "\n{$queryIns}\n\n";
									$result = mysql_query($queryIns);
									if (!$result) throw new Exception('ERROR! Cannot add sniffer data to DB');
								}
								echo "\n";
							}
							//
							$execStage = "find_relevant_consequences";
						case "find_relevant_consequences":
							//check for the last stage saved (if any). possibly skip this stage
							if (($lastStepInfo['execStage'] == "find_relevant_consequences") || !$lastStepInfo){
								//check for consequenses of detects (hits) of users, their number in the recent time (depends on the session timeout) should be no less than a given number (defined in ini file) to consider the consequence as relevant - not an occasionally decected device near out area
								echo "find relevant consequences:\n";
								$minRelevantTime = $stampToTime - $settings['session_timeout'] * 60;
								$minRelevantTimestamp = date("Y-m-d H:i:s", $minRelevantTime);
								$minNumHits = $settings['min_hits'];
								//
								$query  = "SELECT MAX(session_id) FROM tbl_sniff_data 
								WHERE location_ui = '{$settings['location_ui']}'";
								$result = mysql_query($query);
								if (!$result) throw new Exception('ERROR! Cannot get max session ID');
								$myrow = mysql_fetch_array($result);
								$maxSessionID = $myrow['MAX(session_id)'];
								if ($maxSessionID == null) $maxSessionID = 0;
								//
								$query  = "SELECT user_id, relevant, session_id FROM tbl_sniff_data 
								WHERE location_ui = '{$settings['location_ui']}' AND 
								time_stamp >= '{$minRelevantTimestamp}' AND time_stamp <= '{$timeStamp}' 
								ORDER BY user_id";
								$result = mysql_query($query);
								if (!$result) throw new Exception('ERROR! Cannot get recent hits information');
								//go through all recent hits
								$numHits = 0;
								$curID = 0;
								$isRelevant = false;
								for ($i=0; $i<mysql_num_rows($result); $i++){
									$myrow = mysql_fetch_array($result);
									//found new ID
									if ($myrow['user_id'] != $curID) {
										$numHits = 1;
										$curID = $myrow['user_id'];
										$isRelevant = false;
										echo "id = ".$curID."\n";
									}
									//
									if (!$isRelevant) {
										//if it's a part of a relevant consequence or we get enough hits in recent time, mark hit's for this user ID as relevant
										if ($myrow['relevant'] || ($numHits >= $minNumHits)){
											$isRelevant = true;
											echo "relevant {$myrow['user_id']}\n";
											if ($myrow['relevant']){
												$curSessionID = $myrow['session_id'];
											} else {
												$maxSessionID++;
												$curSessionID = $maxSessionID;
											}
											$queryUpd = "UPDATE tbl_sniff_data 
											SET relevant = '1', session_id = '{$curSessionID}'
											WHERE location_ui = '{$settings['location_ui']}' AND 
											user_id = {$myrow['user_id']} AND 
											time_stamp >= '{$minRelevantTimestamp}' AND
											time_stamp <= '{$timeStamp}'";
											///////////////echo "\n{$queryUpd}\n\n";
											$resultUpd = mysql_query($queryUpd);
											if (!$resultUpd) throw new Exception('ERROR! Cannot mark hits as relevant');
										}
										$numHits++;
									}
								}
								echo "mark deprecated consequences as irrelevant\n";
								$queryUpd = "UPDATE tbl_sniff_data SET relevant = '0' 
								WHERE location_ui = '{$settings['location_ui']}' AND relevant IS NULL AND
								time_stamp < '{$minRelevantTimestamp}'";
								///////////////echo "\n{$queryUpd}\n\n";
								$resultUpd = mysql_query($queryUpd);
								if (!$resultUpd) throw new Exception('ERROR! Cannot mark deprecated hits as irrelevant');
							}
							echo "\n";
							//
							$execStage = "finish";
						case "finish":
							//delete dump file
							echo "delete dump file\n";
							$delRes = unlink($filename);
							if (!$delRes) throw new Exception('ERROR! Cannot delete dump file');
							//delete last step marker (if exists)
							@unlink("last_step");
							$lastStepInfo = false;
							//
							mysql_close($link);
							$procSuccess = true;
							echo "SUCCESS\n";
							$time = date("Y-m-d H:i:s");
							echo "time = {$time}\n";
							echo "\n";
							echo "\n";
							echo "\n";
						default:
					}
				}
				catch (Exception $e){
					//if there was an exception try to finish the procees in a while (from the last stage)
					
					//save last step info to recover next time if the script is interrupted before success
					@saveLastStep($filename, $execStage);
					//
					$procSuccess = false;
					echo $e->getMessage(), "\n";
					echo mysql_error()."\n";
					sleep(30);
				}
			}
		}
	}

	//sendind hits to GA
	//cant be fully checked, so just hope it goes well
	try{
		echo "**** send GA hits\n";
		//connect to DB
		echo "connect to DB\n";
		@$link = mysql_connect($settings['server'], $settings['user'], $settings['password']);
		echo mysql_error();
		if (!$link) throw new Exception('ERROR! Cannot connect to DB server');
		@$selRes = mysql_select_db($settings['database_name']);
		if (!$selRes) throw new Exception('ERROR! Cannot find DB');
		//get users list
		$userIDsHitsArray = array();
		$query  = "SELECT DISTINCT user_id FROM tbl_sniff_data 
		WHERE location_ui = '{$settings['location_ui']}' AND relevant = '1' AND ga_hit_time IS NULL 
		ORDER BY user_id";
		//////////echo "\n{$query}\n\n";
		$result = mysql_query($query);
		if (!$result) throw new Exception('ERROR! Cannot get user IDs for ga hits');
		echo "user IDs for current sessions (have hits to send):\n";
		for ($i=0; $i<mysql_num_rows($result); $i++){
			$myrow = mysql_fetch_array($result);
			$userIDsHitsArray[$i] = $myrow['user_id'];
			echo "{$myrow['user_id']}\n";
		}
		echo "send hits:\n";
		//for each user
		for ($i=0; $i<count($userIDsHitsArray); $i++){
			//find records
			$query  = "SELECT session_id, time_stamp, ga_hit_time FROM tbl_sniff_data 
			WHERE location_ui = '{$settings['location_ui']}' AND user_id = '{$userIDsHitsArray[$i]}' AND
			relevant = '1'
			ORDER BY time_stamp";
			$result = mysql_query($query);
			if (!$result) throw new Exception("ERROR! Cannot get sessions for user {$userIDsHitsArray[$i]}");
			//go through records
			$j = 0;
			$curSessionID = null;
			$curSessionStartTimeStamp = null;
			$curSessionStartGAHitTime = null;
			//find the last commited GA hit, and it's session params
			while ($j<mysql_num_rows($result)){
				$myrow = mysql_fetch_array($result);
				if (($myrow['ga_hit_time'] == null)){
					$j++;
					break;
				} else {
					if (($myrow['session_id'] != $curSessionID)){
						$curSessionID = $myrow['session_id'];
						$curSessionStartTimeStamp = $myrow['time_stamp'];
						$curSessionStartGAHitTime = strtotime($myrow['ga_hit_time']);
					}
					$lastSentRecTimestamp = $myrow['time_stamp'];
					$lastGAHitTime = strtotime($myrow['ga_hit_time']);
					$j++;
				}
			}
			//found the last unsent hit
			$curTime = time();
			//many ways to deal with it
			if ($j<=mysql_num_rows($result)){
				//if it's the very first record to send hit
				if (($curSessionID == null)){
					sendGAHit($userIDsHitsArray[$i]);
					$curHitTimestamp = date("Y-m-d H:i:s", $curTime);
					$queryUpd = "UPDATE tbl_sniff_data 
					SET ga_hit_time = '{$curHitTimestamp}'
					WHERE location_ui = '{$settings['location_ui']}' AND user_id = {$userIDsHitsArray[$i]} AND
					session_id = '{$myrow['session_id']}' AND time_stamp = '{$myrow['time_stamp']}' AND
					ga_hit_time IS NULL";
					////////////echo "\n{$queryUpd}\n\n";
					$resultUpd = mysql_query($queryUpd);
					if (!$resultUpd) throw new Exception('ERROR! Cannot mark sent GA hit');
				} 
				//if there was a GA hit sent before
				else{
					$timeDiff = $curTime - $lastGAHitTime;
					//if the current GA(!) session is still running
					if ($timeDiff < ($settings['session_timeout'] - 1) * 60){
						//find the most recent suitable record of this session and sent it's hit to GA
						//sutability depends on time difference between time stamps (from sniff log!) of a record and start of session and time difference between GA(!) hits of session start and considered record
						$timeDiffStart = $curTime - $curSessionStartGAHitTime;
						$curSessionStartTime = strtotime($curSessionStartTimeStamp);
						//go through records
						$lastSuitableRecTimestamp = null;
						if ($myrow['session_id'] == $curSessionID) {
							$lastRecTimestamp = $myrow['time_stamp'];
							$lastRecTime = strtotime($lastRecTimestamp);
							if (($lastRecTime - $curSessionStartTime) <= $timeDiffStart){
								$lastSuitableRecTimestamp = $lastRecTimestamp;
							}
						}
						while ($j<mysql_num_rows($result)){
							$myrow = mysql_fetch_array($result);
								if ($myrow['session_id'] == $curSessionID) {
									$lastRecTimestamp = $myrow['time_stamp'];
									$lastRecTime = strtotime($lastRecTimestamp);
									if (($lastRecTime - $curSessionStartTime) <= $timeDiffStart){
										$lastSuitableRecTimestamp = $lastRecTimestamp;
									} else {
										$j++;
										break;
									}
								} else {
									$j++;
									break;
								}
							$j++;
						}
						if ($lastSuitableRecTimestamp != null){
							sendGAHit($userIDsHitsArray[$i]);
							$curHitTimestamp = date("Y-m-d H:i:s", $curTime);
							$queryUpd = "UPDATE tbl_sniff_data 
							SET ga_hit_time = '{$curHitTimestamp}'
							WHERE location_ui = '{$settings['location_ui']}' AND user_id = {$userIDsHitsArray[$i]} AND
							session_id = '{$curSessionID}' AND time_stamp <= '{$lastSuitableRecTimestamp}' AND
							ga_hit_time IS NULL";
							////////////echo "\n{$queryUpd}\n\n";
							$resultUpd = mysql_query($queryUpd);
							if (!$resultUpd) throw new Exception('ERROR! Cannot mark sent GA hits');
						}
					}
					//if the current GA(!) session is finished
					elseif ($timeDiff > ($settings['session_timeout'] + 1) * 60){
						//if user session wasn't fully sent to GA
						//resend last sent record of this session to continue sending it in further loop iterations
						//that will allow to keep the overall sessions duration correct (although the number of sessions will be incorrect (nothing is perfect))
						if ($myrow['session_id'] == $curSessionID) {
							sendGAHit($userIDsHitsArray[$i]);
							$curHitTimestamp = date("Y-m-d H:i:s", $curTime);
							$queryUpd = "UPDATE tbl_sniff_data 
							SET ga_hit_time = '{$curHitTimestamp}'
							WHERE location_ui = '{$settings['location_ui']}' AND user_id = {$userIDsHitsArray[$i]} AND
							session_id = '{$curSessionID}' AND time_stamp = '{$lastSentRecTimestamp}'";
							////////////echo "\n{$queryUpd}\n\n";
							$resultUpd = mysql_query($queryUpd);
							if (!$resultUpd) throw new Exception('ERROR! Cannot remark sent GA hit');
						}
						//else just send the first hit of the next session
						else {
							sendGAHit($userIDsHitsArray[$i]);
							$curHitTimestamp = date("Y-m-d H:i:s", $curTime);
							$queryUpd = "UPDATE tbl_sniff_data 
							SET ga_hit_time = '{$curHitTimestamp}'
							WHERE location_ui = '{$settings['location_ui']}' AND user_id = {$userIDsHitsArray[$i]} AND
							session_id = '{$myrow['session_id']}' AND time_stamp = '{$myrow['time_stamp']}'AND
							ga_hit_time IS NULL";
							////////////echo "\n{$queryUpd}\n\n";
							$resultUpd = mysql_query($queryUpd);
							if (!$resultUpd) throw new Exception('ERROR! Cannot mark sent GA hit');
						}
					}
				}
			}
		}
		mysql_close($link);
	}
	catch (Exception $e){
		echo $e->getMessage(), "\n";
		echo mysql_error()."\n";
	}
	echo "**** sleep\n";
	sleep(30);
}

//send hit to GA
function sendGAHit($userID){
	//used to check the respond from GA
	$gaStdRespond = "GIF89a";
	//
	global $settings;
	$data = array (
		'v' => '1', 
		't' => 'pageview',
		'tid' => $settings['prop_id'],
		'cid' => $userID,
		'uid' => $userID,
		'dl' => $settings['page']."/offline"
	);
	
	$query  = http_build_query($data);
	echo "id = " . $userID . "\n";
	echo $query . "\n";

	$context = stream_context_create(array(
		'http' => array(
			'method' => 'POST',
			'header' => 'Content-Type: application/x-www-form-urlencoded' . PHP_EOL,
			'content' => $query
		),
	));

	@$gaRespond = file_get_contents(
		$file = "http://www.google-analytics.com/collect",
		$use_include_path = false,
		$context
	);
	if (substr_count($gaRespond, $gaStdRespond) == 0) throw new Exception('ERROR! Cant get respond from GA');
}
?>
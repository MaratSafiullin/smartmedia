<?php
//adding new record to sessions table
function addSessionRecord($locationID, $userID, $startTimestamp, $stopTimestamp, $timeDiff){
	global $connection;
	$queryIns = "INSERT INTO tbl_sessions (location_id, user_id, start_time_stamp, stop_time_stamp, time_diff)
	VALUES ('{$locationID}', '{$userID}', '{$startTimestamp}', '{$stopTimestamp}', '{$timeDiff}')";
	$result = mysqli_query($connection, $queryIns);
	///////////echo ($connection->error)."\n";
	if (!$result) throw new Exception('ERROR! Cannot add session record');
}

//deleting records from sniff data table
function delSniffRecords($locationID, $userID = null, $sessionID = null, $minID = null, $maxID = null){
	global $connection;
	if ($sessionID != null){
		$queryDel = "DELETE FROM tbl_sniff_data
		WHERE location_id = '{$locationID}' AND user_id = '{$userID}' AND session_id = '{$sessionID}'";
	} else {
		$queryDel = "DELETE FROM tbl_sniff_data
		WHERE location_id = '{$locationID}' AND id >= '{$minID}' AND id <= '{$maxID}'";
	}
	$result = mysqli_query($connection, $queryDel);
	////////echo ($connection->error)."\n";
	if (!$result) throw new Exception('ERROR! Cannot delete sniff records');
}
//*********************************************************

echo "****** start\n";
//read settings
@$settings = parse_ini_file("calc_sessions.ini");
if (!$settings) exit ("!!! FATAL ERROR !!! CAN'T FIND/READ INI FILE. EXIT");
echo "****** setings:\n";
echo "****** server = {$settings['server']}\n";
echo "****** user = {$settings['user']}\n";
echo "****** password = {$settings['password']}\n";
echo "****** database_name = {$settings['database_name']}\n";
echo "****** timezone = {$settings['timezone']}\n";
echo "\n";
date_default_timezone_set($settings['timezone']);

//infinite loop, check every 10 minutes if there is time to process records for some locations
while (true){
	echo "\n";
	$time = date("Y-m-d H:i:s");
	echo "**** time = {$time}\n";
	$hour = date("H");
	echo "**** find locations to process sniff data\n";
	try {
		//connect to DB server
		$connection = mysqli_connect($settings['server'], $settings['user'], $settings['password'], $settings['database_name']);
		if ($connection->connect_error) throw new Exception('ERROR! Cannot connect to DB server');
		$query = "SELECT id, location, calc_sessions_start_hour, calc_sessions_stop_hour FROM tbl_location
		WHERE calc_sessions_start_hour <= '{$hour}' AND calc_sessions_stop_hour > '{$hour}' AND
		(calc_sessions_start_hour <> '0' OR calc_sessions_stop_hour <> '0')";
		$result = mysqli_query($connection, $query);
		if (!$result) throw new Exception('ERROR! Cannot get list of locations');
		$locationsArr = array();
		$ind = 0;
		while ($row = mysqli_fetch_array($result)){
			echo "{$row['location']} (from: {$row['calc_sessions_start_hour']} to: {$row['calc_sessions_stop_hour']}) ";
			$queryLoc = "SELECT id FROM tbl_sniff_data 
			WHERE location_id = '{$row['id']}' AND relevant = '1' AND ga_hit_time IS NULL";
			$resultLoc = mysqli_query($connection, $queryLoc);
			if (!$resultLoc) throw new Exception('ERROR! Cannot check session GA hits');
			if ($resultLoc->num_rows != 0) {
				echo "still has unsent GA hits\n";
			} else {
				echo "ready to process\n";
				$locationsArr[$ind] = array();
				$locationsArr[$ind]['location'] = $row['location'];
				$locationsArr[$ind]['id'] = $row['id'];
				$ind++;
			}
		}
	}
	catch (Exception $e){
		//if there was an exception try to finish the procees in a while
		$procSuccess = false;
		echo $e->getMessage(), "\n";
		echo ($connection->error)."\n";
		sleep(30);
	}
	//process if yes
	echo "**** process sniff data\n";
	for ($locInd = 0; $locInd < count($locationsArr); $locInd++){//if (false){   // !!!!!!!!!!!!!!!!!!!!!
		echo "** location = {$locationsArr[$locInd]['location']}\n";
		$locationID = $locationsArr[$locInd]['id'];
		$procSuccess = false;
		while (!$procSuccess) {
			try {
				//connect to DB server
				$connection = mysqli_connect($settings['server'], $settings['user'], $settings['password'], $settings['database_name']);
				if ($connection->connect_error) throw new Exception('ERROR! Cannot connect to DB server');
				
				//check for records
				echo "get min and max IDs\n";
				$query = "SELECT id FROM tbl_sniff_data WHERE location_id = '{$locationID}'";
				$result = mysqli_query($connection, $query);
				if (!$result) throw new Exception('ERROR! Cannot get rows from sniff data table');
				//if there are
				if ($result->num_rows != 0) {
					//get IDs diapason to process
					$query = "SELECT MIN(id) FROM tbl_sniff_data WHERE location_id = '{$locationID}'";
					$result = mysqli_query($connection, $query);
					if (!$result) throw new Exception('ERROR! Cannot get rows from sniff data table');
					$row = mysqli_fetch_array($result);
					$minID = $row['MIN(id)'];
					echo "min ID = {$minID}\n";
					$query = "SELECT MAX(id) FROM tbl_sniff_data WHERE location_id = '{$locationID}'";
					$result = mysqli_query($connection, $query);
					if (!$result) throw new Exception('ERROR! Cannot get rows from sniff data table');
					$row = mysqli_fetch_array($result);
					$maxID = $row['MAX(id)'];
					echo "max ID = {$maxID}\n";
					
					//get records
					$query = "SELECT user_id, MIN(time_stamp), MAX(time_stamp), location_id, session_id FROM tbl_sniff_data 
					WHERE id >= '{$minID}' AND id <= '{$maxID}' AND location_id = '{$locationID}'  AND relevant = '1'
					GROUP BY location_id, user_id, session_id
					ORDER BY location_id, user_id, session_id";
					$result = mysqli_query($connection, $query);
					///////////echo ($connection->error)."\n";
					if (!$result) throw new Exception('ERROR! Cannot get rows from sniff data table');
					//process records to get sessions from there
					echo "find sessions\n";
					while ($row = mysqli_fetch_array($result)){
						$duration = strtotime($row['MAX(time_stamp)']) - strtotime($row['MIN(time_stamp)']);
						echo "user = {$row['user_id']}, session = {$row['session_id']}, start:{$row['MIN(time_stamp)']}, finish:{$row['MAX(time_stamp)']}, duration: {$duration}\n";
						addSessionRecord($locationID, $row['user_id'], $row['MIN(time_stamp)'], $row['MAX(time_stamp)'], $duration);
						delSniffRecords($locationID, $row['user_id'], $row['session_id']);
					}
					echo "delete irrelevent records\n";
					delSniffRecords($locationID, null, null, $minID, $maxID);
					echo "------------------------\n";
				} 
				//if no records in the table
				else {
					echo "no records\n";
				}
				
				//finished successfully
				$procSuccess = true;
			}
			catch (Exception $e){
				//if there was an exception try to finish the procees in a while
				$procSuccess = false;
				echo $e->getMessage(), "\n";
				echo ($connection->error)."\n";
				sleep(30);
			}
		}
	}
	sleep(60*10);
}

























?>
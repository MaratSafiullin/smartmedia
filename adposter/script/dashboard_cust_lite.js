//loading some google stuff
google.load('visualization', '1', {packages: ['corechart', 'bar']});

//*****************************************************************************************************************
// Functions for a-charts page, display info from g-storage
//*****************************************************************************************************************

//flags to check vidgets adgustments completion (change language, format numbers)
var vidgetAdgusted = [];

//????????????????????????????????????????
(function(w,d,s,g,js,fjs){
	g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
	js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
	js.src='https://apis.google.com/js/platform.js';
	fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
}(window,document,'script'));

//*****************************************************************************************************************

//when loaded a-library (if it's a page with a-charts)
gapi.analytics.ready(function() {
	var queriesToLoad = 0;
	var queriesLoaded = 0;
	var startDate;
	var stopDate;
			
	if (pageType == "dashboard_customers_statistics"){
		
		//authorisation
		if (accessToken != ""){
			//use token from php script to get access to data
			gapi.analytics.auth.authorize({
				'serverAuth': {
					'access_token': accessToken
				}
			});
		}
		else {
			// !!!!Used only for testing purposes when we don't get token by php library
			var CLIENT_ID = '885338747009-ija5r7g048nb3cu2pnt3u4n8cabtj493.apps.googleusercontent.com';
			if (!gapi.analytics.auth.isAuthorized()) {
				gapi.analytics.auth.authorize({
					container: 'auth-button',
					clientid: CLIENT_ID,
				});
			}
		}

		//creating different GA api elements to get and show GA statistics data to user
		//***********************************************************************************************

		//small vidgets with total numbers (one number) for the top of the overview page
		//
		queriesToLoad++;
		var visitsTotalReport = new gapi.analytics.report.Data({
		  query: {
			metrics: 'ga:sessions',
		  }
		});
		visitsTotalReport.on('success', function(response) {
		  queriesLoaded++;
		  $("#sessions-total .content").empty();
		  $("#sessions-total .content").append(response.rows[0][0]);
		  hideLoadIndicator();
		});
		visitsTotalReport.on('error', function(response) {
			if ((response.error.code == 403) && (response.error.message.indexOf("Quota Error") != -1)){
				setTimeout(visitsTotalReport.execute(), 500);
				event.stopPropagation();
			} else if (response.error.code == 400) {
				queriesLoaded++;
				hideLoadIndicator(); 
			}
		});
		
		//
		queriesToLoad++;
		var visitsNewOldTotalReport = new gapi.analytics.report.Data({
		  query: {
			metrics: 'ga:sessions',
			dimensions: 'ga:userType'
		  }
		});
		visitsNewOldTotalReport.on('success', function(response) {
			queriesLoaded++;
			$("#new-sessions-total .content").empty();
			$("#old-sessions-total .content").empty();
			$("#new-sessions-total .content").append(response.rows[0][1]);
			$("#old-sessions-total .content").append(response.rows[1][1]);
			hideLoadIndicator();
		});
		visitsNewOldTotalReport.on('error', function(response) {
			if ((response.error.code == 403) && (response.error.message.indexOf("Quota Error") != -1)){
				setTimeout(visitsNewOldTotalReport.execute(), 500);
				event.stopPropagation();
			} else if (response.error.code == 400) {
				queriesLoaded++;
				hideLoadIndicator();
			}
		});
		
		//
		queriesToLoad++;
		var usersTotalReport = new gapi.analytics.report.Data({
			query: {
				metrics: 'ga:users'
			}
		});
		usersTotalReport.on('success', function(response) {
			queriesLoaded++;
			$("#users-total .content").empty();
			$("#users-total .content").append(response.rows[0][0]);
			hideLoadIndicator();
		});
		usersTotalReport.on('error', function(response) {
			if ((response.error.code == 403) && (response.error.message.indexOf("Quota Error") != -1)){
				setTimeout(usersTotalReport.execute(), 500);
				event.stopPropagation();
			} else if (response.error.code == 400) {
				queriesLoaded++;
				hideLoadIndicator();
			}
		});
		
		//
		queriesToLoad++;
		var sessionAvgTimeTotalReport = new gapi.analytics.report.Data({
		query: {
			metrics: 'ga:avgSessionDuration'
		}
		});
		sessionAvgTimeTotalReport.on('success', function(response) {
			queriesLoaded++;
			$("#session-avgtime-total .content").empty();
			var AvgDurationMinutes = Math.round(response.rows[0][0] / 60);
			$("#session-avgtime-total .content").append(AvgDurationMinutes);
			hideLoadIndicator();
		});
		sessionAvgTimeTotalReport.on('error', function(response) {
			if ((response.error.code == 403) && (response.error.message.indexOf("Quota Error") != -1)){
				setTimeout(sessionAvgTimeTotalReport.execute(), 500);
				event.stopPropagation();
			} else if (response.error.code == 400) {
				queriesLoaded++;
				hideLoadIndicator();
			}
		});
		
		
		//GA charts and related functions
		//***********************************************************************************************
		//functions to change/format original charts by GA, to make it look our way
		
		//change language for data object labels (and eventually for charts)
		function dataChangeLang(data){
			//columns labels
			for (i=0; i < data.cols.length; i++){
				for (l=0; l < langArray.length; l++){
					if (data.cols[i].label.toLowerCase() == langArray[l]["id"].toLowerCase()){
						data.cols[i].label = langArray[l][userLanguage];
						break;
					}
				}
			}
			//values
			if (data.rows != null){
				for (i=0; i < data.rows.length; i++){
					var c = data.rows[i].c;
					for (j=0; j < c.length; j++){
						if (typeof(c[j].v) == "string"){
							for (l=0; l < langArray.length; l++){
								if (c[j].v.toLowerCase() == langArray[l]["id"].toLowerCase()){
									c[j].v = langArray[l][userLanguage];
									break;
								}
							}
						}
					}
				}
			}
		}
		
		//format data objects cols and rows to show date/time values in a propper format
		//e.g. start-end dates for a week of the year, month name for a month, show date and time for date-hour value
		//so it makes charts more readable
		function dataFormatDateTime(data){
			if (data.cols[0].id == "ga:dateHour"){
				data.cols[0].type = "datetime";
			}
			
			//number of month to it's name
			function num2month(num) {
				var month = [getSign("january"),getSign("february"),getSign("march"),getSign("april"),getSign("may"),getSign("june"),getSign("july"),getSign("august"),getSign("september"),getSign("october"),getSign("november"),getSign("december")];
				return month[num];
			}
			if (data.cols[0].id == "ga:yearMonth"){
				for (i=0; i < data.rows.length; i++){
					var c = data.rows[i].c;
					if ((c[0].v != null) && (c[0].v != "")) c[0].v = num2month(Number(c[0].v.substr(4, 2))-1);
				}
			}
			
			//get date by year, number of the week, number of the day
			function dateForDayOfWeekOfYear(year, week, day){
				// первый день года 
				var d1 = new Date(year, 0, 1);
				// его день недели
				var firstDay = d1.getDay();
				// день в году
				var days = firstDay == day 
					? 1 
					: (week) * 7 + day - (firstDay - 1);
				// дата для указанного дня недели
				var dn = new Date(year, 0, days);
				return dn;
			}
			if (data.cols[0].id == "ga:isoYearIsoWeek"){
				for (i=0; i < data.rows.length; i++){
					var c = data.rows[i].c;
					if (c[0].v != null){
						var year = Number(c[0].v.substr(0,4))
						var week = Number(c[0].v.substr(4,2)) - 1;
						c[0].v = dateForDayOfWeekOfYear(year, week, 1).toLocaleDateString() +
						" -\n" + dateForDayOfWeekOfYear(year, week, 7).toLocaleDateString();
					}
				}
			}
		}
		
		//round duration in seconds to minutes (for session duration charts)
		function roundDurationSecsToMins(data){
			for (i=0; i < data.rows.length; i++){
				var c = data.rows[i].c;
				var valIndex = c.length-1;
				if (typeof(c[valIndex].v) == "number"){
					c[valIndex].v = Math.round(c[valIndex].v / 60);
				}
			}
		}
		
		//transform table with gendeer statistics to make columns of male, female and total
		//looks much better, ha?
		function transformedGenderTable(data){
			//4 columns
			var newData = {
				cols: []
			};
			newData.cols[0] = data.cols[0];
			var fCol = {
				id: "female",
				label: "Female",
				type: "number"
			};
			var mCol = {
				id: "male",
				label: "Male",
				type: "number"
			};
			var tCol = {
				id: "total",
				label: "Total",
				type: "number"
			};
			newData.cols[1] = fCol;
			newData.cols[2] = mCol;
			newData.cols[3] = tCol;
			//go through rows of original data, make 1 row of each 2 or original (usually)
			//if there is only one gender number for some date the other gender number will be 0
			if (data.rows != null){
				var totalF = 0;
				var totalM = 0;
				var totalT = 0;
				newData.rows = [];
				var i = 0;
				while (i < data.rows.length){
					var newC = {
						c: []
					};
					var vF = 0;
					var vM = 0;
					//get numbers for males and females (no matter in which order they are)
					if (data.rows[i].c[1].v.toLowerCase() == "female"){
						vF = data.rows[i].c[2].v;
					} else {
						vM = data.rows[i].c[2].v;
					}//check that there are two rows with the same date (or skip assignment of value)
					if (i < data.rows.length-1){
						if ((data.rows[i+1].c[0].v).toLocaleString() == (data.rows[i].c[0].v).toLocaleString()){
							if (data.rows[i+1].c[1].v.toLowerCase() == "female"){
								vF = data.rows[i+1].c[2].v;
							} else {
								vM = data.rows[i+1].c[2].v;
							}
							i++;
						}
					}
					//
					totalF = totalF + vF;
					totalM = totalM + vM;
					totalT = totalT + vF + vM;
					//new row for new data object
					newC.c[0] = data.rows[i].c[0];
					newC.c[1] = {v: vF};
					newC.c[2] = {v: vM};
					newC.c[3] = {v: vF+vM};
					newData.rows.push(newC);
					//
					i++;
				}
				var newC = {
					c: []
				};
				newC.c[0] = {v: null};
				newC.c[1] = {v: totalF};
				newC.c[2] = {v: totalM};
				newC.c[3] = {v: totalT};
				newData.rows.push(newC);
			}
			return newData; 
		}
		
		//transform table with age statistics to make columns of diffrent age brackets and total column
		//looks much better, ha?
		function transformedAgeTable(data){
			//4 columns
			var newData = {
				cols: []
			};
			newData.cols[0] = data.cols[0];
			var col18 = {
				id: "18-24",
				label: "18-24",
				type: "number"
			};
			var col25 = {
				id: "25-34",
				label: "25-34",
				type: "number"
			};
			var col35 = {
				id: "35-44",
				label: "35-44",
				type: "number"
			};
			var col45 = {
				id: "45-54",
				label: "45-54",
				type: "number"
			};
			var col55 = {
				id: "55-64",
				label: "55-64",
				type: "number"
			};
			var col65 = {
				id: "65+",
				label: "65+",
				type: "number"
			};
			var tCol = {
				id: "total",
				label: "Total",
				type: "number"
			};
			newData.cols[1] = col18;
			newData.cols[2] = col25;
			newData.cols[3] = col35;
			newData.cols[4] = col45;
			newData.cols[5] = col55;
			newData.cols[6] = col65;
			newData.cols[7] = tCol;
			//go through rows of original data, make 1 row of each 6 or original (usually)
			//if there are some genders missing for the date the number for them will be 0
			if (data.rows != null){
				var total18 = 0;
				var total25 = 0;
				var total35 = 0;
				var total45 = 0;
				var total55 = 0;
				var total65 = 0;
				var totalT = 0;
				newData.rows = [];
				var i = 0;
				while (i < data.rows.length){
					var newC = {
						c: []
					};
					var v18 = 0;
					var v25 = 0;
					var v35 = 0;
					var v45 = 0;
					var v55 = 0;
					var v65 = 0;
					//get numbers for all age brackets (no matter in which order they are)
					//check that original rows have the same date, otherwise proceed to a new formatted row
					startI = i;
					for (n=0; n < 6; n++){
						if (i < data.rows.length){
							if ((data.rows[i].c[0].v).toLocaleString() == (data.rows[startI].c[0].v).toLocaleString()){
								switch (data.rows[i].c[1].v.toLowerCase()){
									case "18-24":
										v18 = data.rows[i].c[2].v;
									break;
									case "25-34":
										v25 = data.rows[i].c[2].v;
									break;
									case "35-44":
										v35 = data.rows[i].c[2].v;
									break;
									case "45-54":
										v45 = data.rows[i].c[2].v;
									break;
									case "55-64":
										v55 = data.rows[i].c[2].v;
									break;
									case "65+":
										v65 = data.rows[i].c[2].v;
									break;
								}
								i++;
							} //else {break;}
						} //else {break;}
					}
					//
					total18 = total18 + v18;
					total25 = total25 + v25;
					total35 = total35 + v35;
					total45 = total45 + v45;
					total55 = total55 + v55;
					total65 = total65 + v65;
					totalT = totalT + v18 + v25 + v35 + v45 + v55 + v65;
					//new row for new data object
					newC.c[0] = data.rows[startI].c[0];
					newC.c[1] = {v: v18};
					newC.c[2] = {v: v25};
					newC.c[3] = {v: v35};
					newC.c[4] = {v: v45};
					newC.c[5] = {v: v55};
					newC.c[6] = {v: v65};
					newC.c[7] = {v: v18+v25+v35+v45+v55+v65};
					newData.rows.push(newC);
				}
				var newC = {
					c: []
				};
				newC.c[0] = {v: null};
				newC.c[1] = {v: total18};
				newC.c[2] = {v: total25};
				newC.c[3] = {v: total35};
				newC.c[4] = {v: total45};
				newC.c[5] = {v: total55};
				newC.c[6] = {v: total65};
				newC.c[7] = {v: totalT};
				newData.rows.push(newC);
			}
			return newData; 
		}
		
		//creating a chart vidget (used to unify the process), can be pie, column, table, line...
		function newGAChart(chartName, type, container, metrics, dimensions, sort, maxResults){
			queriesToLoad++;
			var chartOptions = {
				width: '100%',
				legend: {
				},
				hAxis:{
				},
				vAxis: {
				},
				chartArea: {
				},
				annotations: {
				}
			}
			if (type == "PIE"){
				chartOptions.pieHole = 4/9;
			}
			if ((type == "BAR") || (type == "COLUMN") || (type == "LINE")){
				chartOptions.legend.position = "top";
			}
			if (type == "LINE") {
				chartOptions.pointSize = 3;
			}
			if ((chartName == "interestsGeneralBarChart") || (chartName == "interestsCommercBarChart")){
				chartOptions.height = maxResults*30;
				chartOptions.chartArea.left = "50%";
			}
			var newChart = new gapi.analytics.googleCharts.DataChart({
				query: {
					metrics: metrics,
					dimensions: dimensions,
					'start-date': '30daysAgo',
					'end-date': 'today',
					sort: sort,
					'max-results': maxResults
				},
				chart: {
					container: container,
					type: type,
					options: chartOptions
				}
			});
			newChart.chartOptions = chartOptions;
			//transform data for gender table
			if ((chartName == "genderTable") || (chartName == "genderUniqueTableChart")){
				newChart.on('success', function(response){
					if (!vidgetAdgusted[chartName]) {
						response.data = transformedGenderTable(response.data);
					}
				});
			}
			//transform data for gender table
			if ((chartName == "ageTable") || (chartName == "ageUniqueTableChart")){
				newChart.on('success', function(response){
					if (!vidgetAdgusted[chartName]) {
						response.data = transformedAgeTable(response.data);
						//console.log(response.data);
					}
				});
			}
			//if this is a chart for avg session duration
			if (metrics.indexOf("avgSessionDuration") != -1){
				newChart.on('success', function(response){
					roundDurationSecsToMins(response.data);
				});
			}
			//
			newChart.on('success', function(response) {
				if (!vidgetAdgusted[chartName]) {
					vidgetAdgusted[chartName] = true;
					dataChangeLang(response.data);
					dataFormatDateTime(response.data);
					response.dataTable = new google.visualization.DataTable(response.data);
					$("#" + container).fadeIn(0);
					response.chart.draw(response.dataTable, newChart.chartOptions);
				} else {
					$("#" + container).fadeOut(0);
					queriesLoaded++;
					hideLoadIndicator();
				}
			});
			//
			newChart.on('error', function(response) {
				if ((response.error.code == 403) && (response.error.message.indexOf("Quota Error") != -1)){
					setTimeout(newChart.execute(), 500);
					event.stopPropagation();
				} else if (response.error.code == 400) {
					$("#" + container).empty();
					$("#" + container).append(getSign("no data"));
					queriesLoaded++;
					hideLoadIndicator(); 
				}
			});
			
			return newChart;
		}
		
		//***********************************************************************************************
		
		//different timelines (line charts)
		//
		visitsTimeline = newGAChart('visitsTimeline', 'LINE', 'visits-timeline-container', 'ga:sessions', 'ga:date');
		//
		customersTimeline = newGAChart('customersTimeline', 'LINE', 'customers-timeline-container', 'ga:users', 'ga:date');
		//
		visitsLengthTimeline = newGAChart('visitsLengthTimeline', 'LINE', 'visits-length-timeline-container', 'ga:avgSessionDuration', 'ga:date');
		//
		visitsLengthTable = newGAChart('visitsLengthTable', 'TABLE', 'visits-length-table-container', 'ga:avgSessionDuration', 'ga:date');
		
		//***********************************************************************************************
		
		//different charts with short total info (pie, column, tables)
		//
		userTypesTotalsPieChart = newGAChart('userTypesTotalsPieChart', 'PIE', 'chart-usertypes-totals-container', 'ga:sessions', 'ga:userType');
		//
		dayVisitiorsTotalsColumnChart = newGAChart('dayVisitiorsTotalsColumnChart', 'COLUMN', 'chart-dayvisitors-totals-container', 'ga:sessions', 'ga:hour');
		//
		dayVisitiorsTableChart = newGAChart('dayVisitiorsTableChart', 'TABLE', 'chart-dayvisitors-table-container', 'ga:sessions', 'ga:hour');
		
		//combined table of visits information
		//***********************************************************************************************
		
		//create data obj combined of 3 data objects
		function createCombinedVisitsTableData(chart){
			var newData = {
				cols: []
			};
			var timeDimension = chart.dataObjs['usersData'].columnHeaders[0].name;
			var timeLabel;
			var timeType;
			switch (timeDimension){
				case "ga:date":
					timeLabel = "Date";
					timeType = "date";
				break;
				case "ga:dateHour":
					timeLabel = "Hour of Day";
					timeType = "datetime";
				break;
				case "ga:isoYearIsoWeek":
					timeLabel = "ISO week of ISO year";
					timeType = "string";
				break;
				case "ga:yearMonth":
					timeLabel = "month";
					timeType = "string";
				break;
				default:
					timeLabel = "Time";
					timeType = "string";
				break;
			}
			var dCol = {
				id: timeDimension,
				label: timeLabel,
				type: timeType
			};
			var vCol = {
				id: "visits",
				label: "Overall",
				type: "number"
			};
			var uCol = {
				id: "users",
				label: "Unique",
				type: "number"
			};
			var nvCol = {
				id: "new_visits",
				label: "New",
				type: "number"
			};
			var ovCol = {
				id: "old_visits",
				label: "Returned",
				type: "number"
			};
			newData.cols[0] = dCol;
			newData.cols[1] = nvCol;
			newData.cols[2] = ovCol;
			newData.cols[3] = vCol;
			newData.cols[4] = uCol;
			
			//convert GA date string to datetime obj
			function gaDateStrToDateObj(dateStr){
				var year = dateStr.substr(0, 4);
				var month = dateStr.substr(4, 2) - 1;
				var day = dateStr.substr(6, 2);
				var hour = '00';
				if (dateStr.length > 8){
					hour = dateStr.substr(8, 2);
				}
				return new Date(year, month, day, hour);
			}
			
			var usersDataRows = chart.dataObjs['usersData'].rows;
			var visitsDataRows = chart.dataObjs['visitsData'].rows;
			var userTypesDataRows = chart.dataObjs['userTypesData'].rows;
			var totalV = 0;
			var totalU = 0;
			var totalNV = 0;
			var totalOV = 0;
			newData.rows = [];
			var usersI = 0;
			var visitsI = 0;
			var userTypesI = 0;
			if (visitsDataRows != null){
				while ((visitsI < visitsDataRows.length)){
					var newC = {
						c: []
					};
					var date;
					var vV = 0;
					var vU = 0;
					var vNV = 0;
					var vOV= 0;
					
					switch (timeDimension){
						case "ga:date":
							date = gaDateStrToDateObj(visitsDataRows[visitsI][0]);
						break;
						case "ga:dateHour":
							date = gaDateStrToDateObj(visitsDataRows[visitsI][0]);
						break;
						case "ga:week":
						case "ga:yearMonth":
						default:
							date = visitsDataRows[visitsI][0];
						break;
					}
					vV = visitsDataRows[visitsI][1];
					if (usersDataRows != null){
						if (usersI < usersDataRows.length){
							if (usersDataRows[usersI][0] == visitsDataRows[visitsI][0]){
								vU = usersDataRows[usersI][1];
								usersI++;
							}
						}
					}
					if (userTypesDataRows != null){
						for (i=0; i<2; i++){
							if (userTypesI < userTypesDataRows.length){
								if (userTypesDataRows[userTypesI][0] == visitsDataRows[visitsI][0]){
									if (userTypesDataRows[userTypesI][1] == "New Visitor"){
										vNV = userTypesDataRows[userTypesI][2];
									} else if (userTypesDataRows[userTypesI][1] == "Returning Visitor"){
										vOV = userTypesDataRows[userTypesI][2];
									}
									userTypesI++;
								}
							}
						}
					}
					visitsI++;
					
					totalV = totalV + Number(vV);
					totalU = totalU + Number(vU);
					totalNV = totalNV + Number(vNV);
					totalOV = totalOV + Number(vOV);
					
					newC.c[0] = {v: date};
					newC.c[1] = {v: vNV};
					newC.c[2] = {v: vOV};
					newC.c[3] = {v: vV};
					newC.c[4] = {v: vU};
					newData.rows.push(newC);
				}
			}
			var newC = {
				c: []
			};
			newC.c[0] = {v: null};
			newC.c[1] = {v: totalNV};
			newC.c[2] = {v: totalOV};
			newC.c[3] = {v: totalV};
			newC.c[4] = {v: totalU};
			newData.rows.push(newC);
			
			dataFormatDateTime(newData);
			dataChangeLang(newData);		
			return newData;
		}
		
		//table chart itself
		var chartOptions = {
			width: '100%'
		}
		var TableContainer = document.getElementById('chart-combined-visits-table-container');
		var combinedVisitsTableChart = new google.visualization.Table(TableContainer);
		combinedVisitsTableChart.chartOptions = chartOptions;
		combinedVisitsTableChart.dataToGet = 0;
		combinedVisitsTableChart.dataObjs = [];
		
		//creating data object for getting a part of combined table info
		function newReportDataForCombinedTable(dataName, chart, metrics, dimensions){
			queriesToLoad++;
			var newRepData = new gapi.analytics.report.Data({
				query: {
					metrics: metrics,
					dimensions: dimensions
				}
			});
			//
			newRepData.on('error', function(response) {
				if ((response.error.code == 403) && (response.error.message.indexOf("Quota Error") != -1)){
					setTimeout(newRepData.execute(), 500);
					event.stopPropagation();
				} else if (response.error.code == 400) {
					$("#chart-combined-visits-table-container").empty();
					$("#chart-combined-visits-table-container").append(getSign("no data"));
					queriesLoaded++;
					hideLoadIndicator();
				}
			});
			//
			newRepData.on('success', function(response) {
				queriesLoaded++;
				chart.dataObjs[dataName] = response;
				chart.dataToGet--;
				if (chart.dataToGet <= 0){
					var data = createCombinedVisitsTableData(chart);
					var dataTable = new google.visualization.DataTable(data);
					$("#chart-combined-visits-table-container").fadeIn(0);
					chart.draw(dataTable, chart.chartOptions);
					$(".google-visualization-table").addClass("gapi-analytics-data-chart");
					$("#chart-combined-visits-table-container").fadeOut(0);
				}
				hideLoadIndicator();
			});
			//
			return newRepData;
		}
		
		//reportData for Combined Visits Table (CVT)
		//
		userTypesDataCVT = newReportDataForCombinedTable('userTypesData', combinedVisitsTableChart, 'ga:sessions', 'ga:date, ga:userType');
		//
		usersDataCVT = newReportDataForCombinedTable('usersData', combinedVisitsTableChart, 'ga:users', 'ga:date');
		//
		visitsDataCVT = newReportDataForCombinedTable('visitsData', combinedVisitsTableChart, 'ga:sessions', 'ga:date');
		
		
		
		//general things. handlind controls and drawing charts and stuff
		//***********************************************************************************************
		
		//hide load indicator when all queries are loaded (and all charts are drawn)
		function hideLoadIndicator(){
			if (queriesLoaded >= queriesToLoad){
				$("#load-indicator").fadeOut(0);
				//and show selected panel
				$('.sidebar li.active').click();
			}
		}
		
		//convert string (metro ui format) into date obj
		function strToDate(str){
			var year = str.substr(6, 4);
			var month = str.substr(3, 2);
			var day = str.substr(0, 2);
			return year + "-" + month + "-" + day;
			////////////////return str.replace(/\./g,"-");
		}
		
		//***********************************************************************************************
		
		//(?set new view?) and redraw charts
		function redrawGACharts() {
			//dates
			startDate = strToDate($("#select-date-from .date-text")[0].value);
			stopDate = strToDate($("#select-date-to .date-text")[0].value);
			//check for correct "from" and "to" dates
			if (startDate > stopDate) return;
			//queries counter for load indicator
			queriesLoaded = 0;
			//show load indicator and hide all charts to get rid of scrollbar
			$("#load-indicator").fadeIn(0);
			$(".chart-panel section").fadeOut(0);
			//creating new params for charts:
			//dimension (hours, days, ...)
			var dimensionPeriod = $("#select-period")[0].value;
			var dimension;
			switch (dimensionPeriod){
				case "hours":
					dimension = "ga:dateHour";
				break;
				case "days":
					dimension = "ga:date";
				break;
				case "weeks":
					dimension = "ga:isoYearIsoWeek";
				break;
				case "months":
					dimension = "ga:yearMonth";
				break;
			}
			
			var newParams = {
				query: {
					ids: viewID,    //can be changed theoretically
					'start-date': startDate,
					'end-date': stopDate
				}
			}
			
			var newParamsTimeline = {
				query: {
					dimensions: dimension
				}
			}
			
			var newParamsGenderTable = {
				query: {
					dimensions: dimension + ', ga:userGender'
				}
			}
			
			var newParamsAgeTable = {
				query: {
					dimensions: dimension + ', ga:userAgeBracket'
				}
			}
			
			var newParamsUserTypesDataCVT = {
				query: {
					dimensions: dimension + ', ga:userType'
				}
			}
			
			//apply new params
			visitsTotalReport.set(newParams);
			visitsNewOldTotalReport.set(newParams);
			sessionAvgTimeTotalReport.set(newParams);
			usersTotalReport.set(newParams);
			
			visitsTimeline.set(newParams);
			customersTimeline.set(newParams);
			visitsLengthTimeline.set(newParams);
			visitsLengthTable.set(newParams);
			
			userTypesTotalsPieChart.set(newParams);
			dayVisitiorsTotalsColumnChart.set(newParams);
			dayVisitiorsTableChart.set(newParams);
			
			userTypesDataCVT.set(newParams);
			usersDataCVT.set(newParams);
			visitsDataCVT.set(newParams);
			
			visitsTimeline.set(newParamsTimeline);
			customersTimeline.set(newParamsTimeline);
			visitsLengthTimeline.set(newParamsTimeline);
			visitsLengthTable.set(newParamsTimeline);
			usersDataCVT.set(newParamsTimeline);
			visitsDataCVT.set(newParamsTimeline);
			
			userTypesDataCVT.set(newParamsUserTypesDataCVT);
			
			//show charts
			visitsTotalReport.execute();
			visitsNewOldTotalReport.execute();
			sessionAvgTimeTotalReport.execute();
			usersTotalReport.execute();
			
			vidgetAdgusted['visitsTimeline'] = false;
			visitsTimeline.execute();
			vidgetAdgusted['customersTimeline'] = false;
			customersTimeline.execute();
			vidgetAdgusted['visitsLengthTimeline'] = false;
			visitsLengthTimeline.execute();
			vidgetAdgusted['visitsLengthTable'] = false;
			visitsLengthTable.execute();
			
			vidgetAdgusted['userTypesTotalsPieChart'] = false;
			userTypesTotalsPieChart.execute();
			vidgetAdgusted['dayVisitiorsTotalsColumnChart'] = false;
			dayVisitiorsTotalsColumnChart.execute();
			vidgetAdgusted['dayVisitiorsTableChart'] = false;
			dayVisitiorsTableChart.execute();
			
			combinedVisitsTableChart.dataToGet = 3;
			userTypesDataCVT.execute();
			usersDataCVT.execute();
			visitsDataCVT.execute();
		}
		
		//when change any interface conlrol redraw content
		function addChangeControlHandler(){
			$(".chart-control").unbind("change");
			$(".chart-control").change(function() {
				redrawGACharts();
			});
		}
		
		//draw initial diagrams
		redrawGACharts();
		//add control handler
		addChangeControlHandler();
		
		//export buttons handlers
		$("#export-chart-combined-visits-table").click(function() {
			var table = $("#chart-combined-visits-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visitors_combined_data"));
		});
		$("#export-chart-dayvisitors-table").click(function() {
			var table = $("#chart-dayvisitors-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visitors_timeofday"));
		});
		$("#export-chart-gender-unique-table").click(function() {
			var table = $("#chart-gender-unique-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visitors_gender_unique"));
		});
		$("#export-chart-age-unique-table").click(function() {
			var table = $("#chart-age-unique-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visitors_age_unique"));
		});
		$("#export-chart-gender-table").click(function() {
			var table = $("#chart-gender-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visitors_gender_visits"));
		});
		$("#export-chart-age-table").click(function() {
			var table = $("#chart-age-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visitors_age_visits"));
		});
		$("#export-chart-interests-commerc-table").click(function() {
			var table = $("#chart-interests-commerc-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("interests_commerc"));
		});
		$("#export-chart-interests-general-table").click(function() {
			var table = $("#chart-interests-general-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("interests_general"));
		});
		$("#export-visits-length-table").click(function() {
			var table = $("#visits-length-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visit_avg_duration"));
		});
	}
});

//*****************************************************************************************************************
// Functions for common purposes, interface and so on
//*****************************************************************************************************************

//get sign is user's language
function getSign(signID){
	var sign = "";
	for (l=0; l < langArray.length; l++){
		if (langArray[l]["id"].toLowerCase() == signID){
			sign = langArray[l][userLanguage];
			break;
		}
	}
	return sign;
}

//*****************************************************************************************************************

//export an HTML table to a file (found in the internet)
var tableToExcel = (function() {
    var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head> <!--[if gte mso 9]> <?xml version="1.0" encoding="UTF-8"?> <xml version="1.0" encoding="UTF-8">  <xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--> <meta charset="UTF-8"> <meta charset="UTF-8"/> <meta http-equiv="Content-Type" content="text/xml;charset=utf-8" /> <meta http-equiv="Content-Type" content="text/html;charset=utf-8" > </head><body><table>{table}</table></body></html>'
            , base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
    , format = function(s, c) {
        return s.replace(/{(\w+)}/g, function(m, p) {
            return c[p];
        })
    }
    return function(table, name) {
        if (!table.nodeType)
            table = document.getElementById(table)
        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
        window.location.href = uri + base64(format(template, ctx))
    }
})()

//*****************************************************************************************************************
//*****************************************************************************************************************

//when loaded
$(document).ready(function(){
	//*****************************************************************************************************************
	// Functions for metro dashboard
	//*****************************************************************************************************************
	
	//??????????????????????????????
	function pushMessage(t){
		var mes = 'Info|Implement independently';
		$.Notify({
			caption: mes.split("|")[0],
			content: mes.split("|")[1],
			type: t
		});
	}

	//handle clicks on left menu
	//change main content part
	$('.sidebar li').click(function(){
		if (!$(this).hasClass('active')) {
			$('.sidebar li').removeClass('active');
			$(this).addClass('active');
		}
		//hide charts on panels
		$(".chart-panel section").fadeOut(0);
		//hide panels
		var panelID = this.dataset["panel_id"];
		$(".chart-panel").css("visibility", "hidden");
		//move chart containers to DIVs obn the visible panel (coz one chart can be present on multiple panels and should "jump" (GA API doesn't draw the same chart on multiple vidgets))
		var divArr = $("#"+panelID).find("div");
		for (i=0; i < divArr.length; i++){
			var divID = divArr[i].id;
			var containerID = divID.replace("div", "container");
			var containerArr = $("#"+containerID);
			if (containerArr.length != 0){
				$(divArr[i]).append(containerArr[0]);
			}
		}
		//show charts on chosen panel
		$("#" + panelID + " section").fadeIn(0);
		//show chosen panel
		$("#" + panelID).css("visibility", "visible");
	})
});
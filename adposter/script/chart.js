//loading some google stuff
google.load('visualization', '1', {packages: ['corechart', 'bar']});

//*****************************************************************************************************************
// Functions for advertizer's page
//*****************************************************************************************************************

//draw column diagram for advertiser's page 
function drawStacked(type, startDate, stopDate, locations, periodType, advs) {
	//for "hours" type get data only for one day
	if (periodType == "hours") {
		stopDate = startDate;
	}
	//get data as string from db
	$.post('get_column_chart_data_advr.php', {start_date: startDate, stop_date: stopDate, data_type: type, locations: locations, period_type: periodType, advs: advs, grouping: ""}, function(html){
		
		////////alert(html);
		
		//string to array
		var dataArr = [];
		var strArr = html.split("\n");
		for (i=0; i<strArr.length; i++){
			dataArr[i] = strArr[i].split("+");
		}
		
		//create columns
		var data = new google.visualization.DataTable();
		//set X axis type according to period type
		switch (periodType){
			case "days":
				data.addColumn('date', 'Date');
			break;
			case "weeks":
				data.addColumn('string', 'Week');
			break;
			case "months":
				data.addColumn('string', 'Month');
			break;
			case "hours":
				data.addColumn('timeofday', 'Time of Day');
			break;
		}
		//
		for (j=0; j<dataArr[0].length-1; j++){
			data.addColumn('number', dataArr[0][j]);
		}
		
		//find X axis limits
		switch (periodType){
			case "days":
				//find min and max date
				if (dataArr.length >= 3) {
					minDate = new Date(dataArr[1][0]);
					maxDate = new Date(dataArr[dataArr.length-2][0]);
				} else 
				{
					minDate = new Date();
					maxDate = new Date();
				}
			break;
			case "weeks":
			case "months":
			break;
			case "hours":
				//find min and max hour
				if (dataArr.length >= 3) {
					minHour = [Number(dataArr[1][0]), 0, 0];
					maxHour = [Number(dataArr[dataArr.length-2][0]), 0, 0];
				} else 
				{
					minHour = [0, 0, 0];
					maxHour = [23, 59, 59];
				}
			break;
		}
		
		//create chart data
		var rows = [];
		for (i=1; i<dataArr.length-1; i++){
			rows[i-1] = [];
			//date (period) value according to chosen period type
			switch (periodType){
				case "days":
					rows[i-1][0] = {v: new Date(dataArr[i][0]), f: dataArr[i][0]};
				break;
				case "weeks":
				case "months":
					rows[i-1][0] = {v: String(dataArr[i][0]), f: dataArr[i][0]};
				break;
				case "hours":
					hour = Number(dataArr[i][0]);
					valueArr = [hour, 0, 0];
					if ((hour / 12) >= 1) {
						hStr = "pm";
					} else {
						hStr = "am";
					}
					hint = (hour % 12);
					if (hint == 0) hint = 12;
					hint = hint + hStr;
					rows[i-1][0] = {v: valueArr, f: hint};
				break;
			}
			//for ctr need floating values of percentage, clicks and shows are discrete
			for (j=1; j<dataArr[i].length-1; j++){
				if (type == "CTR") {
					rows[i-1][j] = parseFloat(dataArr[i][j]);
				} else {
					rows[i-1][j] = parseInt(dataArr[i][j]);
				}
			}
		}
		//ta-da, add formatted rows to chart data
		data.addRows(rows);
		
		
		//set parameters
		//chartID, Y axis and target DIV accordong to diagram type 
		switch (type){
			case "shows":
				chartTitle = "Ad shows";
				vAxisTitle = "Shows";
				vAxisFormat = "";
				chartID = "shows-chart";
			break;
			case "clicks":
				chartTitle = "Ad clicks";
				vAxisTitle = "Clicks";
				vAxisFormat = "";
				chartID = "clicks-chart";
			break;
			case "CTR":
				chartTitle = "Ad CTR";
				vAxisTitle = "CTR";
				vAxisFormat = "percent";
				chartID = "CTR-chart";
			break;
		}
		//X axis according to period type
		switch (periodType){
			case "days":
				hAxis = {
					title: 'Date',
					format: 'yy-MM-dd',
					viewWindow: {
						min: minDate,
						max: maxDate
					}
				}
			break;
			case "weeks":
				hAxis = {
					title: 'Week',
					slantedText: true,
					slantedTextAngle: 90
				}
			break;
			case "months":
				hAxis = {
					title: 'Month',
					// slantedText: true,        //maybe don't need this, harder to read
					// slantedTextAngle: 90
				}
			break;
			case "hours":
				hAxis = {
					title: 'Time of Day',
					format: 'h:mm a',
					viewWindow: {
						min: minHour,
						max: maxHour
					}
				}
			break;
		}
		
		var options = {
			title: chartTitle,
			isStacked: true,
			hAxis: hAxis,
			vAxis: {
				title: vAxisTitle,
				format: vAxisFormat
			}
		};
		
		//draw chart
		var chart = new google.visualization.ColumnChart(document.getElementById(chartID));
		chart.draw(data, options);
	}, 'html');
}

//*****************************************************************************************************************

//draw pie diagram with totals for advertiser's page 
function drawPie(type, startDate, stopDate, locations, periodType, advs) {
	//get data as string from db
	$.post('get_pie_chart_data_advr.php', {start_date: startDate, stop_date: stopDate, data_type: type, locations: locations, period_type: periodType, advs: advs}, function(html){
		
		///////////////alert(html);
		
		//string to array
		var dataArr = [];
		var strArr = html.split("\n");
		for (i=0; i<strArr.length; i++){
			dataArr[i] = strArr[i].split("+");
		}
		
		switch (type){
			case "shows":
				chartTitle = "Total shows";
				axisTitle = "Shows";
				chartID = "shows-piechart";
			break;
			case "clicks":
				chartTitle = "Total clicks";
				axisTitle = "Clicks";
				chartID = "clicks-piechart";
			break;
			case "CTR":
				chartTitle = "Total CTR";
				axisTitle = "CTR";
				chartID = "CTR-piechart";
			break;
		}
		
		//create arr for passing to chart framework
		var initArr = [];
		initArr[0] = ['Location', axisTitle];
		for (i=0; i<dataArr.length-1; i++){
			if (type == "CTR") {
				initArr[i+1] = [dataArr[i][0], parseFloat(dataArr[i][1])];
			} else {
				initArr[i+1] = [dataArr[i][0], parseInt(dataArr[i][1])];
			}
		}
		
		//create chart data 
		var data = google.visualization.arrayToDataTable(initArr);
		
		//
		var options = {
			title: chartTitle
		};

		//draw chart
		var chart = new google.visualization.PieChart(document.getElementById(chartID));
		chart.draw(data, options);
	}, 'html');
}

//*****************************************************************************************************************

//draw table with locations statistics for advertisers's page 
function drawTableLocationsStatistics(type, startDate, stopDate, locations, periodType, advs){
	//get data as string from db
	$.post('get_column_chart_data_advr.php', {start_date: startDate, stop_date: stopDate, data_type: type, locations: locations, period_type: periodType, advs: advs}, function(html){
		
		///////$("#php-text").empty();
		///////$("#php-text").append(html);
		
		//string to array
		var dataArr = [];
		var strArr = html.split("\n");
		for (i=0; i<strArr.length; i++){
			dataArr[i] = strArr[i].split("+");
		}
		
		//set parameters
		switch (type){
			case "shows":
				divID = "container-shows-table";
				tableID = "shows-table";
				dataHeader = "Shows";
			break;
			case "clicks":
				divID = "container-clicks-table";
				tableID = "clicks-table";
				dataHeader = "Clicks";
			break;
			case "CTR":
				divID = "container-CTR-table";
				tableID = "CTR-table";
				dataHeader = "CTR";
			break;
		}
		
		//clear container
		$("#" + divID).empty();
		
		//add table
		var table = $("<table id='" + tableID + "' class='statistics-table'><thead></thead><tbody></tbody></table>");
		$(table).appendTo("#" + divID);
		//header
		var tr; 
		tr = $("<tr>");
		$(tr).appendTo("#" + tableID + " thead");
		var td;
		td = $("<td>Date/Location</td>");
		$(td).appendTo(tr);
		//Locations names from array
		for (j=0; j<dataArr[0].length-1; j++){
			td = $("<td>" + dataArr[0][j] + "</td>");
			$(td).appendTo(tr);
		}
		//body, use data from array
		for (i=1; i<dataArr.length-1; i++){
			tr = $("<tr>");
			$(tr).appendTo("#" + tableID + " tbody");
			for (j=0; j<dataArr[i].length-1; j++){
				td = $("<td>" + dataArr[i][j] + "</td>");
				$(td).appendTo(tr);
			}
		}
	}, 'html');
}

//*****************************************************************************************************************

//draw table with locations statistics for advertisers's page (combine shows, clicks and CTR in one table)
function drawTableLocationsStatisticsCombined(startDate, stopDate, locations, periodType, advs){
	var dataToGet = 3;
	
	var dataArrShows = [];
	//get data as string from db (shows)
	$.post('get_column_chart_data_advr.php', {start_date: startDate, stop_date: stopDate, data_type: "shows", locations: locations, period_type: periodType, advs: advs, grouping: "locations"}, function(html){
		//string to array
		var strArr = html.split("\n");
		for (i=0; i<strArr.length; i++){
			dataArrShows[i] = strArr[i].split("+");
		}
		dataToGet--;
	}, 'html');
	
	var dataArrClicks = [];
	//get data as string from db (clicks)
	$.post('get_column_chart_data_advr.php', {start_date: startDate, stop_date: stopDate, data_type: "clicks", locations: locations, period_type: periodType, advs: advs, grouping: "locations"}, function(html){
		//string to array
		var strArr = html.split("\n");
		for (i=0; i<strArr.length; i++){
			dataArrClicks[i] = strArr[i].split("+");
		}
		dataToGet--;
	}, 'html');
	
	var dataArrCTR = [];
	//get data as string from db (CTR)
	$.post('get_column_chart_data_advr.php', {start_date: startDate, stop_date: stopDate, data_type: "CTR", locations: locations, period_type: periodType, advs: advs, grouping: "locations"}, function(html){
		//string to array
		var strArr = html.split("\n");
		for (i=0; i<strArr.length; i++){
			dataArrCTR[i] = strArr[i].split("+");
		}
		dataToGet--;
	}, 'html');
		
	//draw table when get all the data arrays	
	setTimeout(drawTable, 500);
		
	function drawTable(){
		if (dataToGet <= 0){
			divID = "container-combined-table";
			tableID = "combined-table";
			
			//clear container
			$("#" + divID).empty();
			
			//add table
			var table = $("<table id='" + tableID + "' class='statistics-table'><thead></thead><tbody></tbody></table>");
			$(table).appendTo("#" + divID);
			//header
			var tr; 
			tr = $("<tr>");
			$(tr).appendTo("#" + tableID + " thead");
			var td;
			td = $("<td rowspan='2'>Date/Location</td>");
			$(td).appendTo(tr);
			//Locations names from array
			for (j=0; j<dataArrShows[0].length-1; j++){
				td = $("<td colspan='3'>" + dataArrShows[0][j] + "</td>");
				$(td).appendTo(tr);
			}
			//second header row, subheaders
			tr = $("<tr>");
			$(tr).appendTo("#" + tableID + " thead");
			for (j=0; j<dataArrShows[0].length-1; j++){
				td = $("<td>Shows</td>");
				$(td).appendTo(tr);
				td = $("<td>Clicks</td>");
				$(td).appendTo(tr);
				td = $("<td>CTR</td>");
				$(td).appendTo(tr);
			}
			//body, use data from arrays
			var clicksArrRow = 1;
			var ctrArrRow = 1;
			var printClicks;
			var printCTR;
			var clicks;
			var ctr;
			//go through shows array ror by row
			for (i=1; i<dataArrShows.length-1; i++){
				tr = $("<tr>");
				$(tr).appendTo("#" + tableID + " tbody");
				//see if there is correspondent row in clicks array
				if (dataArrShows[i][0] == dataArrClicks[i][0]){
					printClicks = true;
				} else {
					printClicks = false;
				}
				//see if there is correspondent row in CTR array
				if (dataArrShows[i][0] == dataArrCTR[i][0]){
					printCTR = true;
				} else {
					printCTR = false;
				}
				//print cells
				//period description
				td = $("<td>" + dataArrShows[i][0] + "</td>");
				$(td).appendTo(tr);
				//data cells
				for (j=1; j<dataArrShows[i].length-1; j++){
					//actual data or "0" if there is no from clicks and CTR arrays, shows data is always present 
					if (printClicks){
						clicks = dataArrClicks[clicksArrRow][j];
					} else {
						clicks = 0;
					}
					if (printCTR){
						ctr = dataArrCTR[ctrArrRow][j];
					} else {
						ctr = 0;
					}
					//
					td = $("<td>" + dataArrShows[i][j] + "</td>");
					$(td).appendTo(tr);
					td = $("<td>" + clicks + "</td>");
					$(td).appendTo(tr);
					td = $("<td>" + ctr + "</td>");
					$(td).appendTo(tr);
				}
				//if printed actul data go for next rows
				if (printClicks) clicksArrRow++;
				if (printCTR) ctrArrRow++;
			}
		} else {
			setTimeout(drawTable, 500);
		}
	}
}

//*****************************************************************************************************************

//draw diagrams and tables(for advertiser)
function redrawForAdvr(){
	//clear content
	$("#shows-chart").empty();
	$("#clicks-chart").empty();
	$("#CTR-chart").empty();
	$("#container-shows-table").empty();
	$("#container-clicks-table").empty();
	$("#container-CTR-table").empty();
	$("#shows-piechart").empty();
	$("#clicks-piechart").empty();
	$("#CTR-piechart").empty();
	$("#container-combined-table").empty();
	
	//get period type
	periodType = $("#select-period")[0].value;
	//
	var startDate = $("#select-date-from")[0].value;
	var stopDate = $("#select-date-to")[0].value;
	//get a list of selected locations (from locations filter)
	var locations = "";
	var optionsArr = $("#select-locations").find("input");
	for (i=0; i < optionsArr.length; i++){
		if (optionsArr[i].checked){
			locations = locations + optionsArr[i].value + "+";
		}
	}
	//get a list of selected ads (from ads filter)
	var advs = "";
	var optionsArr = $("#select-advs").find("input");
	for (i=0; i < optionsArr.length; i++){
		if (optionsArr[i].checked){
			advs = advs + optionsArr[i].value + "+";
		}
	}
	//draw
	drawTableLocationsStatisticsCombined(startDate, stopDate, locations, periodType, advs);
	
	/* drawTableLocationsStatistics("shows", startDate, stopDate, locations, periodType, advs);
	drawTableLocationsStatistics("clicks", startDate, stopDate, locations, periodType, advs);
	drawTableLocationsStatistics("CTR", startDate, stopDate, locations, periodType, advs); */
	
	drawStacked("shows", startDate, stopDate, locations, periodType, advs);
	drawStacked("clicks", startDate, stopDate, locations, periodType, advs);
	drawStacked("CTR", startDate, stopDate, locations, periodType, advs);
	
	drawPie("shows", startDate, stopDate, locations, periodType, advs);
	drawPie("clicks", startDate, stopDate, locations, periodType, advs);
	drawPie("CTR", startDate, stopDate, locations, periodType, advs);
}

//*****************************************************************************************************************
// Functions for location's (partner's) page to display ads info
//*****************************************************************************************************************

//draw bar diagram for locations's page 
function drawBar(type, startDate, stopDate, locations, customers) {
	//get data as string from db
	$.post('get_bar_chart_data_partner.php', {start_date: startDate, stop_date: stopDate, data_type: type, locations: locations, customers: customers},  function(html){
		
		///////$("#php-text").append(html);
		
		//string to array
		var dataArr = [];
		var strArr = html.split("\n");
		for (i=0; i<strArr.length; i++){
			dataArr[i] = strArr[i].split("+");
		}
		
		//set parameters
		//chartID, X axis and target DIV accordong to diagram type 
		switch (type){
			case "shows":
				chartTitle = "Ad shows";
				hAxisTitle = "Shows";
				hAxisFormat = "";
				chartID = "shows-chart";
			break;
			case "clicks":
				chartTitle = "Ad clicks";
				hAxisTitle = "Clicks";
				hAxisFormat = "";
				chartID = "clicks-chart";
			break;
			case "CTR":
				chartTitle = "Ad CTR";
				hAxisTitle = "CTR";
				hAxisFormat = "percent";
				chartID = "CTR-chart";
			break;
		}
		
		//Y axis
		vAxis = {
			title: 'Customers'
		}
		
		var options = {
			title: chartTitle,
			isStacked: true,
			vAxis: vAxis,
			hAxis: {
				title: hAxisTitle,
				format: hAxisFormat
			}
		}
		
		//create bars
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Customer');
		data.addColumn('number', hAxisTitle);
		
		//create chart data
		var rows = [];
		for (i=0; i<dataArr.length-1; i++){
			rows[i] = [];
			rows[i][0] = {v: String(dataArr[i][0]), f: dataArr[i][0]};
			//for ctr need floating values of percentage, clicks and shows are discrete
			for (j=1; j<dataArr[i].length-1; j++){
				if (type == "CTR") {
					rows[i][j] = parseFloat(dataArr[i][j]);
				} else {
					rows[i][j] = parseInt(dataArr[i][j]);
				}
			}
		}
		//ta-da, add formatted rows to chart data
		data.addRows(rows);
		
		//draw chart
		var chart = new google.visualization.BarChart(document.getElementById(chartID));
		chart.draw(data, options);
	}, 'html');
}

//*****************************************************************************************************************

//draw table with customers statistics for location's page 
function drawTableCustomersStatistics(type, startDate, stopDate, locations, customers) {
	//get data as string from db
	$.post('get_bar_chart_data_partner.php', {start_date: startDate, stop_date: stopDate, data_type: type, locations: locations, customers: customers},  function(html){
		
		//////$("#php-text").append(html);
		
		//string to array
		var dataArr = [];
		var strArr = html.split("\n");
		for (i=0; i<strArr.length; i++){
			dataArr[i] = strArr[i].split("+");
		}
		
		//set parameters
		switch (type){
			case "shows":
				divID = "container-shows-table";
				tableID = "shows-table";
				dataHeader = "Shows";
			break;
			case "clicks":
				divID = "container-clicks-table";
				tableID = "clicks-table";
				dataHeader = "Clicks";
			break;
			case "CTR":
				divID = "container-CTR-table";
				tableID = "CTR-table";
				dataHeader = "CTR";
			break;
		}
		
		//clear container
		$("#" + divID).empty();
		
		//add table
		var table = $("<table id='" + tableID + "' class='statistics-table'><thead></thead><tbody></tbody></table>");
		$(table).appendTo("#" + divID);
		//header
		var tr; 
		tr = $("<tr>");
		$(tr).appendTo("#" + tableID + " thead");
		var td;
		td = $("<td>Customer</td>");
		$(td).appendTo(tr);
		td = $("<td>" + dataHeader + "</td>");
		$(td).appendTo(tr);
		//body, use data from array
		for (i=0; i<dataArr.length-1; i++){
			tr = $("<tr>");
			$(tr).appendTo("#" + tableID + " tbody");
			td = $("<td>" + dataArr[i][0] + "</td>");
			$(td).appendTo(tr);
			td = $("<td>" + dataArr[i][1] + "</td>");
			$(td).appendTo(tr);
		}
	}, 'html');
}

//*****************************************************************************************************************

//draw table with customers statistics for location's page (combine shows, clicks and CTR in one table)
function drawTableCustomersStatisticsCombined(startDate, stopDate, locations, customers) {
	var dataToGet = 3;
	
	var dataArrShows = [];
	//get data as string from db (shows)
	$.post('get_bar_chart_data_partner.php', {start_date: startDate, stop_date: stopDate, data_type: "shows", locations: locations, customers: customers},  function(html){
		//string to array
		var strArr = html.split("\n");
		for (i=0; i<strArr.length; i++){
			dataArrShows[i] = strArr[i].split("+");
		}
		dataToGet--;
	}, 'html');
	
	var dataArrClicks = [];
	//get data as string from db (clicks)
	$.post('get_bar_chart_data_partner.php', {start_date: startDate, stop_date: stopDate, data_type: "clicks", locations: locations, customers: customers},  function(html){
		//string to array
		var strArr = html.split("\n");
		for (i=0; i<strArr.length; i++){
			dataArrClicks[i] = strArr[i].split("+");
		}
		dataToGet--;
	}, 'html');
	
	var dataArrCTR = [];
	//get data as string from db (CTR)
	$.post('get_bar_chart_data_partner.php', {start_date: startDate, stop_date: stopDate, data_type: "CTR", locations: locations, customers: customers},  function(html){
		//string to array
		var strArr = html.split("\n");
		for (i=0; i<strArr.length; i++){
			dataArrCTR[i] = strArr[i].split("+");
		}
		dataToGet--;
	}, 'html');
		
	//draw table when get all the data arrays	
	setTimeout(drawTable, 500);
	
	function drawTable(){
		if (dataToGet <= 0){
			divID = "container-combined-table";
			tableID = "combined-table";
			
			//clear container
			$("#" + divID).empty();
			
			//add table
			var table = $("<table id='" + tableID + "' class='statistics-table'><thead></thead><tbody></tbody></table>");
			$(table).appendTo("#" + divID);
			//header
			var tr; 
			tr = $("<tr>");
			$(tr).appendTo("#" + tableID + " thead");
			var td;
			td = $("<td>Customer</td>");
			$(td).appendTo(tr);
			td = $("<td>Shows</td>");
			$(td).appendTo(tr);
			td = $("<td>Clicks</td>");
			$(td).appendTo(tr);
			td = $("<td>CTR</td>");
			$(td).appendTo(tr);
			//body, use data from arrays
			var clicksArrRow = 0;
			var ctrArrRow = 0;
			var printClicks;
			var printCTR;
			var clicks;
			var ctr;
			//go through shows array ror by row
			for (i=0; i<dataArrShows.length-1; i++){
				tr = $("<tr>");
				$(tr).appendTo("#" + tableID + " tbody");
				//see if there is correspondent row in clicks array
				if (dataArrShows[i][0] == dataArrClicks[i][0]){
					printClicks = true;
				} else {
					printClicks = false;
				}
				//see if there is correspondent row in CTR array
				if (dataArrShows[i][0] == dataArrCTR[i][0]){
					printCTR = true;
				} else {
					printCTR = false;
				}
				//print cells
				//period description
				td = $("<td>" + dataArrShows[i][0] + "</td>");
				$(td).appendTo(tr);
				//data cells
				for (j=1; j<dataArrShows[i].length-1; j++){
					//actual data or "0" if there is no from clicks and CTR arrays, shows data is always present 
					if (printClicks){
						clicks = dataArrClicks[clicksArrRow][j];
					} else {
						clicks = 0;
					}
					if (printCTR){
						ctr = dataArrCTR[ctrArrRow][j];
					} else {
						ctr = 0;
					}
					//
					td = $("<td>" + dataArrShows[i][j] + "</td>");
					$(td).appendTo(tr);
					td = $("<td>" + clicks + "</td>");
					$(td).appendTo(tr);
					td = $("<td>" + ctr + "</td>");
					$(td).appendTo(tr);
				}
				//if printed actul data go for next rows
				if (printClicks) clicksArrRow++;
				if (printCTR) ctrArrRow++;
			}
		} else {
			setTimeout(drawTable, 500);
		}
	}
}

//*****************************************************************************************************************

//draw diagrams and tables(for location(s))
function redrawForLocation(){
	//clear content
	$("#shows-chart").empty();
	$("#clicks-chart").empty();
	$("#CTR-chart").empty();
	$("#container-shows-table").empty();
	$("#container-clicks-table").empty();
	$("#container-CTR-table").empty();
	$("#container-combined-table").empty();
	
	var startDate = $("#select-date-from")[0].value;
	var stopDate = $("#select-date-to")[0].value;
	//get a list of selected locations (from locations filter)
	var locations = "";
	var optionsArr = $("#select-locations").find("input");
	for (i=0; i < optionsArr.length; i++){
		if (optionsArr[i].checked){
			locations = locations + optionsArr[i].value + "+";
		}
	}
	//get a list of selected customers (from customers filter)
	var customers = "";
	var optionsArr = $("#select-customers").find("input");
	for (i=0; i < optionsArr.length; i++){
		if (optionsArr[i].checked){
			customers = customers + optionsArr[i].value + "+";
		}
	}
	//draw
	drawTableCustomersStatisticsCombined(startDate, stopDate, locations, customers);
	
	/* drawTableCustomersStatistics("shows", startDate, stopDate, locations, customers);
	drawTableCustomersStatistics("clicks", startDate, stopDate, locations, customers);
	drawTableCustomersStatistics("CTR", startDate, stopDate, locations, customers); */
	
	drawBar("shows", startDate, stopDate, locations, customers);
	drawBar("clicks", startDate, stopDate, locations, customers);
	drawBar("CTR", startDate, stopDate, locations, customers);
}

//*****************************************************************************************************************
// Functions for a-charts page, display info from g-storage
//*****************************************************************************************************************

//flags to check vidgets adgustments completion (change language, format numbers)
var vidgetAdgusted = [];

//????????????????????????????????????????
(function(w,d,s,g,js,fjs){
	g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
	js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
	js.src='https://apis.google.com/js/platform.js';
	fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
}(window,document,'script'));

//*****************************************************************************************************************

//when loaded a-library (if it's a page with a-charts)
gapi.analytics.ready(function() {
	var queriesToLoad = 0;
	var queriesLoaded = 0;
	var startDate;
	var stopDate;
			
	if (pageType == "dashboard_customers_statistics"){
		
		//authorisation
		if (accessToken != ""){
			//use token from php script to get access to data
			gapi.analytics.auth.authorize({
				'serverAuth': {
					'access_token': accessToken
				}
			});
		}
		else {
			// !!!!Used only for testing purposes when we don't get token by php library
			var CLIENT_ID = '885338747009-ija5r7g048nb3cu2pnt3u4n8cabtj493.apps.googleusercontent.com';
			if (!gapi.analytics.auth.isAuthorized()) {
				gapi.analytics.auth.authorize({
					container: 'auth-button',
					clientid: CLIENT_ID,
				});
			}
		}

		//creating different GA api elements to get and show GA statistics data to user
		//***********************************************************************************************

		//small vidgets with total numbers (one number) for the top of the overview page
		//
		queriesToLoad++;
		var visitsTotalReport = new gapi.analytics.report.Data({
		  query: {
			metrics: 'ga:sessions',
		  }
		});
		visitsTotalReport.on('success', function(response) {
		  queriesLoaded++;
		  $("#sessions-total .content").empty();
		  $("#sessions-total .content").append(response.rows[0][0]);
		  hideLoadIndicator();
		});
		visitsTotalReport.on('error', function(response) {
			if ((response.error.code == 403) && (response.error.message.indexOf("Quota Error") != -1)){
				setTimeout(visitsTotalReport.execute(), 500);
				event.stopPropagation();
			} else if (response.error.code == 400) {
				queriesLoaded++;
				hideLoadIndicator(); 
			}
		});
		
		//
		queriesToLoad++;
		var visitsNewOldTotalReport = new gapi.analytics.report.Data({
		  query: {
			metrics: 'ga:sessions',
			dimensions: 'ga:userType'
		  }
		});
		visitsNewOldTotalReport.on('success', function(response) {
			queriesLoaded++;
			$("#new-sessions-total .content").empty();
			$("#old-sessions-total .content").empty();
			$("#new-sessions-total .content").append(response.rows[0][1]);
			$("#old-sessions-total .content").append(response.rows[1][1]);
			hideLoadIndicator();
		});
		visitsNewOldTotalReport.on('error', function(response) {
			if ((response.error.code == 403) && (response.error.message.indexOf("Quota Error") != -1)){
				setTimeout(visitsNewOldTotalReport.execute(), 500);
				event.stopPropagation();
			} else if (response.error.code == 400) {
				queriesLoaded++;
				hideLoadIndicator();
			}
		});
		
		//
		queriesToLoad++;
		var usersTotalReport = new gapi.analytics.report.Data({
			query: {
				metrics: 'ga:users'
			}
		});
		usersTotalReport.on('success', function(response) {
			queriesLoaded++;
			$("#users-total .content").empty();
			$("#users-total .content").append(response.rows[0][0]);
			hideLoadIndicator();
		});
		usersTotalReport.on('error', function(response) {
			if ((response.error.code == 403) && (response.error.message.indexOf("Quota Error") != -1)){
				setTimeout(usersTotalReport.execute(), 500);
				event.stopPropagation();
			} else if (response.error.code == 400) {
				queriesLoaded++;
				hideLoadIndicator();
			}
		});
		
		//
		queriesToLoad++;
		var sessionAvgTimeTotalReport = new gapi.analytics.report.Data({
		query: {
			metrics: 'ga:avgSessionDuration'
		}
		});
		sessionAvgTimeTotalReport.on('success', function(response) {
			queriesLoaded++;
			$("#session-avgtime-total .content").empty();
			var AvgDurationMinutes = Math.round(response.rows[0][0] / 60);
			$("#session-avgtime-total .content").append(AvgDurationMinutes);
			hideLoadIndicator();
		});
		sessionAvgTimeTotalReport.on('error', function(response) {
			if ((response.error.code == 403) && (response.error.message.indexOf("Quota Error") != -1)){
				setTimeout(sessionAvgTimeTotalReport.execute(), 500);
				event.stopPropagation();
			} else if (response.error.code == 400) {
				queriesLoaded++;
				hideLoadIndicator();
			}
		});
		
		
		//GA charts and related functions
		//***********************************************************************************************
		//functions to change/format original charts by GA, to make it look our way
		
		//change language for data object labels (and eventually for charts)
		function dataChangeLang(data){
			//columns labels
			for (i=0; i < data.cols.length; i++){
				for (l=0; l < langArray.length; l++){
					if (data.cols[i].label.toLowerCase() == langArray[l]["id"].toLowerCase()){
						data.cols[i].label = langArray[l][userLanguage];
						break;
					}
				}
			}
			//values
			if (data.rows != null){
				for (i=0; i < data.rows.length; i++){
					var c = data.rows[i].c;
					for (j=0; j < c.length; j++){
						if (typeof(c[j].v) == "string"){
							for (l=0; l < langArray.length; l++){
								if (c[j].v.toLowerCase() == langArray[l]["id"].toLowerCase()){
									c[j].v = langArray[l][userLanguage];
									break;
								}
							}
						}
					}
				}
			}
		}
		
		//format data objects cols and rows to show date/time values in a propper format
		//e.g. start-end dates for a week of the year, month name for a month, show date and time for date-hour value
		//so it makes charts more readable
		function dataFormatDateTime(data){
			if (data.cols[0].id == "ga:dateHour"){
				data.cols[0].type = "datetime";
			}
			
			//number of month to it's name
			function num2month(num) {
				var month = [getSign("january"),getSign("february"),getSign("march"),getSign("april"),getSign("may"),getSign("june"),getSign("july"),getSign("august"),getSign("september"),getSign("october"),getSign("november"),getSign("december")];
				return month[num];
			}
			if (data.cols[0].id == "ga:yearMonth"){
				for (i=0; i < data.rows.length; i++){
					var c = data.rows[i].c;
					if ((c[0].v != null) && (c[0].v != "")) c[0].v = num2month(Number(c[0].v.substr(4, 2))-1);
				}
			}
			
			//get date by year, number of the week, number of the day
			function dateForDayOfWeekOfYear(year, week, day){
				// первый день года 
				var d1 = new Date(year, 0, 1);
				// его день недели
				var firstDay = d1.getDay();
				// день в году
				var days = firstDay == day 
					? 1 
					: (week) * 7 + day - (firstDay - 1);
				// дата для указанного дня недели
				var dn = new Date(year, 0, days);
				return dn;
			}
			if (data.cols[0].id == "ga:isoYearIsoWeek"){
				for (i=0; i < data.rows.length; i++){
					var c = data.rows[i].c;
					if (c[0].v != null){
						var year = Number(c[0].v.substr(0,4))
						var week = Number(c[0].v.substr(4,2)) - 1;
						c[0].v = dateForDayOfWeekOfYear(year, week, 1).toLocaleDateString() +
						" -\n" + dateForDayOfWeekOfYear(year, week, 7).toLocaleDateString();
					}
				}
			}
		}
		
		//round duration in seconds to minutes (for session duration charts)
		function roundDurationSecsToMins(data){
			for (i=0; i < data.rows.length; i++){
				var c = data.rows[i].c;
				var valIndex = c.length-1;
				if (typeof(c[valIndex].v) == "number"){
					c[valIndex].v = Math.round(c[valIndex].v / 60);
				}
			}
		}
		
		//transform table with gendeer statistics to make columns of male, female and total
		//looks much better, ha?
		function transformedGenderTable(data){
			//4 columns
			var newData = {
				cols: []
			};
			newData.cols[0] = data.cols[0];
			var fCol = {
				id: "female",
				label: "Female",
				type: "number"
			};
			var mCol = {
				id: "male",
				label: "Male",
				type: "number"
			};
			var tCol = {
				id: "total",
				label: "Total",
				type: "number"
			};
			newData.cols[1] = fCol;
			newData.cols[2] = mCol;
			newData.cols[3] = tCol;
			//go through rows of original data, make 1 row of each 2 or original (usually)
			//if there is only one gender number for some date the other gender number will be 0
			if (data.rows != null){
				var totalF = 0;
				var totalM = 0;
				var totalT = 0;
				newData.rows = [];
				var i = 0;
				while (i < data.rows.length){
					var newC = {
						c: []
					};
					var vF = 0;
					var vM = 0;
					//get numbers for males and females (no matter in which order they are)
					if (data.rows[i].c[1].v.toLowerCase() == "female"){
						vF = data.rows[i].c[2].v;
					} else {
						vM = data.rows[i].c[2].v;
					}//check that there are two rows with the same date (or skip assignment of value)
					if (i < data.rows.length-1){
						if ((data.rows[i+1].c[0].v).toLocaleString() == (data.rows[i].c[0].v).toLocaleString()){
							if (data.rows[i+1].c[1].v.toLowerCase() == "female"){
								vF = data.rows[i+1].c[2].v;
							} else {
								vM = data.rows[i+1].c[2].v;
							}
							i++;
						}
					}
					//
					totalF = totalF + vF;
					totalM = totalM + vM;
					totalT = totalT + vF + vM;
					//new row for new data object
					newC.c[0] = data.rows[i].c[0];
					newC.c[1] = {v: vF};
					newC.c[2] = {v: vM};
					newC.c[3] = {v: vF+vM};
					newData.rows.push(newC);
					//
					i++;
				}
				var newC = {
					c: []
				};
				newC.c[0] = {v: null};
				newC.c[1] = {v: totalF};
				newC.c[2] = {v: totalM};
				newC.c[3] = {v: totalT};
				newData.rows.push(newC);
			}
			return newData; 
		}
		
		//transform table with age statistics to make columns of diffrent age brackets and total column
		//looks much better, ha?
		function transformedAgeTable(data){
			//4 columns
			var newData = {
				cols: []
			};
			newData.cols[0] = data.cols[0];
			var col18 = {
				id: "18-24",
				label: "18-24",
				type: "number"
			};
			var col25 = {
				id: "25-34",
				label: "25-34",
				type: "number"
			};
			var col35 = {
				id: "35-44",
				label: "35-44",
				type: "number"
			};
			var col45 = {
				id: "45-54",
				label: "45-54",
				type: "number"
			};
			var col55 = {
				id: "55-64",
				label: "55-64",
				type: "number"
			};
			var col65 = {
				id: "65+",
				label: "65+",
				type: "number"
			};
			var tCol = {
				id: "total",
				label: "Total",
				type: "number"
			};
			newData.cols[1] = col18;
			newData.cols[2] = col25;
			newData.cols[3] = col35;
			newData.cols[4] = col45;
			newData.cols[5] = col55;
			newData.cols[6] = col65;
			newData.cols[7] = tCol;
			//go through rows of original data, make 1 row of each 6 or original (usually)
			//if there are some genders missing for the date the number for them will be 0
			if (data.rows != null){
				var total18 = 0;
				var total25 = 0;
				var total35 = 0;
				var total45 = 0;
				var total55 = 0;
				var total65 = 0;
				var totalT = 0;
				newData.rows = [];
				var i = 0;
				while (i < data.rows.length){
					var newC = {
						c: []
					};
					var v18 = 0;
					var v25 = 0;
					var v35 = 0;
					var v45 = 0;
					var v55 = 0;
					var v65 = 0;
					//get numbers for all age brackets (no matter in which order they are)
					//check that original rows have the same date, otherwise proceed to a new formatted row
					startI = i;
					for (n=0; n < 6; n++){
						if (i < data.rows.length){
							if ((data.rows[i].c[0].v).toLocaleString() == (data.rows[startI].c[0].v).toLocaleString()){
								switch (data.rows[i].c[1].v.toLowerCase()){
									case "18-24":
										v18 = data.rows[i].c[2].v;
									break;
									case "25-34":
										v25 = data.rows[i].c[2].v;
									break;
									case "35-44":
										v35 = data.rows[i].c[2].v;
									break;
									case "45-54":
										v45 = data.rows[i].c[2].v;
									break;
									case "55-64":
										v55 = data.rows[i].c[2].v;
									break;
									case "65+":
										v65 = data.rows[i].c[2].v;
									break;
								}
								i++;
							} //else {break;}
						} //else {break;}
					}
					//
					total18 = total18 + v18;
					total25 = total25 + v25;
					total35 = total35 + v35;
					total45 = total45 + v45;
					total55 = total55 + v55;
					total65 = total65 + v65;
					totalT = totalT + v18 + v25 + v35 + v45 + v55 + v65;
					//new row for new data object
					newC.c[0] = data.rows[startI].c[0];
					newC.c[1] = {v: v18};
					newC.c[2] = {v: v25};
					newC.c[3] = {v: v35};
					newC.c[4] = {v: v45};
					newC.c[5] = {v: v55};
					newC.c[6] = {v: v65};
					newC.c[7] = {v: v18+v25+v35+v45+v55+v65};
					newData.rows.push(newC);
				}
				var newC = {
					c: []
				};
				newC.c[0] = {v: null};
				newC.c[1] = {v: total18};
				newC.c[2] = {v: total25};
				newC.c[3] = {v: total35};
				newC.c[4] = {v: total45};
				newC.c[5] = {v: total55};
				newC.c[6] = {v: total65};
				newC.c[7] = {v: totalT};
				newData.rows.push(newC);
			}
			return newData; 
		}
		
		//creating a chart vidget (used to unify the process), can be pie, column, table, line...
		function newGAChart(chartName, type, container, metrics, dimensions, sort, maxResults){
			queriesToLoad++;
			var chartOptions = {
				width: '100%',
				legend: {
				},
				hAxis:{
				},
				vAxis: {
				},
				chartArea: {
				},
				annotations: {
				}
			}
			if (type == "PIE"){
				chartOptions.pieHole = 4/9;
			}
			if ((type == "BAR") || (type == "COLUMN") || (type == "LINE")){
				chartOptions.legend.position = "top";
			}
			if (type == "LINE") {
				chartOptions.pointSize = 3;
			}
			if ((chartName == "interestsGeneralBarChart") || (chartName == "interestsCommercBarChart")){
				chartOptions.height = maxResults*30;
				chartOptions.chartArea.left = "50%";
			}
			var newChart = new gapi.analytics.googleCharts.DataChart({
				query: {
					metrics: metrics,
					dimensions: dimensions,
					'start-date': '30daysAgo',
					'end-date': 'today',
					sort: sort,
					'max-results': maxResults
				},
				chart: {
					container: container,
					type: type,
					options: chartOptions
				}
			});
			newChart.chartOptions = chartOptions;
			//transform data for gender table
			if ((chartName == "genderTable") || (chartName == "genderUniqueTableChart")){
				newChart.on('success', function(response){
					if (!vidgetAdgusted[chartName]) {
						response.data = transformedGenderTable(response.data);
					}
				});
			}
			//transform data for gender table
			if ((chartName == "ageTable") || (chartName == "ageUniqueTableChart")){
				newChart.on('success', function(response){
					if (!vidgetAdgusted[chartName]) {
						response.data = transformedAgeTable(response.data);
					}
				});
			}
			//if this is a chart for avg session duration
			if (metrics.indexOf("avgSessionDuration") != -1){
				newChart.on('success', function(response){
					roundDurationSecsToMins(response.data);
				});
			}
			//
			newChart.on('success', function(response) {
				if (!vidgetAdgusted[chartName]) {
					vidgetAdgusted[chartName] = true;
					dataChangeLang(response.data);
					dataFormatDateTime(response.data);
					response.dataTable = new google.visualization.DataTable(response.data);
					$("#" + container).fadeIn(0);
					response.chart.draw(response.dataTable, newChart.chartOptions);
				} else {
					$("#" + container).fadeOut(0);
					queriesLoaded++;
					hideLoadIndicator();
				}
			});
			//
			newChart.on('error', function(response) {
				if ((response.error.code == 403) && (response.error.message.indexOf("Quota Error") != -1)){
					setTimeout(newChart.execute(), 500);
					event.stopPropagation();
				} else if (response.error.code == 400) {
					$("#" + container).empty();
					$("#" + container).append(getSign("no data"));
					queriesLoaded++;
					hideLoadIndicator(); 
				} else {
					queriesLoaded++;
					hideLoadIndicator();
				}
			});
			
			return newChart;
		}
		
		//***********************************************************************************************
		
		//different timelines (line charts)
		//
		visitsTimeline = newGAChart('visitsTimeline', 'LINE', 'visits-timeline-container', 'ga:sessions', 'ga:date');
		//
		customersTimeline = newGAChart('customersTimeline', 'LINE', 'customers-timeline-container', 'ga:users', 'ga:date');
		//
		visitsLengthTimeline = newGAChart('visitsLengthTimeline', 'LINE', 'visits-length-timeline-container', 'ga:avgSessionDuration', 'ga:date');
		//
		visitsLengthTable = newGAChart('visitsLengthTable', 'TABLE', 'visits-length-table-container', 'ga:avgSessionDuration', 'ga:date');
		
		//***********************************************************************************************
		
		//different charts with short total info (pie, column, tables)
		//
		genderTotalsPieChart = newGAChart('genderTotalsPieChart', 'PIE', 'chart-gender-totals-container', 'ga:users', 'ga:userGender');
		//
		genderUniqueTableChart = newGAChart('genderUniqueTableChart', 'TABLE', 'chart-gender-unique-table-container', 'ga:users', 'ga:date, ga:userGender');
		//
		genderTable = newGAChart('genderTable', 'TABLE', 'chart-gender-table-container', 'ga:sessions', 'ga:date, ga:userGender');
		//
		ageTotalsColumnChart = newGAChart('ageTotalsColumnChart', 'COLUMN', 'chart-age-totals-container', 'ga:users', 'ga:userAgeBracket');
		//
		ageUniqueTableChart = newGAChart('ageUniqueTableChart', 'TABLE', 'chart-age-unique-table-container', 'ga:users', 'ga:date, ga:userAgeBracket');
		//
		ageTable = newGAChart('ageTable', 'TABLE', 'chart-age-table-container', 'ga:sessions', 'ga:date, ga:userAgeBracket');
		//
		userTypesTotalsPieChart = newGAChart('userTypesTotalsPieChart', 'PIE', 'chart-usertypes-totals-container', 'ga:sessions', 'ga:userType');
		//
		dayVisitiorsTotalsColumnChart = newGAChart('dayVisitiorsTotalsColumnChart', 'COLUMN', 'chart-dayvisitors-totals-container', 'ga:sessions', 'ga:hour');
		//
		dayVisitiorsTableChart = newGAChart('dayVisitiorsTableChart', 'TABLE', 'chart-dayvisitors-table-container', 'ga:sessions', 'ga:hour');
		//
		interestsGeneralBarChart = newGAChart('interestsGeneralBarChart', 'BAR', 'chart-interests-general-container', 'ga:users', 'ga:interestAffinityCategory', '-ga:users', 20);
		//
		interestsGeneralTableChart = newGAChart('interestsGeneralTableChart', 'TABLE', 'chart-interests-general-table-container', 'ga:users', 'ga:interestAffinityCategory', '-ga:users', 20);
		//
		interestsCommercBarChart = newGAChart('interestsCommercBarChart', 'BAR', 'chart-interests-commerc-container', 'ga:users', 'ga:interestInMarketCategory', '-ga:users', 20);
		//
		interestsCommercTableChart = newGAChart('interestsCommercTableChart', 'TABLE', 'chart-interests-commerc-table-container', 'ga:users', 'ga:interestInMarketCategory', '-ga:users', 20);
		//combined table of visits information
		//***********************************************************************************************
		
		//create data obj combined of 3 data objects
		function createCombinedVisitsTableData(chart){
			var newData = {
				cols: []
			};
			var timeDimension = chart.dataObjs['usersData'].columnHeaders[0].name;
			var timeLabel;
			var timeType;
			switch (timeDimension){
				case "ga:date":
					timeLabel = "Date";
					timeType = "date";
				break;
				case "ga:dateHour":
					timeLabel = "Hour of Day";
					timeType = "datetime";
				break;
				case "ga:isoYearIsoWeek":
					timeLabel = "ISO week of ISO year";
					timeType = "string";
				break;
				case "ga:yearMonth":
					timeLabel = "month";
					timeType = "string";
				break;
				default:
					timeLabel = "Time";
					timeType = "string";
				break;
			}
			var dCol = {
				id: timeDimension,
				label: timeLabel,
				type: timeType
			};
			var vCol = {
				id: "visits",
				label: "Overall",
				type: "number"
			};
			var uCol = {
				id: "users",
				label: "Unique",
				type: "number"
			};
			var nvCol = {
				id: "new_visits",
				label: "New",
				type: "number"
			};
			var ovCol = {
				id: "old_visits",
				label: "Returned",
				type: "number"
			};
			newData.cols[0] = dCol;
			newData.cols[1] = nvCol;
			newData.cols[2] = ovCol;
			newData.cols[3] = vCol;
			newData.cols[4] = uCol;
			
			//convert GA date string to datetime obj
			function gaDateStrToDateObj(dateStr){
				var year = dateStr.substr(0, 4);
				var month = dateStr.substr(4, 2) - 1;
				var day = dateStr.substr(6, 2);
				var hour = '00';
				if (dateStr.length > 8){
					hour = dateStr.substr(8, 2);
				}
				return new Date(year, month, day, hour);
			}
			
			var usersDataRows = chart.dataObjs['usersData'].rows;
			var visitsDataRows = chart.dataObjs['visitsData'].rows;
			var userTypesDataRows = chart.dataObjs['userTypesData'].rows;
			var totalV = 0;
			var totalU = 0;
			var totalNV = 0;
			var totalOV = 0;
			newData.rows = [];
			var usersI = 0;
			var visitsI = 0;
			var userTypesI = 0;
			if (visitsDataRows != null){
				while ((visitsI < visitsDataRows.length)){
					var newC = {
						c: []
					};
					var date;
					var vV = 0;
					var vU = 0;
					var vNV = 0;
					var vOV= 0;
					
					switch (timeDimension){
						case "ga:date":
							date = gaDateStrToDateObj(visitsDataRows[visitsI][0]);
						break;
						case "ga:dateHour":
							date = gaDateStrToDateObj(visitsDataRows[visitsI][0]);
						break;
						case "ga:week":
						case "ga:yearMonth":
						default:
							date = visitsDataRows[visitsI][0];
						break;
					}
					vV = visitsDataRows[visitsI][1];
					if (usersDataRows != null){
						if (usersI < usersDataRows.length){
							if (usersDataRows[usersI][0] == visitsDataRows[visitsI][0]){
								vU = usersDataRows[usersI][1];
								usersI++;
							}
						}
					}
					if (userTypesDataRows != null){
						for (i=0; i<2; i++){
							if (userTypesI < userTypesDataRows.length){
								if (userTypesDataRows[userTypesI][0] == visitsDataRows[visitsI][0]){
									if (userTypesDataRows[userTypesI][1] == "New Visitor"){
										vNV = userTypesDataRows[userTypesI][2];
									} else if (userTypesDataRows[userTypesI][1] == "Returning Visitor"){
										vOV = userTypesDataRows[userTypesI][2];
									}
									userTypesI++;
								}
							}
						}
					}
					visitsI++;
					
					totalV = totalV + Number(vV);
					totalU = totalU + Number(vU);
					totalNV = totalNV + Number(vNV);
					totalOV = totalOV + Number(vOV);
					
					newC.c[0] = {v: date};
					newC.c[1] = {v: vNV};
					newC.c[2] = {v: vOV};
					newC.c[3] = {v: vV};
					newC.c[4] = {v: vU};
					newData.rows.push(newC);
				}
			}
			var newC = {
				c: []
			};
			newC.c[0] = {v: null};
			newC.c[1] = {v: totalNV};
			newC.c[2] = {v: totalOV};
			newC.c[3] = {v: totalV};
			newC.c[4] = {v: totalU};
			newData.rows.push(newC);
			
			dataFormatDateTime(newData);
			dataChangeLang(newData);		
			return newData;
		}
		
		//table chart itself
		var chartOptions = {
			width: '100%'
		}
		var TableContainer = document.getElementById('chart-combined-visits-table-container');
		var combinedVisitsTableChart = new google.visualization.Table(TableContainer);
		combinedVisitsTableChart.chartOptions = chartOptions;
		combinedVisitsTableChart.dataToGet = 0;
		combinedVisitsTableChart.dataObjs = [];
		
		//creating data object for getting a part of combined table info
		function newReportDataForCombinedTable(dataName, chart, metrics, dimensions){
			queriesToLoad++;
			var newRepData = new gapi.analytics.report.Data({
				query: {
					metrics: metrics,
					dimensions: dimensions
				}
			});
			//
			newRepData.on('error', function(response) {
				if ((response.error.code == 403) && (response.error.message.indexOf("Quota Error") != -1)){
					setTimeout(newRepData.execute(), 500);
					event.stopPropagation();
				} else if (response.error.code == 400) {
					$("#chart-combined-visits-table-container").empty();
					$("#chart-combined-visits-table-container").append(getSign("no data"));
					queriesLoaded++;
					hideLoadIndicator();
				}
			});
			//
			newRepData.on('success', function(response) {
				queriesLoaded++;
				chart.dataObjs[dataName] = response;
				chart.dataToGet--;
				if (chart.dataToGet <= 0){
					var data = createCombinedVisitsTableData(chart);
					var dataTable = new google.visualization.DataTable(data);
					$("#chart-combined-visits-table-container").fadeIn(0);
					chart.draw(dataTable, chart.chartOptions);
					$(".google-visualization-table").addClass("gapi-analytics-data-chart");
					$("#chart-combined-visits-table-container").fadeOut(0);
				}
				hideLoadIndicator();
			});
			//
			return newRepData;
		}
		
		//reportData for Combined Visits Table (CVT)
		//
		userTypesDataCVT = newReportDataForCombinedTable('userTypesData', combinedVisitsTableChart, 'ga:sessions', 'ga:date, ga:userType');
		//
		usersDataCVT = newReportDataForCombinedTable('usersData', combinedVisitsTableChart, 'ga:users', 'ga:date');
		//
		visitsDataCVT = newReportDataForCombinedTable('visitsData', combinedVisitsTableChart, 'ga:sessions', 'ga:date');
		
		
		
		//general things. handlind controls and drawing charts and stuff
		//***********************************************************************************************
		
		//hide load indicator when all queries are loaded (and all charts are drawn)
		function hideLoadIndicator(){
			if (queriesLoaded >= queriesToLoad){
				$("#load-indicator").fadeOut(0);
				//and show selected panel
				$('.sidebar li.active').click();
			}
		}
		
		//convert string (metro ui format) into date obj
		function strToDate(str){
			var year = str.substr(6, 4);
			var month = str.substr(3, 2);
			var day = str.substr(0, 2);
			return year + "-" + month + "-" + day;
			////////////////return str.replace(/\./g,"-");
		}
		
		//***********************************************************************************************
		
		//(?set new view?) and redraw charts
		function redrawGACharts() {
			//dates
			startDate = strToDate($("#select-date-from .date-text")[0].value);
			stopDate = strToDate($("#select-date-to .date-text")[0].value);
			//check for correct "from" and "to" dates
			if (startDate > stopDate) return;
			//queries counter for load indicator
			queriesLoaded = 0;
			//show load indicator and hide all charts to get rid of scrollbar
			$("#load-indicator").fadeIn(0);
			$(".chart-panel section").fadeOut(0);
			//creating new params for charts:
			//dimension (hours, days, ...)
			var dimensionPeriod = $("#select-period")[0].value;
			var dimension;
			switch (dimensionPeriod){
				case "hours":
					dimension = "ga:dateHour";
				break;
				case "days":
					dimension = "ga:date";
				break;
				case "weeks":
					dimension = "ga:isoYearIsoWeek";
				break;
				case "months":
					dimension = "ga:yearMonth";
				break;
			}
			
			var newParams = {
				query: {
					ids: viewID,    //can be changed theoretically
					'start-date': startDate,
					'end-date': stopDate
				}
			}
			
			var newParamsTimeline = {
				query: {
					dimensions: dimension
				}
			}
			
			var newParamsGenderTable = {
				query: {
					dimensions: dimension + ', ga:userGender'
				}
			}
			
			var newParamsAgeTable = {
				query: {
					dimensions: dimension + ', ga:userAgeBracket'
				}
			}
			
			var newParamsUserTypesDataCVT = {
				query: {
					dimensions: dimension + ', ga:userType'
				}
			}
			
			//apply new params
			visitsTotalReport.set(newParams);
			visitsNewOldTotalReport.set(newParams);
			sessionAvgTimeTotalReport.set(newParams);
			usersTotalReport.set(newParams);
			
			visitsTimeline.set(newParams);
			customersTimeline.set(newParams);
			visitsLengthTimeline.set(newParams);
			visitsLengthTable.set(newParams);
			
			genderTotalsPieChart.set(newParams);
			genderUniqueTableChart.set(newParams);
			genderTable.set(newParams);
			ageTotalsColumnChart.set(newParams);
			ageUniqueTableChart.set(newParams);
			ageTable.set(newParams);
			userTypesTotalsPieChart.set(newParams);
			dayVisitiorsTotalsColumnChart.set(newParams);
			dayVisitiorsTableChart.set(newParams);
			
			userTypesDataCVT.set(newParams);
			usersDataCVT.set(newParams);
			visitsDataCVT.set(newParams);
			
			interestsGeneralBarChart.set(newParams);
			interestsGeneralTableChart.set(newParams);
			interestsCommercBarChart.set(newParams);
			interestsCommercTableChart.set(newParams);
			
			visitsTimeline.set(newParamsTimeline);
			customersTimeline.set(newParamsTimeline);
			visitsLengthTimeline.set(newParamsTimeline);
			visitsLengthTable.set(newParamsTimeline);
			usersDataCVT.set(newParamsTimeline);
			visitsDataCVT.set(newParamsTimeline);
			
			genderTable.set(newParamsGenderTable);
			genderUniqueTableChart.set(newParamsGenderTable);
			
			ageTable.set(newParamsAgeTable);
			ageUniqueTableChart.set(newParamsAgeTable);
			
			userTypesDataCVT.set(newParamsUserTypesDataCVT);
			
			//show charts
			visitsTotalReport.execute();
			visitsNewOldTotalReport.execute();
			sessionAvgTimeTotalReport.execute();
			usersTotalReport.execute();
			
			vidgetAdgusted['visitsTimeline'] = false;
			visitsTimeline.execute();
			vidgetAdgusted['customersTimeline'] = false;
			customersTimeline.execute();
			vidgetAdgusted['visitsLengthTimeline'] = false;
			visitsLengthTimeline.execute();
			vidgetAdgusted['visitsLengthTable'] = false;
			visitsLengthTable.execute();
			
			vidgetAdgusted['genderTotalsPieChart'] = false;
			genderTotalsPieChart.execute();
			vidgetAdgusted['genderUniqueTableChart'] = false;
			genderUniqueTableChart.execute();
			vidgetAdgusted['genderTable'] = false;
			genderTable.execute();
			vidgetAdgusted['ageTotalsColumnChart'] = false;
			ageTotalsColumnChart.execute();
			vidgetAdgusted['ageUniqueTableChart'] = false;
			ageUniqueTableChart.execute();
			vidgetAdgusted['ageTable'] = false;
			ageTable.execute();
			vidgetAdgusted['userTypesTotalsPieChart'] = false;
			userTypesTotalsPieChart.execute();
			vidgetAdgusted['dayVisitiorsTotalsColumnChart'] = false;
			dayVisitiorsTotalsColumnChart.execute();
			vidgetAdgusted['dayVisitiorsTableChart'] = false;
			dayVisitiorsTableChart.execute();
			
			combinedVisitsTableChart.dataToGet = 3;
			userTypesDataCVT.execute();
			usersDataCVT.execute();
			visitsDataCVT.execute();
			
			vidgetAdgusted['interestsGeneralBarChart'] = false;
			interestsGeneralBarChart.execute();
			vidgetAdgusted['interestsGeneralTableChart'] = false;
			interestsGeneralTableChart.execute();
			vidgetAdgusted['interestsCommercBarChart'] = false;
			interestsCommercBarChart.execute();
			vidgetAdgusted['interestsCommercTableChart'] = false;
			interestsCommercTableChart.execute();
		}
		
		//when change any interface conlrol redraw content
		function addChangeControlHandler(){
			$(".chart-control").unbind("change");
			$(".chart-control").change(function() {
				redrawGACharts();
			});
		}
		
		//draw initial diagrams
		redrawGACharts();
		//add control handler
		addChangeControlHandler();
		
		//export buttons handlers
		$("#export-chart-combined-visits-table").click(function() {
			var table = $("#chart-combined-visits-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visitors_combined_data"));
		});
		$("#export-chart-dayvisitors-table").click(function() {
			var table = $("#chart-dayvisitors-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visitors_timeofday"));
		});
		$("#export-chart-gender-unique-table").click(function() {
			var table = $("#chart-gender-unique-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visitors_gender_unique"));
		});
		$("#export-chart-age-unique-table").click(function() {
			var table = $("#chart-age-unique-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visitors_age_unique"));
		});
		$("#export-chart-gender-table").click(function() {
			var table = $("#chart-gender-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visitors_gender_visits"));
		});
		$("#export-chart-age-table").click(function() {
			var table = $("#chart-age-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visitors_age_visits"));
		});
		$("#export-chart-interests-commerc-table").click(function() {
			var table = $("#chart-interests-commerc-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("interests_commerc"));
		});
		$("#export-chart-interests-general-table").click(function() {
			var table = $("#chart-interests-general-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("interests_general"));
		});
		$("#export-visits-length-table").click(function() {
			var table = $("#visits-length-table-container .google-visualization-table-table")[0];
			tableToExcel(table, getSign("visit_avg_duration"));
		});
	}
});

//*****************************************************************************************************************
// Functions for common purposes, interface and so on
//*****************************************************************************************************************

//when change any interface conlrol redraw content
function addChangeControlHandler(){
	$(".chart-control").change(function() {
		chartControlChange();
	});
}

//*****************************************************************************************************************

//redraw content
function chartControlChange(){
	switch (pageType) {
		case "location":
			redrawForLocation();
		break;
		case "advertiser":
			redrawForAdvr();
		break;
	}
}

//*****************************************************************************************************************

//get sign is user's language
function getSign(signID){
	var sign = "";
	for (l=0; l < langArray.length; l++){
		if (langArray[l]["id"].toLowerCase() == signID){
			sign = langArray[l][userLanguage];
			break;
		}
	}
	return sign;
}

//*****************************************************************************************************************

//export an HTML table to a file (found in the internet)
var tableToExcel = (function() {
    var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head> <!--[if gte mso 9]> <?xml version="1.0" encoding="UTF-8"?> <xml version="1.0" encoding="UTF-8">  <xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--> <meta charset="UTF-8"> <meta charset="UTF-8"/> <meta http-equiv="Content-Type" content="text/xml;charset=utf-8" /> <meta http-equiv="Content-Type" content="text/html;charset=utf-8" > </head><body><table>{table}</table></body></html>'
            , base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
    , format = function(s, c) {
        return s.replace(/{(\w+)}/g, function(m, p) {
            return c[p];
        })
    }
    return function(table, name) {
        if (!table.nodeType)
            table = document.getElementById(table)
        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
        window.location.href = uri + base64(format(template, ctx))
    }
})()

//*****************************************************************************************************************
//*****************************************************************************************************************

//when loaded
$(document).ready(function(){
	//create datepickers (interface elements)
	
	/* $("#select-date-from").datepicker({changeMonth:true, changeYear:true}); //created by metrogui 
	$("#select-date-to").datepicker({changeMonth:true, changeYear:true}); */
	
	//draw initial diagrams
	chartControlChange();
	
	//when change period type change dialog (for ads)
	$("#select-period").change(function() {
		if (pageType == "advertiser") {
			if (this.value == "hours"){
				$("#date-to-box").css("display", "none");
				$("#select-date-from-label")[0].innerHTML = "DATE:"
			} else {
				$("#date-to-box").css("display", "");
				$("#select-date-from-label")[0].innerHTML = "FROM:"
			}
		}
	});
	
	//when change plan chechboxes, reload ads list (for ads filter)
	$(".checkbox-tariff").change(function() {
		//get a list of selected plans (from plans filter)
		var tariffs = "";
		var optionsArr = $("#select-tariffs").find("input");
		for (i=0; i < optionsArr.length; i++){
			if (optionsArr[i].checked){
				tariffs = tariffs + optionsArr[i].value + "+";
			}
		}
		//reload
		$("#select-advs").empty();
		$.post('get_advs_list.php', {tariffs: tariffs}, function(html){
			$("#select-advs").append(html);
			//add change handler to added controls
			addChangeControlHandler();
			//redraw content
			chartControlChange();
		});
	});
	
	//add change handler to controls
	addChangeControlHandler();
	
	//*****************************************************************************************************************
	// Functions for metro dashboard
	//*****************************************************************************************************************
	
	//??????????????????????????????
	function pushMessage(t){
		var mes = 'Info|Implement independently';
		$.Notify({
			caption: mes.split("|")[0],
			content: mes.split("|")[1],
			type: t
		});
	}

	//handle clicks on left menu
	//change main content part
	$('.sidebar li').click(function(){
		if (!$(this).hasClass('active')) {
			$('.sidebar li').removeClass('active');
			$(this).addClass('active');
		}
		//hide charts on panels
		$(".chart-panel section").fadeOut(0);
		//hide panels
		var panelID = this.dataset["panel_id"];
		$(".chart-panel").css("visibility", "hidden");
		//move chart containers to DIVs obn the visible panel (coz one chart can be present on multiple panels and should "jump" (GA API doesn't draw the same chart on multiple vidgets))
		var divArr = $("#"+panelID).find("div");
		for (i=0; i < divArr.length; i++){
			var divID = divArr[i].id;
			var containerID = divID.replace("div", "container");
			var containerArr = $("#"+containerID);
			if (containerArr.length != 0){
				$(divArr[i]).append(containerArr[0]);
			}
		}
		//show charts on chosen panel
		$("#" + panelID + " section").fadeIn(0);
		//show chosen panel
		$("#" + panelID).css("visibility", "visible");
	})
});
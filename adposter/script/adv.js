$(window).load(function(){
	$(".hidden").css('opacity', 0.1);
	$(".se-pre-con").fadeOut("slow");
	elem = $("#adv1")[0];
	setTimeout(fadeIn, 500, elem);
	elem = $("#adv2")[0];
	setTimeout(fadeIn, 1000, elem);
	elem = $("#adv3")[0];
	setTimeout(fadeIn, 1500, elem);
	elem = $("#adv4")[0];
	setTimeout(fadeIn, 2000, elem);
});

function fadeIn(elem){
	$(elem).css('opacity', 1);
	//$(elem).css('display', 'none');
	$(elem).hide( "slide", { direction: "down"  }, 2000 );
	//$(elem).show("slide", {direction: "up"}, 2000 );
}
/**
 * ���������� ������ ��������� ������� � ��������
 * @param {Element/Object} toObj - ������ �� ������ 
 * @param {String} tcSplit - ������ ����������� �����
 * @return {String} - ������ �� ������� ��������� ������� 
 * � ���������� ���������
 */
function getProps(toObj, tcSplit)
{
  if (!tcSplit) tcSplit = '\n';
  var lcRet = '';
  var lcTab = '    ';

    for (var i in toObj) // ��������� � ��������� ������� �� �������
      lcRet += lcTab + i + " : " + toObj[i] + tcSplit;

    lcRet = '{' + tcSplit + lcRet + '}';

  return lcRet;
}
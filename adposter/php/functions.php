<?php
date_default_timezone_set('Europe/Moscow');
/**
 * @return mysqli
 */
function dbConnect() {
	//return mysqli_connect('sql305.byethost14.com','b14_16471037','NA21ro20','b14_16471037_cornell');
	//return mysqli_connect("localhost", "root", "*070797deanMZ*" , "hot_spot_db");
	//return mysqli_connect("mysql.hostinger.co.uk", "u313079502_huser", "42KbpMP3He" , "u313079502_hs");
	return mysqli_connect("localhost", "smartmed_login", "bHnvFcSnLaF6zXus", "smartmedia_hs");
}

function select_tariff($location, $mac_address) {
	$user_id = check_user($mac_address);
	$con = dbConnect();
	//echo "location = ".$identuty."<br/>";
	$query = "select t1000.tariff, sum(t1000.views), sum(t1000.count), sum(t1000.count)/sum(t1000.views) param from
(SELECT t100.adv_id, t100.views, IFNULL( t101.count, 0 ) count, IFNULL( t101.count, 0 )/t100.views param, t100.tariff
FROM (

    SELECT DISTINCT t10.adv_id, t10.views, t10.tariff
    FROM (

    SELECT t3.id adv_id, t3.link adv_link, t3.views_per_week views, t4.type_id, t3.tariff
    FROM tbl_advs AS t3
    JOIN tbl_advs_types AS t4 ON t3.id = t4.adv_id
    ) AS t10
    JOIN (

    SELECT t2.location_id, t2.type_id
    FROM tbl_location AS t1
    JOIN tbl_locations_types AS t2 ON t2.location_id = t1.id
    WHERE t1.location =  '" . $location . "'
    ) AS t11 ON t10.type_id = t11.type_id
    ) AS t100
    LEFT JOIN (

    SELECT t5.adv_id, COUNT(t5.adv_id) AS count
    FROM tbl_stat_adv_shown as t5
    GROUP BY adv_id
    ) AS t101 ON t100.adv_id = t101.adv_id
) t1000
group by tariff order by param limit 1";

	//////echo "query = " . $query . "<br><br>";
	$result = mysqli_query($con, $query);
	$row = mysqli_fetch_array($result);
	$tariff = $row['tariff'];
	mysqli_close($con);
	return $row['tariff'];
}

function select_advs($location, $username, $tariff = '0', $mobile = false) {
	$user_id = check_user($username);
	$con = dbConnect();

	$query = "select t100.location_id, t100.adv_id, t100.adv_link, t100.views, IFNULL( t101.count, 0 ) count, IFNULL( t101.count, 0 )/t100.views param
FROM (

    SELECT DISTINCT t11.location_id, t10.adv_id, t10.adv_link, t10.views
    FROM (

    SELECT t3.id adv_id, t3.link adv_link, t3.views_per_week views, t4.type_id
    FROM tbl_advs AS t3
    JOIN tbl_advs_types AS t4 ON t3.id = t4.adv_id
    WHERE t3.tariff='" . $tariff . "'
    ) AS t10
    JOIN (

    SELECT t2.location_id, t2.type_id
    FROM tbl_location AS t1
    JOIN tbl_locations_types AS t2 ON t2.location_id = t1.id
    WHERE t1.location =  '" . $location . "'
    ) AS t11 ON t10.type_id = t11.type_id
    ) AS t100
    LEFT JOIN (

    SELECT t5.adv_id, COUNT(t5.adv_id) AS count
    FROM tbl_stat_adv_shown as t5
    GROUP BY adv_id
    ) AS t101 ON t100.adv_id = t101.adv_id
ORDER BY param limit ";
	if ($tariff == '0') {
		if ($mobile) {
			$query = $query . "2";
		} else {
			$query = $query . "4";
		}
	} else {
		$query = $query . "1";
	}
	// echo "query = ";
	// echo $query . "<br/><br/>";
	$data = array();
	$result = mysqli_query($con, $query);
	while ($row = mysqli_fetch_array($result)) {
		add_show_stat($user_id, $row[1], $row[0]);
		array_push($data, $row);
	}
	mysqli_close($con);
	return $data;
}

function check_user($username) {

	$con = dbConnect();

	$query = "select id from tbl_stat_users WHERE mac_address='{$username}'";
	$result = mysqli_query($con, $query);

	if (!$row = mysqli_fetch_array($result)) {
		$user_id = add_user($username);
	} else {
		$user_id = $row[0];
	}

	mysqli_close($con);

	return $user_id;
}

function add_user($username) {

	$con = dbConnect();

	$query = "SET time_zone = 'Pacific/Auckland'";
	$saveResult = mysqli_query($con, $query);
	$query = "insert into tbl_stat_users (mac_address) values('$username')";
	$saveResult = mysqli_query($con, $query);
	$query = "select id from tbl_stat_users WHERE mac_address='$username'";
	$result = mysqli_query($con, $query);

	$row = mysqli_fetch_array($result);
	$user_id = $row[0];
	mysqli_close($con);

	return $user_id;
}

function add_show_stat($user_id, $adv_id, $loc_id) {

	$con = dbConnect();

	$query = "SET time_zone = 'Pacific/Auckland'";
	$saveResult = mysqli_query($con, $query);
	$query = "insert into tbl_stat_adv_shown (user_id, adv_id, location_id)
      values('$user_id', '$adv_id', '$loc_id')";
	$saveResult = mysqli_query($con, $query);
	mysqli_close($con);
}

function add_click_stat($user_id, $adv_id, $loc_id) {

	$con = dbConnect();

	$query = "SET time_zone = 'Pacific/Auckland'";
	$saveResult = mysqli_query($con, $query);
	$query = "insert into tbl_stat_adv_clicked (user_id, adv_id, location_id)
      values('$user_id', '$adv_id', '$loc_id')";
	$saveResult = mysqli_query($con, $query);
	mysqli_close($con);
}

//*****************************************************************************
//Added by Marat. For GA tracking
//*****************************************************************************
function get_location_page($location){
	$con = dbConnect();
	$query = "SELECT analytics_link FROM tbl_location WHERE location='{$location}'";
	$result = mysqli_query($con, $query);

	if (!$row = mysqli_fetch_array($result)) {
		$page = "http://unknown-location.smartmedia.nz";
	} else {
		$page = $row[0];
	}
	mysqli_close($con);
	return $page;
}

/*select t3.id, t3.link, t2.type_id, t4.id, sum(t5.counter), t2.id
FROM tbl_location as t1
join tbl_locations_types as t2 on t2.location_id=t1.id
join tbl_adds as t3 on t3.type_id=t2.type_id
join tbl_stat_users as t4
left join tbl_stat_add_shown as t5 on t5.add_id=t3.id and t5.user_id=t4.id
WHERE t1.location='HobsonSt150' and t4.mac_address='74-F0-6D-92-59-A5'
group by t3.id, t3.link, t2.type_id, t4.id, t2.id order by sum(t5.counter) limit 4

select t2.location_id, t2.type_id
FROM tbl_location as t1
join tbl_locations_types as t2 on t2.location_id=t1.id
WHERE t1.location='HobsonSt150'

select distinct types_add.add_id, types_add.add_link from (select t3.id add_id,
t3.link add_link, t4.id type_id
FROM tbl_adds as t3
join tbl_adds_types as t4 on t3.id=t4.add_id) as types_add

select distinct types_add.add_id, types_add.add_link from (select t3.id add_id, t3.link add_link, t4.id type_id
FROM tbl_adds as t3
join tbl_adds_types as t4 on t3.id=t4.add_id) as types_add
inner join (select t2.location_id, t2.type_id
FROM tbl_location as t1
join tbl_locations_types as t2 on t2.location_id=t1.id
WHERE t1.location='HobsonSt150') as types_location on types_add.type_id=types_location.type_id

select distinct types_location.location_id, types_add.add_id, types_add.add_link from (select t3.id add_id, t3.link add_link, t4.id type_id
FROM tbl_adds as t3
join tbl_adds_types as t4 on t3.id=t4.add_id) as types_add
inner join (select t2.location_id, t2.type_id
FROM tbl_location as t1
join tbl_locations_types as t2 on t2.location_id=t1.id
WHERE t1.location='HobsonSt150') as types_location on types_add.type_id=types_location.type_id

select types_location.location_id, types_add.add_id, types_add.add_link, types_add.type_id from (select t3.id add_id, t3.link add_link, t4.id type_id
FROM tbl_adds as t3
join tbl_adds_types as t4 on t3.id=t4.add_id) as types_add
join (select t2.location_id, t2.type_id
FROM tbl_location as t1
join tbl_locations_types as t2 on t2.location_id=t1.id
WHERE t1.location='HobsonSt150') as types_location on types_add.type_id=types_location.type_id

select t3.id add_id, t3.link add_link, t4.id type_id
FROM tbl_adds as t3
join tbl_adds_types as t4 on t3.id=t4.add_id

select distinct types_location.location_id, types_add.add_id, types_add.add_link
from (select t3.id add_id, t3.link add_link, t4.type_id
FROM tbl_adds as t3
join tbl_adds_types as t4 on t3.id=t4.add_id) as types_add
join (select t2.location_id, t2.type_id
FROM tbl_location as t1
join tbl_locations_types as t2 on t2.location_id=t1.id
WHERE t1.location='HobsonSt150') as types_location on types_add.type_id=types_location.type_id

SELECT add_id, count(add_id) as count FROM tbl_stat_add_shown group by add_id

select t100.location_id, t100.add_id, t100.add_id, t100.add_link
from (select distinct t11.location_id, t10.add_id, t10.add_link
from (select t3.id add_id, t3.link add_link, t4.type_id
FROM tbl_adds as t3
join tbl_adds_types as t4 on t3.id=t4.add_id) as t10
join (select t2.location_id, t2.type_id
FROM tbl_location as t1
join tbl_locations_types as t2 on t2.location_id=t1.id
WHERE t1.location='HobsonSt150') as t11 on t10.type_id = t11.type_id) as t100

select t100.location_id, t100.add_id, t100.add_id, t100.add_link, t101.count
from (select distinct t11.location_id, t10.add_id, t10.add_link
from (select t3.id add_id, t3.link add_link, t4.type_id
FROM tbl_adds as t3
join tbl_adds_types as t4 on t3.id=t4.add_id) as t10
join (select t2.location_id, t2.type_id
FROM tbl_location as t1
join tbl_locations_types as t2 on t2.location_id=t1.id
WHERE t1.location='HobsonSt150') as t11 on t10.type_id = t11.type_id)
as t100 left join (SELECT add_id, count(add_id) as count
FROM tbl_stat_add_shown group by add_id) as t101 on t100.add_id = t101.add_id ORDER by t101.count limit 4

select t10.adv, t2.name, t10.cnt, t1.views_per_week, t10.cnt/t1.views_per_week as percent from (SELECT add_id as adv, count(time) as cnt FROM tbl_stat_add_shown group by add_id) as t10 join tbl_adds as t1 on t1.id=t10.adv join tbl_customers as t2 on t2.id=t1.customer_id order by percent

SELECT t100.location_id, t100.adv_id, t100.adv_link, t100.adv_motto, t100.views, IFNULL( t101.count, 0 ) count, IFNULL( t101.count, 0 )/t100.views param
FROM (

SELECT DISTINCT t11.location_id, t10.adv_id, t10.adv_link, t10.adv_motto, t10.views
FROM (

SELECT t3.id adv_id, t3.link adv_link, t3.motto adv_motto, t3.views_per_week views, t4.type_id
FROM tbl_advs AS t3
JOIN tbl_advs_types AS t4 ON t3.id = t4.adv_id
) AS t10
JOIN (

SELECT t2.location_id, t2.type_id
FROM tbl_location AS t1
JOIN tbl_locations_types AS t2 ON t2.location_id = t1.id
WHERE t1.location =  'QeenSt150'
) AS t11 ON t10.type_id = t11.type_id
) AS t100
LEFT JOIN (

SELECT t5.adv_id, COUNT(t5.adv_id) AS count
FROM tbl_stat_adv_shown as t5
join tbl_stat_users as t6 on t5.user_id=t6.id
where t6.stuff='0'
GROUP BY adv_id
) AS t101 ON t100.adv_id = t101.adv_id
ORDER BY param

SELECT t100.location_id, t100.adv_id, t100.adv_link, t100.adv_motto, t100.views, IFNULL( t101.count, 0 ) count, IFNULL( t101.count, 0 )/t100.views param, t100.tariff
FROM (

SELECT DISTINCT t11.location_id, t10.adv_id, t10.adv_link, t10.adv_motto, t10.views, t10.tariff
FROM (

SELECT t3.id adv_id, t3.link adv_link, t3.motto adv_motto, t3.views_per_week views, t4.type_id, t3.tariff
FROM tbl_advs AS t3
JOIN tbl_advs_types AS t4 ON t3.id = t4.adv_id
) AS t10
JOIN (

SELECT t2.location_id, t2.type_id
FROM tbl_location AS t1
JOIN tbl_locations_types AS t2 ON t2.location_id = t1.id
WHERE t1.location =  'QeenSt150'
) AS t11 ON t10.type_id = t11.type_id
) AS t100
LEFT JOIN (

SELECT t5.adv_id, COUNT(t5.adv_id) AS count
FROM tbl_stat_adv_shown as t5
join tbl_stat_users as t6 on t5.user_id=t6.id
where t6.stuff='0'
GROUP BY adv_id
) AS t101 ON t100.adv_id = t101.adv_id
ORDER BY param

select t1000.tariff, sum(t1000.views), sum(t1000.count), sum(t1000.count)/sum(t1000.views) param from
(SELECT t100.adv_id, t100.views, IFNULL( t101.count, 0 ) count, IFNULL( t101.count, 0 )/t100.views param, t100.tariff
FROM (

SELECT DISTINCT t10.adv_id, t10.views, t10.tariff
FROM (

SELECT t3.id adv_id, t3.link adv_link, t3.motto adv_motto, t3.views_per_week views, t4.type_id, t3.tariff
FROM tbl_advs AS t3
JOIN tbl_advs_types AS t4 ON t3.id = t4.adv_id
) AS t10
JOIN (

SELECT t2.location_id, t2.type_id
FROM tbl_location AS t1
JOIN tbl_locations_types AS t2 ON t2.location_id = t1.id
WHERE t1.location =  'QeenSt150'
) AS t11 ON t10.type_id = t11.type_id
) AS t100
LEFT JOIN (

SELECT t5.adv_id, COUNT(t5.adv_id) AS count
FROM tbl_stat_adv_shown as t5
GROUP BY adv_id
) AS t101 ON t100.adv_id = t101.adv_id
) t1000
group by tariff order by param

 */
?>